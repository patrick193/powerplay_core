<?php

namespace PowerPlay;

use PowerPlay\Users\AbstractUser;

/**
 * Description of User
 *
 * @author Developer Pohorielov Vladyslav
 */
abstract class User extends AbstractUser {

    private $switch = false;

    public function Lock($id) {
        $this->setIsLocked(true);
    }

    public function Create() {
        
    }

    protected function Switcher($switch = false) {
        $this->switch = $switch;
    }

    protected function getSwitcher() {
        return $this->switch;
    }


    abstract public function Execute(User $user);
}
