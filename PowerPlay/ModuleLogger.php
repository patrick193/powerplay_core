<?php

namespace PowerPlay;

/**
 * @author Pavel Petrov
 */
class ModuleLogger
{

    /**
     * @var resource
     */
    private static $logFile;

    public static function log($message)
    {
        if (empty(self::$logFile)) {
            self::$logFile = fopen(\Config::$log, 'a+');
        }

        $trace = debug_backtrace();

        $dateTime = new \DateTime('now');
        self::write($dateTime->format('d-m-Y H:i:s'));
        self::write('message: ' . $message);

        $space = '';

        for ($index = count($trace) - 1; $index > -1; $index--) {
            $file     = $trace[$index]['file'];
            $line     = $trace[$index]['line'];
            $class    = $trace[$index]['class'];
            $function = $trace[$index]['function'];

            if (!isset($class) and !isset($file)) {
                $class = $trace[$index - 1]['class'];
            } elseif ($file) {
                $class = "$file($line)";
            } else {
                $class = "$class";
            }

            $logMessage = "$class->$function()";

            self::write($space.$logMessage);
            $space.='    ';
        }

        self::write();
        fclose(self::$logFile);
    }

    private static function write($message = null)
    {
        fwrite(self::$logFile, (string) $message . "\n");
    }

}
