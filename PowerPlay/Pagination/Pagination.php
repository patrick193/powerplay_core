<?php

namespace PowerPlay\Pagination;

use PowerPlay\Database;
use PowerPlay\PowerplayException\PowerplayException;

/**
 * @author Pavel Petrov
 */
class Pagination
{

    private function __construct()
    {
        
    }

    /**
     * This method designed for calculate items per page
     * Concatenation LIMIT OFFSET to query
     * 
     * @param string $query
     * @param int $pageNumber
     * @param int $perPage
     * @return array
     * @throws PowerplayException
     */
    public static function paginate($query, $pageNumber = NULL, $perPage = NULL)
    {
        if(!$query) {
            throw new PowerplayException(MOD_SET_UP_DATA);
        }

        $baseDBClass = new Database();
        $baseDBClass->Connect();

        $countRecords  = self::getCount($query);
        $itemsPerPage  = $perPage ? : \Config::$itemsPerPage;
        $numberOfPages = ceil($countRecords / $itemsPerPage);
        $pageNumber    = $numberOfPages < $pageNumber ? $numberOfPages : $pageNumber;

        if(!$pageNumber or $pageNumber == 1) {
            $query.=" LIMIT 0 , $itemsPerPage";
        } else {
            $limit = ($itemsPerPage * $pageNumber) - $itemsPerPage;
            $query.=" LIMIT $limit , $itemsPerPage";
        }

        return ['page_num' => $numberOfPages, 'query' => $baseDBClass->Execute($query)];
    }

    /**
     * Method retrns count of item in currenct query
     * 
     * @param string $query
     * @return int
     * @throws PowerplayException
     */
    private static function getCount($query)
    {
        if(!$query) {
            throw new PowerplayException(MOD_SET_UP_DATA);
        }

        $query       = str_replace('FROM', ', COUNT(*) FROM', $query);
        $baseDBClass = new Database();
        $baseDBClass->Connect();
        $mysqli      = $baseDBClass->getDb();
        $result      = $mysqli->query($query)->fetch_row();
        return (int) end($result);
    }

}
