<?php

error_reporting(E_ERROR);
if (isset($argv[1])) {
     split_argv($argv);
}

function split_argv($argv) {
    switch ($argv[1]) {
        case 'session':
            $result = session($argv[2]);
            break;
    }
    return $result;
}

/////////////////////////////

function session($argvTwo) {
    $colors = new Colors();
    switch ($argvTwo) {
        case 'destroy':
            session_destroy();
            echo $colors->getColoredString("Session was destroed!\n", 'blue');
            break;
        case 'restart':
            session_destroy();
            session_start();
            echo $colors->getColoredString('Session was reloaded!' ."\n", 'green');
            break;
        case 'start':
            session_start();
            echo $colors->getColoredString('Session was started' ."\n", 'green');
            break;
        case 'status':
            $status = session_status();
           echo $colors->getColoredString($status . "\n", 'green');
        default :
            session_start();
            break;
    }
}

//// CLASS COLORS

class Colors {

    private $foreground_colors = array();
    private $background_colors = array();

    public function __construct() {
        // Set up shell colors
        $this->foreground_colors['black'] = '0;30';
        $this->foreground_colors['dark_gray'] = '1;30';
        $this->foreground_colors['blue'] = '0;34';
        $this->foreground_colors['light_blue'] = '1;34';
        $this->foreground_colors['green'] = '0;32';
        $this->foreground_colors['light_green'] = '1;32';
        $this->foreground_colors['cyan'] = '0;36';
        $this->foreground_colors['light_cyan'] = '1;36';
        $this->foreground_colors['red'] = '0;31';
        $this->foreground_colors['light_red'] = '1;31';
        $this->foreground_colors['purple'] = '0;35';
        $this->foreground_colors['light_purple'] = '1;35';
        $this->foreground_colors['brown'] = '0;33';
        $this->foreground_colors['yellow'] = '1;33';
        $this->foreground_colors['light_gray'] = '0;37';
        $this->foreground_colors['white'] = '1;37';

        $this->background_colors['black'] = '40';
        $this->background_colors['red'] = '41';
        $this->background_colors['green'] = '42';
        $this->background_colors['yellow'] = '43';
        $this->background_colors['blue'] = '44';
        $this->background_colors['magenta'] = '45';
        $this->background_colors['cyan'] = '46';
        $this->background_colors['light_gray'] = '47';
    }

    // Returns colored string
    public function getColoredString($string, $foreground_color = null, $background_color = null) {
        $colored_string = "";

        // Check if given foreground color found
        if (isset($this->foreground_colors[$foreground_color])) {
            $colored_string .= "\033[" . $this->foreground_colors[$foreground_color] . "m";
        }
        // Check if given background color found
        if (isset($this->background_colors[$background_color])) {
            $colored_string .= "\033[" . $this->background_colors[$background_color] . "m";
        }

        // Add string and end coloring
        $colored_string .= $string . "\033[0m";

        return $colored_string;
    }

    // Returns all foreground color names
    public function getForegroundColors() {
        return array_keys($this->foreground_colors);
    }

    // Returns all background color names
    public function getBackgroundColors() {
        return array_keys($this->background_colors);
    }

}

/**
 * if (preg_match("/\D/", $moduleRouting['currently'][$i]) and ! preg_match("/\=/", $moduleRouting['currently'][$i])) {
                $pathName .= "_" . $moduleRouting['currently'][$i]; //concat out rule
            } else {
                if (preg_match("/\=/", $moduleRouting['currently'][$i])) {
                    $simpleData = $this->URI($moduleRouting['currently'][$i]); // standart url like name=vlad&lastname=pogorelov
                    $parameters[] = $this->CreateParametersFromArray($simpleData); //create parameters from those array
                } else {
                    $parameters[] = $moduleRouting['currently'][$i]; // or just add in array
                }
            }
 */