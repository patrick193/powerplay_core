<?php

namespace PowerPlay;

use PowerPlay\Routing\BasicRouting;

/**
 *
 * @author Developer Pohorielov Vladyslav
 */
class Route extends BasicRouting{

    public function __construct(Storage $storage = null){
        parent::__construct();
    }
    
}
