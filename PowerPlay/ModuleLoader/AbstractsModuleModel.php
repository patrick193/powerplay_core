<?php

namespace PowerPlay\ModuleLoader;

/**
 * Object of Module
 *
 * @author Your Name Pohorielov Vladyslav
 */
abstract class AbstractsModuleModel {

    /**
     * Module Name
     * @var string 
     */
    private $moduleName;

    /**
     * Module Description
     * @var string 
     */
    private $moduleDescription;

    /**
     * Namespace of the module
     * @var type 
     */
    private $moduleLoaction;

    /**
     * Anable or not 
     * @var boolean 
     */
    private $enable;

    /**
     * Url 
     * @var string 
     */
    private $uri;

    /**
     *
     * Module 
     */
    private $module;

    /**
     * How can use it
     * @var array 
     */
    private $groups;

    /**
     * What every group can do
     * @var array 
     */
    private $access;

    /**
     * If we have an override db
     * @var array 
     */
    private $db;

    /**
     * Routing of the module
     * @var array 
     */
    private $routing;

    /**
     * For more flexobl access and settings;
     * @var array 
     */
    private $defineAccess;

    public function getDefineAccess() {
        return $this->defineAccess;
    }

    public function setDefineAccess($defineAccess) {
        $this->defineAccess = $defineAccess;
        return $this;
    }

    public function getRouting() {
        return $this->routing;
    }

    public function setRouting($routing) {
        $this->routing = $routing;
        return $this;
    }

    public function getDb() {
        return $this->db;
    }

    public function setDb($db) {
        $this->db = $db;
        return $this;
    }

    public function getGroups() {
        return $this->groups;
    }

    public function getAccess() {
        return $this->access;
    }

    public function setGroups($groups) {
        $this->groups = $groups;
        return $this;
    }

    public function setAccess($access) {
        $this->access = $access;
        return $this;
    }

    public function getModuleName() {
        return $this->moduleName;
    }

    public function setModuleName($moduleName) {
        $this->moduleName = $moduleName;
    }

    public function getModuleDescription() {
        return $this->moduleDescription;
    }

    public function setModuleDescription($moduleDescription) {
        $this->moduleDescription = $moduleDescription;
    }

    public function getModuleLocation() {
        return $this->moduleLoaction;
    }

    public function setModuleLocation($moduleLocation) {
        $this->moduleLoaction = $moduleLocation;
    }

    public function getEnable() {
        return $this->enable;
    }

    public function setEnable($moduleAnable) {
        $this->enable = $moduleAnable;
    }

    public function getURI() {
        return $this->uri;
    }

    public function setURI($uri) {
        $this->uri = $uri;
    }

    public function getModule() {
        return $this->module;
    }

    public function setModule($module) {
        $this->module = $module;
    }

    abstract public function ExecuteUpdate($moduleName, $yaml);

    abstract public function PrepareYaml(AbstractsModuleModel $module);
}
