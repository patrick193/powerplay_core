<?php

namespace PowerPlay\PPS;

/**
 * Description of PPS
 *
 * @author Developer Pohorielov Vladyslav
 */
class PPS {

    /**
     *
     * @var \Smarty 
     */
    private $smarty;

    public function __construct() {

        $this->SmartyStart();
    }

    /**
     * Method for connection and initialization a smarty
     */
    private function SmartyStart() {

        //__DIR__ . "/libs/Smarty.class.php"
        \autoloader::addAutoloader(__DIR__ . "/libs/Smarty.class.php");
        $this->smarty = new \Smarty();
        $this->SmartySetUp();
    }

    /**
     * Method for initialization and config smarty
     */
    protected function SmartySetUp() {
        $this->smarty->template_dir = \Config::$web . 'templates/';
        $this->smarty->compile_dir = \Config::$web . 'templates_c/';
        $this->smarty->cache_dir = \Config::$web . 'cache/';

//        $this->smarty->caching = true;
    }

    /**
     * Method for seting up all variables
     * @param mixed $variableName Variable name. You will call this variable with this name from .tpl file. It can be a list of names
     * @param mixed $variableValue Value of this variables
     */
    public function setParameters($variableName, $variableValue) {
        !$this->smarty ? $this->SmartyStart() : '';

        if(is_array($variableName)) {
            foreach($variableName as $key => $name) {
                $this->smarty->assign($name, $variableValue[$key]);
            }
        } else {
            $this->smarty->assign($variableName, $variableValue);
        }
    }

    /**
     * Method for setting up a variable like an object
     * This is a function like a setParameters but working with objects
     * @param string $variableName
     * @param object $object
     */
    public function setObject($variableName, $object) {
        !$this->smarty ? $this->SmartyStart() : '';

        $this->smarty->registerObject($variableName, $object);
    }

    /**
     * Method for rendoring a template
     * @param string $template 
     */
    public function Display($template) {

        !$this->smarty ? $this->smarty = $this->SmartyStart() : '';
        strpos($template, ".tpl") ? '' : $template .= ".tpl";
        $this->smarty->display($template);
    }

}
