<?php

namespace PowerPlay;

use PowerPlay\Users\AbstractRole;

/**
 * Description of Role
 *
 * @author Developer Pohorielov Vladyslav
 */
abstract class Role extends AbstractRole {

    protected $role_code, $group_id;

    public function getGroupId() {
        return $this->group_id;
    }

    public function setGroupId($group_id) {
        $this->group_id = $group_id;
        return $this;
    }

    public function getRoleCode() {
        return $this->role_code;
    }

    public function setRoleCode($roleCode) {
        $this->role_code = $roleCode;
        return $this;
    }

    public function Lock($id) {
        $this->setIsLoacked(true);
    }

    public function Create() {
    }

    public function Load($parameters) {
        
    }

    abstract public function Execute(Role $role);
    
}
