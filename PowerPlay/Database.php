<?php

namespace PowerPlay;

use PowerPlay\DatabaseLoader\BasicDataBase;
/**
 * Description of Database
 *
 * @author Developer Pohorielov Vladyslav
 */
class Database extends BasicDataBase {

    /**
     * Init Class
     */
    public function __construct($host = '', $user = '', $password = '', $database = '', $execute = true) {
        parent::__construct($host, $user, $password, $database, $execute);
    }

    /**
     * Destruct of Class
     */
    public function __destruct() {
        parent::__destruct();
    }

}
