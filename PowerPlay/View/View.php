<?php

namespace PowerPlay\View;

use Exception;

/**
 * Description of View
 *
 * @author Developer Pohorielov Vladyslav
 */
class View {

    /**
     * Function to get path to the template
     * @param string $moduleName
     * @return string
     * @throws Exception
     */
    public function getPath($moduleName) {
        if (!is_string($moduleName) or empty($moduleName)) {
            throw new Exception('We can not find a template');
        }

        $dir = \Config::$web . 'views/';
        $templatePath = $dir . $moduleName . "/index.tpl";
        if (is_dir($dir) and file_exists($templatePath)) {
            return $templatePath;
        }else{
            throw new Exception('Wrong path to templates: ' . $templatePath);
        }
        
    }
    

}
