<?php

namespace PowerPlay\Mailer\Helpers;

use PowerPlay\YamlConfiguration;
use Exception;

class MailerHelper {

    /**
     * 
     * @var string $smtp_username - login
     * @var string $smtp_password - pass
     * @var string $smtp_host - host
     * @var string $smtp_from - sender
     * @var integer $smtp_port - port
     * @var string $smtp_charset - charset
     *
     */
    public $smtp_username;
    public $smtp_password;
    public $smtp_host;
    public $smtp_from;
    public $smtp_port;
    public $smtp_charset;

    public function __construct($smtp_charset = "utf-8") {
        date_default_timezone_set('UTC');

        $yaml = new YamlConfiguration();

        $config = $yaml->GetConfigurations(__DIR__ . "/../Config/MailerCustomConfig.yml");

        if($config) {
            $this->smtp_username = $config->Username;
            $this->smtp_password = $config->Password;
            $this->smtp_host = $config->Host;
            $this->smtp_from = $config->From;
            $this->smtp_port = $config->Port;
            $this->smtp_charset = $smtp_charset;
        } else {
            throw new Exception('We can not connect to the server');
        }
    }

    /**
     * Отправка письма
     * 
     * @param string $mailTo - получатель письма
     * @param string $subject - тема письма
     * @param string $message - тело письма
     * @param string $headers - заголовки письма
     *
     * @return bool|string В случаи отправки вернет true, иначе текст ошибки    *
     */
    public function send($mailTo, $subject, $message, $document = false) {
        require_once __DIR__ . '/PHPMailer/PHPMailerAutoload.php';

        $db = new \PowerPlay\Database();
        $name = $db->Select([['user_first_name', 'user_last_name'], 'powerplay_users',
                    ['user_email' => $mailTo]])[0];

        $mail = new \PHPMailer;

        $mail->isSMTP();
        $mail->SMTPDebug = 0;
        $mail->Debugoutput = 'html';
        $mail->Host = $this->smtp_host;
        $mail->Port = $this->smtp_port;
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAuth = true;
        $mail->Username = $this->smtp_username;
        $mail->Password = $this->smtp_password;
        $mail->setFrom('vpogorelov@livinginternet.de', 'Vlad Pogorelov');
        $mail->addAddress($mailTo, $name->user_first_name . " " . $name->user_last_name);
        $mail->Subject = $subject;

        $mail->Body = $message;
        if($document !== false) {
            $mail->addAttachment($document);
        }

        if(!$mail->send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
            echo "Message sent!";
        }
    }

}
