<?php

namespace PowerPlay\Mailer;

use PowerPlay\Module;
use PowerPlay\Mailer\Helpers\MailerHelper;

/**
 * Description of Mailer
 *
 * @author Developer Pohorielov Vladyslav
 */
class Mailer extends Module {

    private $config;

    public function getConfig() {
        if(!$this->config) {
            $helper = new MailerHelper();
            $this->config = $helper->getConfig();
        }
        return $this->config;
    }

    public function Send($args) {
        if(!is_array($args)) {
            throw new Exception('Wrong data');
        }
        if(!isset($args['header'])){
            $ymal = new \PowerPlay\YamlConfiguration();
            $config = $ymal->GetConfigurations(__DIR__ . "/Config/MailerCustomConfig.yml");
            $args['header'] = $config->defaultHeader;
        }
        try {
            $mailSMTP = new MailerHelper();
            $result = $mailSMTP->send($args['to'], $args['subject'], $args['message'], $args['header']);
 
            
        } catch(\Exception $ex){
            
            print_r("<p style='color: red'>Exception is: ".$ex->getMessage() . "</p> in " . $ex->getFile() . " on " . $ex->getLine());
            exit;
            
        }
    }

}
