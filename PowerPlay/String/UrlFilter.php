<?php

namespace PowerPlay\String;

use PowerPlay\String\AbstractFilters;
use PowerPlay\PowerplayException\PowerplayException;

/**
 * Description of UrlFilter
 *
 * @author Developer Pohorielov Vladyslav
 */
class UrlFilter extends AbstractFilters{

    protected $remove_list = array(
        'a', 'an', 'as', 'at', 'before', 'but', 'by', 'for', 'from',
        'is', 'in', 'into', 'like', 'of', 'off', 'on', 'onto', 'per',
        'since', 'than', 'the', 'this', 'that', 'to', 'up', 'via',
        'with', 'union', 'join', "'", '%27', '--', '---'
    );

   /**
     * Function to protect our system from mysql ijection
     * @param string $url
     * @return string
     * @throws Exception
     */
    public function Filter($url) {
        if (!is_string($url) or is_null($url) or empty($url)) {
            throw new PowerplayException('Wrong url: ' . $url);
        }

        $separate = explode("/", $url);
        if (!is_null($separate) and ! empty($separate)) {
            $checkedUrl = '';
            foreach ($separate as $value) {
                $text = preg_replace('/\b(' . join('|', $this->remove_list) . ')\b/i', '', $value);
//                $remove_pattern = '/[^\s_\-a-zA-Z0-9]/u';
//                $text = preg_replace($remove_pattern, '', $text); // if we dont want to have this kind of url: id=1
                $text = str_replace(' ', '_', $text);
                $text = preg_replace('/^\s+|\s+$/', '', $text);
                $text = preg_replace('/[-\s]+/', '-', $text);
                $checkedUrl .= trim(    $text, '-') . '/';
            }
            $checkedUrl = str_replace("//", "/", $checkedUrl);
        }
        return $checkedUrl;
    }

}
