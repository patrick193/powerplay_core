<?php

namespace PowerPlay\String;

use PowerPlay\String\SQLFilter;
use PowerPlay\String\StringFilter;
use PowerPlay\String\UrlFilter;

/**
 * Description of Filter
 *
 * @author Developer Pohorielov Vladyslav
 */
class Bridge {

    public function Type($type) {
        switch ($type) {
            case 'url':
                return new UrlFilter();
            case 'string':
                return new StringFilter();
            case 'sql':
                return new SQLFilter();
            default :
                return new UrlFilter();
        }
    }

}
