<?php

namespace PowerPlay\String;

/**
 * 
 * @author Developer Pohorielov Vladyslav
 */
abstract class AbstractFilters {
    
    /**
     * function to filter string
     */
    abstract public function Filter($string);
    
    /**
     * Function for build parameters for db class
     * @param array $array Type: ['first, second','first, second']
     */
    public function BuildParam($array){
        
        if(is_array($array) and !is_null($array) and !empty($array) and count($array) == 2){
            $str = '';
            $fields = explode(",", trim($array[0]));
            $values = explode(",", trim($array[1]));
            
            if(count($values) == count($fields)){
                for($i = 0, $c = count($fields); $i < $c; $i++) {
                    $str .= $fields[$i] . " = " . $values[$i];
                }
                $str = (strip_tags($str));
            }
            
        }  else {
            throw new Exception("Wrong type of array");
        }
    }
    

}
