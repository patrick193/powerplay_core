<?php

namespace Modules\Easybill;

use PowerPlay\Module;
use Modules\Easybill\Helpers\EasybillHelper;

/**
 * Description of Easybill
 *
 * @author Developer Pohorielov Vladyslav
 */
class Easybill extends Module {

    public function CreateDoc($userId, $type, $offerId, $currency = 'EUR') {
        
    }

    public function getData() {
        $user = unserialize($this->session->get('user_auth'));

        $eb = new EasybillHelper();
        $eb->Connect();
//        $eb->CreateCustomer((int) $user->getUserId());

        $docResponse = $eb->CreateDocument((int) $user->getUserId(), 'INVOICE', 10);

        $eb->getDocument($docResponse->documentID);
    }

}
