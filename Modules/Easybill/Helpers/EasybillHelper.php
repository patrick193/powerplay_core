<?php

namespace Modules\Easybill\Helpers;

use \SoapClient;
use \SoapHeader;
use \stdClass;
use Modules\Roles\Users;
use Modules\Easybill\Easybill;
use PowerPlay\Database;
use PowerPlay\PowerplayException\PowerplayException;

/**
 * Description of Easybill
 *
 * @author Developer Pohorielov Vladyslav
 */
class EasybillHelper extends Easybill {

    /**
     * if dont have api key we will use my
     */
    const API_CODE = "72371npsnezgpe4svbkjhsoon1n6g2sevlcmzr5cwujcyeujg5ton8200gx78fbt";

    /**
     * if we dont have defaultg tax we will use it
     */
    const TAX = 12;

    /**
     * if we dont have wsdl path in the config file
     */
    const WSDL = __DIR__ . "/../wsdl/soap.easybill.wsdl";

    /**
     * if we dont have default currency in the config file we will use it
     */
    const CURRENCY = 'EUR';

    /**
     *
     * @var Database
     */
    protected $db;

    /**
     *
     * @var \stdClass
     */
    private $config;

    /**
     *
     * @var \SoapClient 
     */
    private $client;

    public function Connect() {
        date_default_timezone_set('UTC');
        ini_set("soap.wsdl_cache_enabled", "0");
        ini_set("soap.wsdl_cache", "0");
        ini_set("display_errors", "On");
        ini_set("track_errors", "On");

        $this->config = $this->yaml->GetConfigurations(__DIR__ . "/../Config/EasybillConfig.yml");

        $this->config->easybill_code ? $apiCode = $this->config->easybill_code : $apiCode = self::API_CODE;
        $this->config->easybill_wsdl ? $wsdl = $this->config->easybill_wsdl : $wsdl = self::WSDL;

        $client = new SoapClient(__DIR__ . $wsdl, array("trace" => 1, "exceptions" => 1));
        $client->__setSoapHeaders(new SoapHeader("http://www.easybill.de/webservice", "UserAuthKey", $apiCode));
        if(!$this->client) {
            $this->client = $client;
        } else {
            throw new PowerplayException(MOD_EASYBILL_CONNECTION_PROBLEM, 'user');
        }
    }

    private function PrepareUser(Users $user = null) {
        if(is_null($user)) {
            $user = @unserialize($this->session->get('user_auth'));
            if(!is_object($user)) {
                throw new PowerplayException(MOD_USER_NULL);
            }
        }
        $address = $this->db->Select(['*', 'user_address', ['user_address_id' => $user->getUserAddressId()]])[0];
        $countryRegionCity = $this->db->Select([['name', 'countryId', 'regionId'],
                    'cities', ['cityId' => $address->city_id]])[0];

        $country = $this->db->Select([['code'], 'country', ['countryId' => $countryRegionCity->countryId]])[0]->code;


        $tax = $this->db->Select([['vat_rate'], 'powerplay_vat',
                    ['country_id' => $countryRegionCity->countryId]])[0]->vat_rate;

        !is_object($tax) ? $this->config->default_tax ? $tax = $this->config->default_tax : $tax = self::TAX : '';

        $easyBillClient = new stdClass();
        $easyBillClient->salutation = null;
        $easyBillClient->firstName = $user->getUserFirstName();
        $easyBillClient->lastName = $user->getUserLastName();
        $easyBillClient->email = $user->getUserEmail();
        $easyBillClient->street = $address->street;
        $easyBillClient->zipCode = $address->postal_code;
        $easyBillClient->city = $countryRegionCity->name;
        $easyBillClient->country = !$country ? 'DE' : $country;
        $easyBillClient->taxOptions = $tax;

        return $easyBillClient;
    }

    private function PrepareDoc($userId) {
        
    }

    public function CreateCustomer($userId) {

        if(!is_int($userId)) {
            throw new PowerplayException(MOD_USER_ID);
        }
        $EBUser = $this->PrepareUser();
        try {
            $response = $this->client->setCustomer($EBUser);
            if($response) {
                if($this->CheckEasyBillId($userId, $response->customerNumber)) {
                    $this->db->Update([['easybill_id' => $response->customerNumber],
                        'powerplay_users',
                        ['user_id' => $userId]]);
                }
                return true;
            } else {
                return false;
            }
        } catch(PowerplayException $ex) {
            die;
        }

        return $response;
    }

    public function getCustomer($userId) {
        if(!is_int($userId)) {
            throw new PowerplayException(MOD_USER_ID);
        }
        !$this->client ? $this->Connect() : '';

        $easyBillId = $this->db->Select([['easybill_id'], 'powerplay_users', ['user_id' => $userId]])[0]->easybill_id;

        if($easyBillId and $this->EasyBillCheckCustomer((int) $easyBillId)) {
            $user = $this->client->getCustomerByCustomerNumber($easyBillId);
            return $user;
        } else {
            $this->CreateCustomer($userId);
            return $this->getCustomer($userId);
        }
    }

    public function CreateDocument($userId, $type = 'OFFER', $offerId = null) {
        if(!is_int($userId)) {
            throw new PowerplayException(MOD_USER_ID);
        }
        is_null($offerId) ? $this->config->default_currency ? $currency = $this->config->default_currency : $currency = self::CURRENCY : $currency = $this->getCurrency($offerId);

        $customer = $this->getCustomer($userId);
        $templateName = strtoupper(explode('-', $this->session->get('language'))[0]);
        $type == 'OFFER' ? $statusType = 'OPEN' : $statusType = 'PAYED';

        $newDoc = new stdClass();
        $newDoc->customerID = $customer->customerID;
        $newDoc->documentType = $type;
//        $newDoc->bankTransfer = 'EU';// for bank transfer. add something like post
        $newDoc->templateName = $templateName; //  language 
        $newDoc->statusType = $statusType; //  status type
        $newDoc->currency = $currency; // currency
        $newDoc->documentTitle = "New " . strtolower($type);
//        $newDoc->text = "test Text here";
        $newDoc->documentPosition = [];
        $newDoc->documentPosition[] = $this->DocPosition();
        $newDoc->documentPosition[] = array(
            "positionType" => "TEXT",
            "itemDescription" => "LivingInternet"
        );
        $newDoc->signDocument = true;
        $newDoc->sendasemail = false;
        $newDoc->sendaspost = false;

        $response = $this->client->createDocument($newDoc);

        $type === 'OFFER' ? : $this->AddPayment($response->document->documentID, $offerId);

        return $response->document;
    }

    public function AddPayment($documentId, $offerId = 1) {
        !$this->client ? $this->Connect() : '';

//        $info = $this->getPaymentInformation($offerId);

        $payment = new stdClass();
        $payment->documentID = $documentId;
        $payment->amount = 133;
        $payment->paymentdate = date("Y-m-d");
        $payment->paymenttype = "PayPal";
        $payment->notice = "";
        $payment->payed = true;
        return $this->client->addDocumentPayment($payment);
    }

    public function getDocument($docId) {

        $response = $this->client->getDocumentPDF($docId);
        $this->downloadDoc($response);
    }

    public function EasyBillCheckCustomer($customerId) {
        if(!is_int($customerId)) {
            throw new PowerplayException(MOD_USER_ID);
        }
        !$this->client ? $this->Connect() : '';

        $customer = $this->client->getCustomerByCustomerNumber($customerId);

        if($customer)
            return true;
        return false;
    }

    private function downloadDoc($response) {
        if(!$response) {
            throw new PowerplayException('asd'); //chenge
        }
        $pdf = \Config::$documentsPath . "/" . $response->filename ;
        if(!file_exists($pdf)) {
            touch($pdf);
        }
        $handle = fopen($pdf, "w");
        fwrite($handle, base64_decode($response->file));
        fclose($handle);

        if(!file_exists($pdf)) {
            header("Content-type: application/pdf");
            header("Content-Disposition: attachment; filename=\"" . $response->fileName . "\"");
            readfile($pdf);
            unlink($pdf);
        }
    }

    private function DocPosition() {
        $pos = new stdClass();
        $pos->positionType = "POSITION";
        $pos->itemNumber = "Test";
        $pos->itemDescription = "JustTest";
        $pos->count = 10;
//        $pos->unit = "hrenEgo";
        $pos->singlePriceNetto = 100000;
        $pos->ustPercent = 34;
//        $pos->discount = 12;
        $pos->discountType = "PERCENT";

        return $pos;
    }

    private function CheckEasyBillId($userId, $ebId) {
        !$this->db ? $this->db = new Database() : '';

        if(!is_int($userId)) {
            throw new PowerplayException(MOD_USER_ID);
        }
        $c = $this->db->Count('*', 'powerplay_users', ['easybill_id' => $ebId]);
        if($c <= 0) {
            return true;
        }
        return false;
    }

    private function getCurrency($offerId) {
        if(!is_int($offerId)) {
            throw new PowerplayException(MOD_EMPTY);
        }
        $offer = $this->db->Select([['code'], 'powerplay_currency', ['id' => $this->db->Select([[
                        'currency_id'], 'powerplay_offers', ['id' => $offerId]])[0]->currency_id]])[0];

        if(is_object($offer)) {
            return strtoupper($offer->code);
        }
        return self::CURRENCY;
    }

    public function getPaymentInformation($offerId) {

//        $offer = $this->db->Join(['`powerplay_offers`, ``']);
        $query = "SELECT * FROM `powerplay_offers` LEFT JOIN `powerplay_users` ON `powerplay_offers`.`user_id` = `powerplay_users`.`user_id`"
                . " LEFT JOIN `user_accounts` ON `powerplay_users`.`account_id` = `user_accounts`.`id`"
                . " RIGHT JOIN `account_incomes` ON `account_incomes`.`account_id` = `user_accounts`.`id`"
                . " INNER JOIN `payments_variants` ON `payments_variants`.`id` = `account_incomes`.`payment_variant_id` WHERE `powerplay_offers`.`id` = $offerId";
        $offer = $this->db->Execute($query);
        var_dump($offer);
        die;
    }

}
