<?php

namespace Modules\Products;

use PowerPlay\Module;

/**
 * @author Pavel Petrov
 */
class Products extends Module {

    use \Modules\Products\Traits\traitHr,
        \Modules\Products\Traits\traitHrTech,
        \Modules\Products\Traits\traitHrCategory;

    public function __construct() {
        parent::__construct();
    }

    public function Render($args) {
        echo '<pre>' . print_r($args, 1) . '</pre>';
        die;
    }

}
