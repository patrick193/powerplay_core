<?php

namespace Modules\Products\Helpers;

use \Modules\Products\Entity\ProductHr;
use Modules\Products\Products;
use PowerPlay\PowerplayException\PowerplayException;

/**
 * This model is store, update and delete data of hr product
 * 
 * @author Pavel Petrov
 */
class HrHelper extends Products {

    /**
     * Get all prdocutsHr
     * 
     * @return ProductHr
     */
    public function Index() {
        $this->db->setExecute(false);
        $query = $this->db->Select(['*', 'products_hr']);
        return $this->db->Execute($query, ProductHr::class);
    }

    /**
     * SKILLS MUST BE IN FORMAT SHOWN BELOW (SKILL_ID = LEVEL)
     * $parameters['skills'] = array(
     *      '1' => 'elementary',
     *      '2' => 'expert',
     *      '3' => 'medium');
     * 
     * 
     * PROJECTS MUST BE IN FORMAT SHOWN BELOW
     * $parameters['projects'] = array(
     *      array(
     *          'name'        => 'PowerPlay',
     *          'description' => 'PowerPlay project',
     *          'link'        => 'powerplay.de',
     *          'part'        => 'PHP Developer'
     *      ));
     * 
     * EDUCATION MUST BE IN FORMAT SHOWN BELOW
     * $parameters['education'] = array(
     *      array(
     *          'city_id'        => '1',
     *          'year_start' => '2010',
     *          'year_end'        => '2015',
     *          'studied_at'        => 'KhNEU',
     *          'degree'        => 'specialist'
     *      ));
     * 
     * @param array $parameters
     */
    public function Create($parameters) {
        $end_date   = new \DateTime($parameters['end_date']);
        $start_date = new \DateTime($parameters['start_date']);

        $product = new ProductHr();

        $product->setName($parameters['name']);
        $product->setDescription($parameters['description']);
        $product->setCode();
        $product->setEndDate($end_date->format('Y-m-d H:i:s'));
        $product->setStartDate($start_date->format('Y-m-d H:i:s'));
        $product->setPartTime($parameters['part_time']);
        $product->setFullTime($parameters['full_time']);
        if (isset($_FILES['photo'])) {
            $product->setPhoto($_FILES['photo']);
        }
        $product->setIsLocked(false);
        $product = $product->Store();

        if (isset($parameters['skills'])) {
            $this->InsertSkills($product->getProductsHrId(), $parameters['skills']);
        }
        if (isset($parameters['projects'])) {
            $this->InsertProjects($product->getProductsHrId(), $parameters['projects']);
        }
        if (isset($parameters['education'])) {
            $this->InsertEducation($product->getProductsHrId(), $parameters['education']);
        }
    }

    /**
     * Returns only one HR Product
     * 
     * @param int $id
     * @return ProductHr
     */
    public function Show($id) {
        $this->db->setExecute(false);
        $query = $this->db->Select(['*', 'products_hr', ['products_hr_id' => $id]]);
        return $this->db->Execute($query, ProductHr::class)[0];
    }

    /**
     * SKILLS MUST BE IN FORMAT SHOWN BELOW
     * SKILL_ID = LEVEL
     * 
     * $parameters['skills'] = array(
     *      '1' => 'elementary',
     *      '2' => 'expert',
     *      '3' => 'medium');
     * 
     * @param array $parameters
     */
    public function Edit($parameters) {
        $this->db->setExecute(false);
        $query   = $this->db->Select(['*', 'products_hr', ['products_hr_id' => $parameters['id']]]);
        $product = $this->db->Execute($query, ProductHr::class)[0];
        if (isset($parameters['name'])) {
            $product->setName($parameters['name']);
        }
        if (isset($parameters['description'])) {
            $product->setDescription($parameters['description']);
        }
        if (isset($parameters['start_date'])) {
            $start_date = new \DateTime($parameters['start_date']);
            $product->setStartDate($start_date->format('Y-m-d H:i:s'));
        }
        if (isset($parameters['end_date'])) {
            $end_date = new \DateTime($parameters['end_date']);
            $product->setEndDate($end_date->format('Y-m-d H:i:s'));
        }
        if (isset($parameters['full_time'])) {
            $product->setFullTime($parameters['full_time']);
        }
        if (isset($parameters['part_time'])) {
            $product->setPartTime($parameters['part_time']);
        }
        if (isset($_FILES['photo'])) {
            $product->setPhoto($_FILES['photo']);
        }

        if (isset($parameters['skills'])) {
            $this->UpdateSkills($product->getProductsHrId(), $parameters['skills']);
        }

        $product->Store();
    }

    /**
     * This method updates information of skills of specific product 
     * (IF NEW -> Insert)
     * (IF UPDATED -> Update)
     * (IF in DB has a skills 
     *      but not from form in this request -> makes it locked)
     * 
     * @param int $productId
     * @param array $skills
     */
    private function UpdateSkills($productId, $skills) {
        $skillsToLock = array();
        foreach ($skills as $skillId => $level) {
            $skillsToLock[] = $skillId;
            if ($this->db->Count('*', 'skills', ['product_hr_id' => $productId, 'skill_technology_id' => $skillId])) {
                $this->db->Execute("UPDATE skills SET level = '$level', isLocked = false "
                        . "WHERE product_hr_id = $productId "
                        . "AND skill_technology_id = $skillId");
            } else {
                $this->InsertSkills($productId, [$skillId => $level]);
            }
        }
        $skillsToLock = implode(',', $skillsToLock);
        $this->db->Execute("UPDATE skills SET isLocked = true "
                . "WHERE product_hr_id = $productId "
                . "AND skill_technology_id NOT IN ($skillsToLock)");
    }

    /**
     * Only inserts new skills for specific product
     * 
     * @param int $productId
     * @param array $skills
     * @throws PowerplayException
     */
    public function InsertSkills($productId, $skills) {
        if (!$productId or ! count($skills)) {
            throw new PowerplayException(MOD_EMPTY);
        }
        foreach ($skills as $skillId => $level) {
            $parameters = array(
                'skill_technology_id' => $skillId,
                'product_hr_id'       => $productId,
                'level'               => $level,
            );
            $this->db->Insert([$parameters, 'skills']);
        }
    }

    /**
     * Only inserts new projects for specific product
     * 
     * @param int $productId
     * @param array $projects
     * @throws PowerplayException
     */
    public function InsertProjects($productId, $projects) {
        if (!$productId or ! count($projects)) {
            throw new PowerplayException(MOD_EMPTY);
        }
        foreach ($projects as $project) {
            $projectParameters = array(
                'name'        => $project['name'],
                'description' => $project['description'],
                'link'        => $project['link'],
                'part'        => $project['part'],
            );

            $projectId = $this->db->Insert([$projectParameters, 'project_examples']);

            $parameters = array(
                'product_hr_id'      => $productId,
                'project_example_id' => $projectId,
            );

            $this->db->Insert([$parameters, 'product_hr_to_projects']);
        }
    }

    /**
     * Only inserts new education positions for specific product
     * 
     * @param int $productId
     * @param array $education
     * @throws PowerplayException
     */
    public function InsertEducation($productId, $education) {
        if (!$productId or ! count($education)) {
            throw new PowerplayException(MOD_EMPTY);
        }

        foreach ($education as $place) {
            $educationParameters = array(
                'city_id'    => $place['city_id'],
                'year_start' => $place['year_start'],
                'year_end'   => $place['year_end'],
                'studied_at' => $place['studied_at'],
                'degree'     => $place['degree'],
            );

            $educationId = $this->db->Insert([$educationParameters, 'education']);

            $parameters = array(
                'product_hr_id' => $productId,
                'education_id'  => $educationId,
            );

            $this->db->Insert([$parameters, 'product_hr_to_education']);
        }
    }

    public function Delete($id) {
        return $this->db->Delete(["products_hr", ['products_hr_id' => $id]]);
    }

}
