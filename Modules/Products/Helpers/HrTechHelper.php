<?php

namespace Modules\Products\Helpers;

use Modules\Products\Products;

/**
 * @author Pavel Petrov
 */
class HrTechHelper extends Products {

    public function Index() {
        return $this->db->Select(['*', 'skills_technology']);
    }

    public function Create($parameters) {
        $values = array(
            'technology'             => $parameters['technology'],
            'technology_category_id' => $parameters['category']
        );
        return $this->db->Insert([$values, 'skills_technology']);
    }

    public function Show($id) {
        return $this->db->Select(['*', 'skills_technology', ['technology_id' => $id]])[0];
    }

    public function Edit($parameters) {
        $values = array(
            'technology'             => $parameters['technology'],
            'technology_category_id' => $parameters['category']
        );
        return $this->db->Update([$values, 'skills_technology', ['technology_id' => $parameters['id']]]);
    }

    public function Delete($id) {
        return $this->db->Delete(['skills_technology', ['technology_id' => $id]]);
    }

}
