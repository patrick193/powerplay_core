<?php

namespace Modules\Products\Helpers;

use Modules\Products\Products;

/**
 * @author Pavel Petrov
 */
class HrCatHelper extends Products {

    public function Index() {
        return $this->db->Select(['*', 'technology_categories']);
    }

    public function Create($parameters) {
        return $this->db->Insert([['name' => $parameters['name']], 'technology_categories']);
    }

    public function Show($id) {
        return $this->db->Select(['*', 'technology_categories', ['technology_category_id' => $id]])[0];
    }

    public function Edit($parameters) {
        return $this->db->Update([['name' => $parameters['name']], 'technology_categories', ['technology_category_id' => $parameters['id']]]);
    }

    public function Delete($id) {
        return $this->db->Delete(['technology_categories', ['technology_category_id' => $id]]);
    }

}
