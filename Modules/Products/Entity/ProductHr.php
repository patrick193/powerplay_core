<?php

namespace Modules\Products\Entity;

use PowerPlay\PowerplayException\PowerplayException;
use Modules\Authorization\Security\Security;
use PowerPlay\Database;

/**
 * @author Pavel Petrov
 */
class ProductHr {
    
    /**
     *
     * @var Database 
     */
    protected $db;
    
    public function __construct() {
        !$this->db ? $this->db = new Database() : '';
        
    }

    protected $products_hr_id;
    protected $name;
    protected $description;
    protected $start_date;
    protected $end_date;
    protected $full_time;
    protected $part_time;
    protected $code;
    protected $photo;
    protected $isLocked;
    protected $price;

    public function getPrice() {
        return $this->price;
    }

    public function setPrice($price) {
        $this->price = $price;
    }

    public function __toString() {
        return $this->name;
    }

    public function setProductsHrId($products_hr_id) {
        $this->products_hr_id = $products_hr_id;
    }

    public function getProductsHrId() {
        return $this->products_hr_id;
    }

    public function getName() {
        return $this->name;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getStartDate() {
        return $this->start_date;
    }

    public function getEndDate() {
        return $this->end_date;
    }

    public function getFullTime() {
        return $this->full_time;
    }

    public function getPartTime() {
        return $this->part_time;
    }

    public function getCode() {
        return $this->code;
    }

    public function getPhoto() {
        return $this->photo;
    }

    public function getIsLocked() {
        return $this->isLocked;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setStartDate($start_date) {
        $this->start_date = $start_date;
    }

    public function setEndDate($end_date) {
        $this->end_date = $end_date;
    }

    public function setFullTime($full_time) {
        $this->full_time = $full_time;
    }

    public function setPartTime($part_time) {
        $this->part_time = $part_time;
    }

    public function setCode() {
        $this->code = $this->getUniqueCode();
    }

    /**
     * This method generate new unique generated code for product
     * 
     * @return string $code
     */
    private function getUniqueCode() {
        $code = strtoupper(Security::GeneratePassword());
        $count = $this->db->Count("code", "products_hr", ["code" => $code]);

        if($count > 0) {
            $code = $this->getUniqueCode();
        }

        return $code;
    }

    /**
     * This method moving image from tmp dir and stores set image name to entity
     * 
     * @param array $photo
     * @throws PowerplayException
     */
    public function setPhoto($photo) {
        @unlink(\Config::$imagePath . 'products_hr/' . $this->photo);

        $tmpName = $photo['tmp_name'];
        $type = $photo['type'];
        $mime = explode('/', $type);
        $imageName = $this->code . '.' . end($mime);
        $uploadTo = \Config::$imagePath . 'products_hr/' . $imageName;

        if(!reset($mime) == 'image') {
            throw new PowerplayException(MOD_IMAGE);
        }
        if(!move_uploaded_file($tmpName, $uploadTo)) {
            throw new PowerplayException(MOD_ACCESS);
        }

        $this->photo = $imageName;
    }

    public function setIsLocked($isLocked) {
        $this->isLocked = $isLocked;
    }

    /**
     * This method store data from $this to DB
     * 
     * @return Product
     */
    public function Store() {

        $parameters = [];
        foreach($this as $key => $value) {
            $parameters[$key] = $value;
        }
        unset($parameters['db']);

        if(!$this->getProductsHrId()) {
            $result = $this->db->Insert([$parameters, 'products_hr']);
            if($result and ! is_bool($result)) {
                $this->setProductsHrId($result);
            } else {
                throw new PowerplayException(MOD_ERR_INSERT);
            }
        } else {
            $result = $this->db->Update([$parameters, 'products_hr', ['products_hr_id' => $this->products_hr_id]]);
        }

        return $this;
    }

    /**
     * 
     * @param mixed $productId
     * @return mixed
     * @throws PowerplayException
     */
    public function FindById($productId) {
        if(!($productId) or $productId == 0) {
            throw new PowerplayException(MOD_EMPTY);
        }

        if(is_array($productId)) {
            $products = [];
            foreach($productId as $productOne) {
                $this->db->setExecute(false);
                $q = $this->db->Select(['*', 'products_hr', ['products_hr_id' => $productId]]);
                $product = $this->db->Execute($q, ProductHr::class)[0];
                array_push($products, $product);
            }
            if($products) {
                return $products;
            }
            return false;
        } else {
            $this->db->setExecute(false);
            $q = $this->db->Select(['*', 'products_hr', ['products_hr_id' => $productId]]);
            $product = $this->db->Execute($q, ProductHr::class)[0];
            if($product) {
                return $product;
            }
            return false;
        }
    }

}
