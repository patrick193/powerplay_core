<?php

namespace Modules\Products\Traits;

use Modules\Products\Helpers\HrHelper;
use PowerPlay\PowerplayException\PowerplayException;

/**
 * This trait provides all needed actions for HR product activity
 * CRUD
 * 
 * @author Pavel Petrov
 */
trait traitHr {

    public function actionHr() {
        $hrHelper = new HrHelper();
        $products = $hrHelper->Index();

        $this->Render($products);
    }

    public function actionHrNew($args) {
        $hrHelper = new HrHelper();
        if (!is_array($args)) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }

        //TODO returns exceptions to SMARTY
        $this->CheckForRequired($args);

        $createdProduct = $hrHelper->Create($args);

        $this->Render($createdProduct);
    }

    public function actionHrDelete($args) {
        $hrHelper = new HrHelper();
        if (!$args['id']) {
            throw new PowerplayException(MOD_EMPTY);
        }

        $result = $hrHelper->Delete($args['id']);

        $this->Render($result);
    }

    public function actionHrEdit($args) {
        if (!is_array($args)) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        if (!isset($args['id'])) {
            throw new PowerplayException(MOD_EMPTY);
        }
        $hrHelper       = new HrHelper();
        $updatedProduct = $hrHelper->Edit($args);

        $this->Render($updatedProduct);
    }

    public function actionHrShow($args) {
        if (!isset($args['id'])) {
            throw new PowerplayException(MOD_EMPTY);
        }

        $hrHelper = new HrHelper();
        $product  = $hrHelper->Show($args['id']);

        $this->Render($product);
    }

    private function CheckForRequired($args) {
        if (!$args['name']) {
            throw new PowerplayException(MOD_EMPTY);
        }
        if (!$args['description']) {
            throw new PowerplayException(MOD_EMPTY);
        }
        if (!$args['end_date']) {
            throw new PowerplayException(MOD_EMPTY);
        }
        if (!$args['start_date']) {
            throw new PowerplayException(MOD_EMPTY);
        }
        if (!$args['part_time']) {
            throw new PowerplayException(MOD_EMPTY);
        }
        if (!$args['full_time']) {
            throw new PowerplayException(MOD_EMPTY);
        }
    }

}
