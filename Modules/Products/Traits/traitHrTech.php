<?php

namespace Modules\Products\Traits;

use Modules\Products\Helpers\HrTechHelper;
use PowerPlay\PowerplayException\PowerplayException;

/**
 * Techonologies and categories of technologies for hr products skills
 * CRUD
 * 
 * @author Pavel Petrov
 */
trait traitHrTech {

    public function actionHrTech() {
        $technologyHelper = new HrTechHelper();
        $result           = $technologyHelper->Index();

        $this->Render($result);
    }

    public function actionHrTechNew($args) {
        if (!is_array($args)) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        if (!isset($args['category'])) {
            throw new PowerplayException(MOD_EMPTY);
        }
        if (!isset($args['technology'])) {
            throw new PowerplayException(MOD_EMPTY);
        }

        $technologyHelper = new HrTechHelper();
        $result           = $technologyHelper->Create($args);

        $this->Render($result);
    }

    public function actionHrTechDelete($args) {
        if (!isset($args['id'])) {
            throw new PowerplayException(MOD_EMPTY);
        }
        $technologyHelper = new HrTechHelper();
        $result           = $technologyHelper->Delete($args['id']);

        $this->Render($result);
    }

    public function actionHrTechEdit($args) {
        if (!isset($args['id'])) {
            throw new PowerplayException(MOD_EMPTY);
        }
        if (empty($args['technology'])) {
            throw new PowerplayException(MOD_EMPTY);
        }
        if (empty($args['category'])) {
            throw new PowerplayException(MOD_EMPTY);
        }

        $technologyHelper = new HrTechHelper();
        $result           = $technologyHelper->Edit($args);

        $this->Render($result);
    }

    public function actionHrTechShow($args) {
        if (!isset($args['id'])) {
            throw new PowerplayException(MOD_EMPTY);
        }
        $technologyHelper = new HrTechHelper();
        $result           = $technologyHelper->Show($args['id']);

        $this->Render($result);
    }

}
