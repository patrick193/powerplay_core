<?php

namespace Modules\Products\Traits;

use Modules\Products\Helpers\HrCatHelper;
use PowerPlay\PowerplayException\PowerplayException;

/**
 * Categories for HR technologies
 * CRUD
 * 
 * @author Pavel Petrov
 */
trait traitHrCategory {

    public function actionHrCat() {
        $hrCatHelper = new HrCatHelper();
        $result      = $hrCatHelper->Index();
        $this->Render($result);
    }

    public function actionHrCatNew($args) {
        if (!isset($args['name'])) {
            throw new PowerplayException(MOD_EMPTY);
        }
        $hrCatHelper = new HrCatHelper();
        $result      = $hrCatHelper->Create($args);

        $this->Render($result);
    }

    public function actionHrCatShow($args) {
        if (!isset($args['id'])) {
            throw new PowerplayException(MOD_EMPTY);
        }
        $hrCatHelper = new HrCatHelper();
        $result      = $hrCatHelper->Show($args['id']);

        $this->Render($result);
    }

    public function actionHrCatEdit($args) {
        if (!isset($args['id'])) {
            throw new PowerplayException(MOD_EMPTY);
        }
        if (!isset($args['name'])) {
            throw new PowerplayException(MOD_EMPTY);
        }
        $hrCatHelper = new HrCatHelper();
        $result      = $hrCatHelper->Edit($args);

        $this->Render($result);
    }

    public function actionHrCatDelete($args) {
        if (!isset($args['id'])) {
            throw new PowerplayException(MOD_EMPTY);
        }
        $hrCatHelper = new HrCatHelper();
        $result      = $hrCatHelper->Delete($args['id']);

        $this->Render($result);
    }

}
