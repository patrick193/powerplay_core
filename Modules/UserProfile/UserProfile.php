<?php

namespace Modules\UserProfile;

use PowerPlay\Module;
use Modules\UserProfile\Helpers\UserProfileHelper;
use PowerPlay\PowerplayException\PowerplayException;

/**
 * @author Pavel Petrov
 */
class UserProfile extends Module {

    public function actionUserUpdate($args) {
        if (!is_array($args)) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        if ($args['language'] && !(int) $args['language']) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        if ($args['city'] && !(int) $args['city']) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        $userProfile = new UserProfileHelper();
        $result      = $userProfile->UpdateUser($args);

        $this->Render($result);
    }

    public function actionMain() {
        $userProfile = new UserProfileHelper();
        $result      = $userProfile->GetUserData();

        $this->Render($result);
    }

    

}
