<?php

namespace Modules\UserProfile\Helpers;

use PowerPlay\Database;
use PowerPlay\PowerplayException\PowerplayException;
use Modules\Authorization\Security\Security;
use Modules\Authorization\Models\AuthUser;

/**
 * @author Pavel Petrov
 */
class UserProfileHelper {

    /**
     * @var \PowerPlay\Database
     */
    private $db;

    /**
     * @var \PowerPlay\Users
     */
    private $user;

    public function __construct() {
        $ses           = new \PowerPlay\Session();
        $currentUser   = unserialize($ses->get('user_auth')); // session
        $this->db      = new Database();
        $this->db->Connect();
        $queryAuthUser = "SELECT * FROM powerplay_users WHERE user_id = " . $currentUser->getUserId();
        $users         = $this->db->Execute($queryAuthUser, AuthUser::class);
        $this->user    = end($users);
    }

    /**
     * Method for update userdata
     * 
     * @param array $args
     * @return User
     * @throws Exception
     */
    public function UpdateUser($args) {
        $this->user->setUserFirstName($args['first_name']);
        $this->user->setUserLastName($args['last_name']);
        $this->user->setCompanyName($args['company']);
        $this->user->setCellPhone($args['cell_phone']);
        $this->user->setUserEmail($args['email']);
        $this->user->setCountryId($args['country']);
        $this->user->setRegionId($args['region']);
        $this->user->setCity($args['city']);
        $this->user->setStreet($args['street']);
        $this->user->setPostalCode($args['postal_code']);
        $this->user->setLanguageId($args['language']);
        $this->user->Execute($this->user, (int) $this->user->getUserId());

        if ($args['password']) {
            $encryptedPassword = Security::GetEncrypted($args['password']);
            $this->db->Execute("UPDATE powerplay_users SET user_password = '$encryptedPassword' WHERE user_id = " . (int) $this->user->getUserId());
        }
        if ($_FILES['photo']) {
            $this->setImage($_FILES['photo']);
        }

        return true;
    }

    /**
     * Method for setting image to DB and move it to application image directory
     * 
     * @param resource $image
     * @throws PowerplayException
     */
    private function setImage($image) {
        $tmpName   = $image['tmp_name'];
        $type      = $image['type'];
        $mime      = explode('/', $type);
        $imageName = $this->user->getUserEmail() . '.' . end($mime);
        $uploadTo  = \Config::$imagePath . 'user_profile/' . $imageName;

        if (!reset($mime) == 'image') {
            throw new PowerplayException(MOD_IMAGE);
        }
        if (!move_uploaded_file($tmpName, $uploadTo)) {
            throw new PowerplayException(MOD_ACCESS);
        }

        $this->db->Execute("UPDATE powerplay_users SET photo = '" . $imageName . "' WHERE user_id = " . (int) $this->user->getUserId());
    }

    /**
     * Method gets user data and returns it
     * 
     * @return object stdClass
     */
    public function GetUserData() {
        $userId    = $this->user->getUserId();
        $selection = implode(',', array(
            'user_id', 'user_first_name', 'user_last_name',
            'user_email', 'cell_phone', 'company_name',
            'language_id', 'parent_id', 'role_id',
            'cityId', 'regionId', 'countryId',
            'account_id', 'photo', 'street'
        ));

        $query = "SELECT $selection FROM powerplay_users pu "
                . "INNER JOIN user_address ua ON ua.user_address_id = pu.user_address_id "
                . "INNER JOIN cities ci ON ci.cityId = ua.city_id "
                . "WHERE pu.user_id = $userId "
                . "AND pu.isLocked = 0";

        $result = $this->db->Execute($query);
        return end($result);
    }

}
