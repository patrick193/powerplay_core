<?php

namespace Modules\system_lang;

use Modules\system_lang\System;

/**
 * Description of SystemCore
 *
 * @author Developer Pohorielov Vladyslav
 */
class SystemCore extends System {

    /**
     * Default Language for the core
     * @var string 
     */
    private $defaultLanguage;
    
    public function __construct($languageCode) {

        parent::__construct($languageCode);
        
        !$this->defaultLanguage ? $this->defaultLanguage = parent::DEFAULT_LANGUAGE : '';
        if(!$languageCode) {
            $languageCode = $this->defaultLanguage;
        }
        $this->filePath = \Config::$langDir . "system_core/$languageCode/$languageCode.ini";
    }

    /**
     * {@inheritDoc}
     */
    public function UseLanguage() {
           if(file_exists($this->filePath)) {
            $language = parse_ini_file($this->filePath);
            $this->DefineLanguage($language);
            return true;
        }
        return false;
    }

    /**
     * 
     * {@inheritDoc}
     */
    protected function DefineLanguage($parsedIni) {
        parent::DefineLanguage($parsedIni);
    }

}
