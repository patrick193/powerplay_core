<?php

namespace Modules\UserManagement;

use PowerPlay\Module;
use Modules\UserManagement\Helpers\UserHelper;

/**
 * User Managment
 * We should change render and args befor render
 * @author Developer Pohorielov Vladyslav
 */
class UserManagement extends Module
{

    public function actionAjaxGetChildren($args)
    {
        if(!$args['user_id'] or $args['user_id'] == 0) {
            throw new \PowerPlay\PowerplayException\PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        $userId   = trim(strip_tags($args['user_id']));
        $uh       = new UserHelper();
        $children = $uh->ajaxGetChildren($userId);
        if(!is_array($children)) {
            echo '{error: This user does not has customeers}';
            exit;
        } else {
            $children = $this->__objectToArray($children);
            echo json_encode($children);
            exit;
        }
    }

    public function actionAddUser($args)
    {
        if(!is_array($args)) {
            throw new \PowerPlay\PowerplayException\PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        $helper = new UserHelper();
        $helper->AddUser($args);
//        $users = $helper->getAll();
//        return $users;
//        $this->actionMain();
    }

    public function actionLogout()
    {
        $user = @unserialize($this->session->get('user_auth'));
        if($user) {
            $this->session->DestroySession();
            $this->Render('', '', 'login/EnterPage/index');
        }
    }

    public function actionGoUp()
    {
        $helper = new UserHelper();
        $helper->goUp();
        $this->actionMain();
    }

    public function actionEditUser($args)
    {
        if(!is_array($args)) {
            throw new \PowerPlay\PowerplayException\PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        $helprer = new UserHelper();
        $helprer->EditUser($args);
        $this->actionMain();
    }

    public function actionAuth($args)
    {
        if(!is_array($args)) {
            throw new \PowerPlay\PowerplayException\PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        $helprer = new UserHelper();
        $helprer->AuthUser($args['parent_id'], $args['customer_id']);

        $users = $helprer->getAll(true);

        $authUser = unserialize($this->session->get('user_auth'));

        $this->moduleName  = 'User Management';
        $this->breadscrumb = ['home'                                                             => '/dashboard',
            'usermanagement'                                                   => '/usermanagement',
            $authUser->getUserFirstName() . ' ' . $authUser->getUserLastName() => ''];

        $this->Render('users', $users, 'UserManagement/index');
    }

    public function actionAuthBack()
    {
        $uh = new UserHelper();
        $uh->AuthBack();
        $this->actionMain();

        $users = $uh->getAll();
        $this->Render('users', $users, 'UserManagement/index');
    }

    public function actionMain()
    {
        $helper = new UserHelper();
        $users  = $helper->getAll(true);

        $this->moduleName  = 'User Management';
        $this->breadscrumb = ['home' => '/dashboard', 'usermanagement' => ''];

        $this->Render('users', $users, 'UserManagement/index');
    }

    public function actionBlockUser($args)
    {
        if(!isset($args['user_id']) or empty($args['user_id']) or $args['user_id'] == 0) {
            throw new \PowerPlay\PowerplayException\PowerplayException(MOD_EMPTY);
        }
        $uh  = new UserHelper();
        $res = $uh->BlockUser((int) $args['user_id']);
        print($res);
    }

    public function actionDelete($args)
    {
        if(!is_array($args) or ! isset($args['user_id']) or empty($args['user_id'])) {
            throw new \PowerPlay\PowerplayException\PowerplayException(MOD_EMPTY);
        }
        $uh = new UserHelper();
        $uh->BlockUser((int) $args['user_id'], true);
    }

    /**
     * function convert objects to array
     * @param mixed $args
     * @return mixed
     */
    private function __objectToArray($args)
    {
        $array = [];
        if(is_array($args)) {
            foreach($args as $key => $value) {
                $array[$key] = $this->__objectToArray($value);
            }
        }
        if(is_object($args)) {
            $rc   = new \ReflectionClass($args);
            $prop = $rc->getProperties();
            foreach($prop as $property) {
                $propertyName = $property->getName();
                if(strpos($propertyName, "password") === false) {
                    $pn   = explode("_", $propertyName);
                    $anon = function() use($pn) {
                        $name = '';
                        foreach($pn as $p) {
                            $name .= ucfirst($p);
                        }
                        return $name;
                    };
                    $methodName = 'get' . $anon();
                    if(method_exists($args, $methodName)) {
                        $array[$propertyName] = $args->$methodName();
                    }
                    if(is_object($array[$propertyName])) {
                        $array[$propertyName] = $this->__objectToArray($array[$propertyName]);
                    }
                }
            }
        }

        return $array;
    }

}
