<?php

namespace Modules\UserManagement\Helpers;

use Modules\Roles\Users;
use Modules\Roles\Roles;
use PowerPlay\Session;
use PowerPlay\Database;
use PowerPlay\PowerplayException\PowerplayException;

/**
 * Description of UserHelper
 *
 * @author Developer Pohorielov Vladyslav
 */
class UserHelper
{

    private $users, $user;
    private $session;

    public function __construct()
    {
        !$this->users ? $this->users   = new Users() : '';
        !$this->session ? $this->session = new Session() : '';
    }

    /**
     * Function for adding user in the system
     * @param array $arguments Array of arguments for user<br>
     * Example:<br>
     * ['first_name' => first name, 'last_name' => last name ....]
     * @throws Exception
     */
    public function AddUser($arguments)
    {
        $this->CheckExc($arguments);
        $newUser = $this->users->Create();
        $newUser->setIsLocked(1);
        $newUser->setParentId($this->user->getUserId());
        $newUser->setRoleId($arguments['role_id']);
        $newUser->setUserEmail($arguments['email']);
        $newUser->setUserFirstName($arguments['name']);
        $newUser->setUserLastName($arguments['lastname']);
        $newUser->setLanguageId(isset($arguments['language_id']) ? $arguments['language_id'] : 1);
        $newUser->setCompanyName($arguments['company_name']);
        $newUser->setPostalCode($arguments['postal_code']);
        $newUser->setCity($arguments['city']);
        $newUser->setStreet($arguments['street']);
        $newUser->setCellPhone($arguments['cell_phone']);

        return $this->users->Execute($newUser, (int) $this->user->getUserId());
    }

    /**
     * Function for edit user information in the system
     * @param array $arguments Array of arguments for user<br>
     * Example:<br>
     * ['first_name' => first name, 'last_name' => last name ....]
     * @return boolean
     */
    public function EditUser($arguments)
    {
        $this->CheckExc($arguments);
        $newUser = $this->users->Create();
        $db      = new Database();
        $role    = $db->Select(['role_id', 'powerplay_users', ['user_id' => $arguments['user_id']]]);
        $roleId  = $role[0]->role_id;
        $newUser->setIsLocked($arguments['islocked']);
        $newUser->setParentId($this->user->getUserId());
        $newUser->setRoleId($roleId);
        $newUser->setUserEmail($arguments['email']);
        $newUser->setUserFirstName($arguments['name']);
        $newUser->setUserLastName($arguments['lastname']);
        $newUser->setUserId($arguments['user_id']);
        $newUser->setLanguageId($arguments['language_id']);
        $newUser->setCompanyName($arguments['company_name']);
        $newUser->setPostalCode($arguments['postal_code']);
        $newUser->setCity($arguments['city']);
        $newUser->setStreet($arguments['street']);
        $newUser->setCellPhone($arguments['cell_phone']);

        return $this->users->Execute($newUser, (int) $this->user->getUserId());
    }

    /**
     * Function for auth users under other user
     * @param string $userId
     * @param string $userIdAuth
     * @return boolean|int
     */
    public function AuthUser($userId, $userIdAuth)
    {
        if($userId == $userIdAuth or $userId == 0 or $userIdAuth == 0) {
            return false;
        }
        $mainUser = @unserialize($this->session->get('user_auth'));
        $role     = new Roles();
        $authUser = $this->users->Load(['user_id', $userIdAuth]);
        if($authUser) {
            $rules  = $this->users->CheckAction((int) $userId, $role->FindById((int) $authUser->getRoleId())->getRoleCode(), 'auth');
            $parent = $this->users->getParent((int) $userIdAuth);
            if($rules === true and ( $parent[0]->getUserId() == $userId) and ( $mainUser->getUserId() == $userId)) {
                $backUp = $this->session->get('user_back_up');
                if($backUp) {
                    $this->session->set('user_back_up_2', $backUp);
                    $this->session->set('user_back_up', $this->session->get('user_auth'));
                    $this->session->set('user_auth', serialize($authUser));
                } else {
                    $this->session->set('user_back_up', $this->session->get('user_auth'));
                    $this->session->Remove('user_auth');
                    $this->session->set('user_auth', serialize($authUser));
                }
            } else {
                return $this->BusinessmanToCustoer($userId, $authUser);
            }
            return 1;
        }
        return -1;
    }

    /**
     * function for log in businessman to customer
     * @param int $userId
     * @param Users $authUser
     * @return int
     */
    private function BusinessmanToCustoer($userId, $authUser)
    {
        if($authUser->getParentId() != $userId) {
            $parentUser = $this->users->Load(['user_id', $authUser->getParentId()]);
            if($parentUser->getParentId() == $userId) {
                $backUp2 = @serialize($this->users->Load(['user_id', $userId]));
                $this->session->set('user_back_up_2', $backUp2);
                $this->session->set('user_back_up', @serialize($parentUser));
                $this->session->set('user_auth', @serialize($authUser));
                return 1;
            }
            return 0;
        }
        return -1;
    }

    /**
     * Go up on the tree of users
     */
    public function goUp()
    {
        $userAuth    = @unserialize($this->session->get('user_auth'));
        $userBackUp  = @unserialize($this->session->get('user_back_up'));
        $userBackUp2 = @unserialize($this->session->get('user_back_up_2'));
        if(!is_bool($userAuth) and ! is_bool($userBackUp) and ! is_bool($userBackUp2)) {
            $this->session->set('user_auth', @serialize($userBackUp2));
            $this->session->Remove('user_back_up');
            $this->session->Remove('user_back_up_2');
        }
    }

    /**
     * Funciton for back to current user
     */
    public function AuthBack()
    {
        $backUp            = $this->session->get('user_back_up');
        $backUpSecondLevel = $this->session->get('user_back_up_2');
        $userAuth          = $this->session->get('user_auth');
        if(!isset($backUpSecondLevel) or empty($backUpSecondLevel)) {
            if((isset($backUp) and isset($userAuth)) and ( !empty($backUp) and ! empty($userAuth))) {
                $this->session->Remove('user_auth');
                $this->session->set('user_auth', $backUp);
                $this->session->Remove('user_back_up');
                return 1;
            }
        } else {
            if((isset($backUp) and isset($userAuth)) and ( !empty($backUp) and ! empty($userAuth))) {

                $this->session->set('user_auth', $this->session->get('user_back_up'));
                $this->session->set('user_back_up', $backUpSecondLevel);
                $this->session->Remove('user_back_up_2');
                return 1;
            }
        }
        return 0;
    }

    /**
     * Function to get all children
     * @return array
     * @throws Exception
     */
    public function getAll($all = false)
    {
        !$this->user ? $this->session = new Session() : '';
        $user          = @unserialize($this->session->get('user_auth'));
        if(!is_object($user)) {
            throw new PowerplayException(MOD_USER_NULL);
        }
        $this->user = $user;
        $children   = $this->user->getChild((int) $this->user->getUserId());
        if($all === true and is_array($children)) {
            foreach($children as $key => $cUser) {
                $children[$key]->setChildren($this->user->getChild((int) $cUser->getUserId()));
                $children[$key]->getChildren() === true ? $children[$key]->setChildren(0) : '';
            }
        }
        if(is_array($children)) {
            return $children;
        }
        return 0;
    }

    /**
     * Function for block a user
     * @param int $userId
     * @throws PowerplayException
     */
    public function BlockUser($userId, $hide = false)
    {
        if(!is_int($userId)) {
            throw new PowerplayException(MOD_USER_ID);
        }
        $this->CheckExc();
        $role = new Roles();
        $usr  = $this->users->Load(['user_id', $userId]);

        if($this->users->isChild((int) $this->user->getUserId(), $userId)) {
            $access = $this->users->CheckAction((int) $this->user->getUserId(), $role->FindById((int) $usr->getRoleId())->getRoleCode(), 'edit');
            if($access === true) {

                $hide === true ? $this->users->HideUser($userId) : $this->users->BlockUser($userId);
                return true;
            }
        } else {
            throw new PowerplayException(USR_USER_CHILD);
        }
    }

    /**
     * Private funtion for check all arguments and user
     * @param array $arguments
     * @throws Exception
     */
    private function CheckExc($arguments = array())
    {
        if(!is_array($arguments)) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        if(!$this->user) {
            $this->session = new Session();
            $user          = @unserialize($this->session->get('user_auth'));
            if(!is_object($user)) {
                throw new PowerplayException(MOD_USER_NULL);
            } else {
                $this->user = $user;
            }
        }
    }

    /**
     * function fore getting all children via ajax
     * @param int $userId
     * @return boolean
     */
    public function ajaxGetChildren($userId = null)
    {
        $user = @unserialize($this->session->get('user_auth'));
        if(!is_int($userId) or $userId == 0 or is_null($userId)) {
            $userId = $user->getUserId();
        }
        $children = $user->getChild((int)$userId);
        if($children){
            return $children;
        }
        return false;
    }

}
