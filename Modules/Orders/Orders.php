<?php

namespace Modules\Orders;

use PowerPlay\Module;
use PowerPlay\PowerplayException\PowerplayException;
use Modules\Orders\SubModules\Offers\Offers;

/**
 * Description of Orders
 *
 * @author Developer Pohorielov Vladyslav
 */
class Orders extends Module
{

    public function actionMain($args = [])
    {
        $this->display('Orders/Offer/index');
    }

    /**
     * Method for adding a new offer
     * @param array $args
     */
    public function actionOfferAdd($args)
    {

        $offers = new Offers();
        $offers->OfferCreate($args);
    }

    /**
     * Method for block offer after some time
     * @param array $args
     * @throws Exception
     */
    public function actionOfferBlock($args)
    {
        if(!isset($args['offer_id'])) {
            throw new Exception(MOD_WRONG_PARAMETERS_TYPE);
        }
        $offer = new Offers();
        $offer->OfferBlock((int) $args['offer_id']);
    }

    public function actionOrderAdd($args)
    {
        $offer = new Offers();
        $offer->OrderAdd($args);
    }

    public function actionSendPayment($args)
    {
        $this->db->Connect();

        $paymentVariant    = (int) $args['payment_id'];
        $isAccountPayments = $args['is_account'];

        $offerId = $args['offer_id'];

        $products = $this->db->Execute("SELECT * FROM products_hr phr "
                . "INNER JOIN products_to_offer pto ON phr.products_hr_id = pto.product_id "
                . "INNER JOIN product_hr_reserve phrr ON phr.products_hr_id = phrr.product_id "
                . "WHERE pto.offer_id = $offerId");

        $productsArray = [];
        foreach($products as $product) {
            $startDate = new \DateTime($product->end_date);
            $endDate   = new \DateTime($product->start_date);

            $productsArray[] = array(
                'name'        => $product->name,
                'description' => $product->description,
                'price'       => $product->full_time,
                'quantity'    => $endDate->diff($startDate)->d
            );
        }

        $paymentsMethodsFactory = new SubModules\Payment\PaymentsMethodsFactory();

        $paymentMethodName = $this->db->Execute("SELECT description FROM payments_variants WHERE id = $paymentVariant");
        $paymentMethod     = $paymentsMethodsFactory->factory($paymentMethodName[0]->description);
        $userAuth          = unserialize($this->session->get('user_auth'));

        $countryId = (int) $userAuth->getCountry()->countryId;

        $vat = new SubModules\VAT\VAT();
        $tax = $vat->FindByCountry($countryId);

        $arguments = array(
            'offer_id'            => $offerId,
            'products'            => $productsArray,
            'is_account_payments' => $isAccountPayments,
            'tax'                 => $tax->vat_rate
        );

        $paymentMethod->sendPayment($arguments);
    }

    public function actionSuccessPayPal($args)
    {
        if($args['success'] == true) {
            $pay              = new SubModules\Payment\PayPal();
            $this->db->Connect();
            $userAuth         = unserialize($this->session->get('user_auth'));
            $paymentVariantId = $this->db->Execute("SELECT id FROM payments_variants WHERE description LIKE 'PayPal'");
            $arguments        = array(
                'offer_id'           => $args['offer_id'],
                'payer_id'           => $args['PayerID'],
                'payment_id'         => $args['paymentId'],
                'is_account'         => $args['is_account'],
                'user_auth'          => $userAuth->getUserId(),
                'paymentv_ariant_id' => $paymentVariantId[0]->id
            );
            $pay->approvePayment($arguments);
        } else {
            //TODO return user to last page
        }
    }

    public function actionVatCreate($args)
    {

        if(!isset($args['country_id']) or ! isset($args['currency_id']) or ! isset($args['vat_rate'])) {
            throw new PowerplayException(MOD_EMPTY, 'user');
        }
        $vat = new SubModules\VAT\VAT();
        $vat->Create($args);
    }

    public function actionVatDelete($args)
    {
        if(!isset($args['vat_id'])) {
            throw new PowerplayException(MOD_EMPTY, 'user');
        }
        $vat = new SubModules\VAT\VAT();
        $vat->Delete($args);
    }

    public function actionVatUpdate($args)
    {
        if(!isset($args['vat_id']) or ! isset($args['country_id']) or ! isset($args['currency_id']) or ! isset($args['vat_rate'])) {
            throw new PowerplayException(MOD_EMPTY, 'user');
        }
//        !isset($args['isLocked']) ? $args['isLocked'] = 
        $vat = new SubModules\VAT\VAT();
        $vat->Edit($args);
    }

    public function actionVatMain()
    {
        $eb = new \Modules\Easybill\Easybill();

        $eb->getData();

//        $res = $eb->AddPayment($docResponse->document->documentID);
//                ;
//        $eb->getUser((int)$user->getUserId());
    }

}
