<?php

require __DIR__.'/../../../autoloader.php';

$mysqli  = new mysqli(Config::$host, Config::$user, Config::$password, Config::$db);
$invoice = new \Modules\Orders\SubModules\Invoice\Invoice();
$nowDate = date('Y-m-d H:00:00');

if ($mysqli->connect_error) {
    // TODO email send to developers
    exit();
}

$result = $mysqli->query("SELECT por.id FROM powerplay_offers po "
        . "INNER JOIN powerplay_orders por ON por.offer_id = po.id "
        . "INNER JOIN product_hr_reserve phr ON phr.offer_id = po.id "
        . "WHERE phr.start_date = '$nowDate' "
        . "GROUP BY phr.offer_id");

while ($row = $result->fetch_assoc()) {
//    $invoice->CreateInvoice($row['id']);
}

$mysqli->close();
exit();