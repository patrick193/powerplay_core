<?php

namespace Modules\Orders\SubModules\Payment;

use Modules\Orders\SubModules\Payment\PaymentMethodAbstract;
use PowerPlay\PowerplayException;

/**
 * @author Pavel Petrov
 */
class PaymentsMethodsFactory {

    /**
     * $paymentMethod has to be a classname of payment method 
     * that extends PaymentMethodAbstract
     * 
     * @param string $paymentMethod
     * @return \Modules\Orders\SubModules\Payment\paymentMethod
     * @throws PowerplayException
     */
    public function factory($paymentMethod) {
        $paymentMethod = "Modules\Orders\SubModules\Payment\\$paymentMethod";
        $paymentMethod = new $paymentMethod();

        if (!$paymentMethod instanceof PaymentMethodAbstract) {
            throw new PowerplayException(MOD_PAYMENT_METHOD_ABSTRACT);
        }

        return $paymentMethod;
    }
    
}