<?php

namespace Modules\Orders\SubModules\Payment;

use PowerPlay\Database;
use PowerPlay\PowerplayException\PowerplayException;
use Modules\Orders\SubModules\Account\Account;

/**
 * Description of Paymant
 *
 * @author Developer Pohorielov Vladyslav
 */
class Payment {

    private $db;
    private $account;

    public function __construct() {
        date_default_timezone_set('UTC');
        !$this->db ? $this->db = new Database() : '';
        !$this->account ? $this->account = new Account() : '';
    }

    public function Incomes($args) {
        if(!is_array($args) or ! isset($args['user_id'])) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        (float) $amount = (isset($args['ammount'])) ? $args['ammount'] : 0.00;
        (int) $currencyId = (isset($args['currency_id'])) ? (int) $args['currency_id'] : 1;
        (int) $paymentVar = (isset($args['payment_variant_id'])) ? (int) $args['payment_variant_id'] : 0;

        /** very important that the system should know all about incomes. That is why we use type of varriebles */
        if(($currencyId === 0) or ( $paymentVar === 0)) {
            throw new PowerplayException(MOD_INCOME_ERROR);
        }

        $userAccount = $this->account->getAccountByUser((int) $args['user_id']);
        if(is_object($userAccount)) {
            try {
                (int) $income = $this->db->Insert([['account_id' => $userAccount->id,
                'date' => date("Y-m-d"),
                'time' => date("H:m:s"), 'amount' => $amount, 'currency_id' => $currencyId,
                'payment_variant_id' => $paymentVar], 'account_incomes']);
                if($income === 0 or is_null($income) or is_bool($income) or ! is_int($income)) {
                    throw new PowerplayException(MOD_DATABASE_CONNECTION);
                }
                (float) $accountAmount = (float) $userAccount->amount + $amount;

                $this->account->UpdateAmount($userAccount->id, $accountAmount);

                return true;
            } catch(PowerplayException $ex) {
                echo 'Fatal error. ' . $ex->getMessage();
            }
        } else {
            $this->account->CreateAccount((int) $args['user_id'], $args['user_id'], 0.00);
            $this->Incomes($args);
        }

        return false;
    }

}
