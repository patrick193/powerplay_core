<?php

namespace Modules\Orders\SubModules\Payment;

/**
 * @author Pavel Petrov
 */
interface PaymentMethodInterface
{

    public function sendPayment($args);

    public function approvePayment($args);
}
