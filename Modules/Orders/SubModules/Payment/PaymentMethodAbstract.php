<?php

namespace Modules\Orders\SubModules\Payment;

use Modules\Orders\SubModules\Payment\PaymentMethodInterface;

/**
 * @author Pavel Petrov
 */
class PaymentMethodAbstract implements PaymentMethodInterface
{

    protected $requisites;

    public function sendPayment($args)
    {
    }

    public function approvePayment($args)
    {
    }

    /**
     * Income has to be created when payment successfully transferred
     */
    public function createIncome()
    {
    }

}
