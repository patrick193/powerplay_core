<?php

namespace Modules\Orders\SubModules\Payment;

use Modules\Orders\SubModules\Payment\PaymentMethodAbstract;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use PayPal\Api\Amount;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\PaymentExecution;
use PowerPlay\PowerplayException\PowerplayException;
use Modules\Orders\SubModules\Payment\Payment as PowerPlayPayment;
use Modules\Orders\SubModules\Offers\Offers;
use PayPal\Api\Details;

/**
 * @author Pavel Petrov
 */
class PayPal extends PaymentMethodAbstract
{
    
    public function __construct()
    {
        \autoloader::addAutoloader(__DIR__.'/autoloadPayPal.php');
    }

    public function sendPayment($args)
    {
        if (!$args['products']) {
            throw new PowerplayException(MOD_EMPTY);
        }
        $isAccountPayments = $args['is_account_payments'];

        $offerId = $args['offer_id'];

        $oAuthTokenCredential = new OAuthTokenCredential(\Config::$paypalClientId, \Config::$paypalClientSecret);
        $apiContext           = new ApiContext($oAuthTokenCredential);

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $items       = [];
        $amountTotal = 0;

        foreach ($args['products'] as $product) {
            $amountTotal += $product['quantity'] * $product['price'];
            $item    = new Item();
            $item->setName($product['name'])
                    ->setCurrency(\Config::$currency)
                    ->setQuantity($product['quantity'])
                    ->setPrice($product['price'])
                    ->setDescription($product['description']);
            $items[] = $item;
        }

        $itemList = new ItemList();
        $itemList->setItems($items);

        $taxAmount = ($args['tax'] * 0.01) * $amountTotal;
        
        $details = new Details();
        $details->setTax($taxAmount)
                ->setSubtotal($amountTotal);

        $amount = new Amount();
        $amount->setCurrency(\Config::$currency)
                ->setTotal($amountTotal + $taxAmount)
                ->setDetails($details);

        $transaction = new Transaction();
        $transaction->setAmount($amount);
        $transaction->setItemList($itemList);

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl("http://" . $_SERVER['HTTP_HOST'] . "/orders/paypal/success/$isAccountPayments/$offerId/true")
                ->setCancelUrl("http://" . $_SERVER['HTTP_HOST'] . "/orders/success/false");

        $payment = new Payment();
        $payment->setIntent('sale');
        $payment->setPayer($payer);
        $payment->setTransactions(array($transaction));
        $payment->setRedirectUrls($redirectUrls);
        $payment->create($apiContext);

        header('location:' . $payment->getApprovalLink());
    }

    public function approvePayment($args)
    {
        $offerId           = $args['offer_id'];
        $payerId           = $args['payer_id'];
        $paymentId         = $args['payment_id'];
        $isAccountPayments = $args['is_account'];
        $userId            = $args['user_auth'];
        $paymentVariantId  = $args['paymentv_ariant_id'];

        $oAuthTokenCredential = new OAuthTokenCredential(\Config::$paypalClientId, \Config::$paypalClientSecret);
        $apiContext           = new ApiContext($oAuthTokenCredential);
        $payment              = Payment::get($paymentId, $apiContext);
        $paymentExecution     = new PaymentExecution();

        $paymentExecution->setPayerId($payerId);

        $payment->execute($paymentExecution, $apiContext);

        $pay = new PowerPlayPayment();

        $amount   = $payment->getTransactions()[0]->getAmount()->getTotal();
        // TODO get id from DB
        $currency = $payment->getTransactions()[0]->getAmount()->getCurrency();
        $currency = 1;

        $pay->Incomes(['user_id' => $userId, 'ammount' => $amount, 'currency_id' => $currency, 'payment_variant_id' => $paymentVariantId]);

        if (!$isAccountPayments) {
            $offer  = new Offers();
            $offer->OrderAdd(['offer_id' => $offerId, 'user_id' => $userId]);
            // OFFER PAYED
        }
    }

}