<?php

namespace Modules\Orders\SubModules\Invoice;

use PowerPlay\Mailer\Mailer;
use Modules\PDFGenerator\PDFGenerator;
use PowerPlay\YamlConfiguration;
use PowerPlay\PowerplayException\PowerplayException;
use PowerPlay\Module;

/**
 * Description of Invoice
 *
 * @author Developer Pohorielov Vladyslav
 */
class Invoice extends Module {

    /**
     * Function for creating invoice
     * @param int $orderId Id of order
     * @throws PowerplayException
     */
    public function CreateInvoice($orderId) {
        if(!is_int($orderId)) {
            throw new PowerplayException(MOD_EMPTY);
        }
        if($this->CheckOrder($orderId) === false) {
            throw new PowerplayException(MOD_EMPTY);
        }
        $this->ProductAviable($orderId);
    }

    /**
     * Function for checking a product for an avaliablity. If it is true we should Send a product and create an invoice & 
     * @param int $orderId Id of order
     * @throws PowerplayException
     */
    protected function ProductAviable($orderId) {
        if(!is_int($orderId) or $orderId == 0) {
            throw new PowerplayException(MOD_EMPTY);
        }
        if($this->CheckOrder($orderId) === true) {/* if order exist */
            $order = $this->db->Select(['*', 'powerplay_orders', ['id' => $orderId]])[0];
            $this->db->setExecute(false);
            $q = "SELECT * FROM `products_hr`"
                    . " WHERE `products_hr_id` "
                    . "IN (select product_id from products_to_offer where offer_id = " . $order->offer_id . ")";
            $products = $this->db->Execute($q, \Modules\Products\Entity\ProductHr::class); /* get all product in offer/order */
            $reserve = $this->db->Select(['*', 'product_hr_reserve', ['offer_id' => $order->offer_id]]);
            if(count($reserve) !== count($products)) {
                throw new PowerplayException('Not the same');
            }
            $pr = [];
            foreach($products as $product) {
                $endDate = new \DateTime($product->getEndDate());
                $today = new \DateTime(date("Y-m-d H:00:00"));

                if($endDate->getTimestamp() <= $today->getTimestamp()) {
                    $q = "update `products_hr` inner join `product_hr_reserve` on `product_hr_reserve`.`offer_id` = " . $order->offer_id . ""
                            . " set `products_hr`.`start_date` = "
                            . "if(`products_hr`.`start_date` < `product_hr_reserve`.`start_date`, `product_hr_reserve`.`start_date`, `products_hr`.`start_date`), "
                            . "`products_hr`.`end_date` = "
                            . "if(`products_hr`.`end_date` < `product_hr_reserve`.`end_date`, `product_hr_reserve`.`end_date`, `products_hr`.`end_date`) "
                            . "WHERE `products_hr`.`products_hr_id`= " . $product->getProductsHrId(); /* update product */
                    $this->db->Execute($q);
                    array_push($pr, $product);
                }
            }
            if(count($pr) != 0) {
                $this->PdfInvoice($orderId, $pr);
                $this->SendEmail($orderId);
            }
        } else {
            throw new PowerplayException('Order not found');
        }
    }

    /**
     * Function for generation an invoice pdf file
     * @param int $orderId
     * @param array $products
     */
    private function PdfInvoice($orderId, $products) {
        $pdf = new PDFGenerator();
        $pdf->__init__($orderId, $products);
    }

    /**
     * Function for sending email
     * @param int $invoiceNumber
     */
    private function SendEmail($invoiceNumber) {
        $mailer = new Mailer();
        $yml = new YamlConfiguration();
        $config = $yml->GetConfigurations(__DIR__ . "/../Offers/Config/MailTemplate.yml");

        $user = $this->db->Select(['user_id', 'powerplay_offers', ['id' => $this->db->Select(['offer_id',
                            'powerplay_orders', ['id' => $invoiceNumber]])[0]->offer_id]])[0]->user_id;
        $to = $config->to;
        $subject = $config->subject;
        $message = $config->message;
        $company = $this->GetCompanyName($user);
        $argum['header'] = \Config::$documentsPath . "/Invoice_$invoiceNumber.pdf";
        $argum['to'] = $this->getBy($to['field'], $to['table'], "user_id", $user);
        $argum['subject'] = $subject['text_invoice'];
        $argum['subject'] .= ' ' . $company;
        $argum['message'] = $message['text_invoice'];
        $argum['message'] .= ' ' . $this->getBy($message['field'], $message['table_invoice'], "id", (int) $invoiceNumber);

        $mailer->Send($argum);
    }

    protected function GetCompanyName($userId) {
        $user = @unserialize($this->session->get('user_auth'));
        return $user->getUpperParentInGroup((int) $userId)->getCompanyName();
    }

    /**
     * Method for geting value from table by conditions from configurations
     * 
     * @param string $field
     * @param string $table
     * @param string $conditionKey
     * @param string $conditionValue
     * @return string
     */
    private function getBy($field, $table, $conditionKey, $conditionValue) {
        $query = "SELECT $field FROM $table WHERE $conditionKey = $conditionValue";
        $this->db->Connect();
        $result = $this->db->Execute($query);
        return end($result)->$field;
    }

    protected function CheckOrder($orderId) {
        if(!is_int($orderId) or $orderId == 0) {
            throw new PowerplayException(MOD_EMPTY);
        }
        $order = $this->db->Count('*', 'powerplay_orders', ['id' => $orderId]);
        if($order > 0) {
            return true;
        }
        return false;
    }

}
