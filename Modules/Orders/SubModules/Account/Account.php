<?php

namespace Modules\Orders\SubModules\Account;

use PowerPlay\Database;
use PowerPlay\PowerplayException\PowerplayException;

/**
 * Description of Account
 *
 * @author Developer Pohorielov Vladyslav
 */
class Account {

    /**
     *
     * @var Database 
     */
    private $db;

    /**
     * Init Class
     */
    public function __construct() {
        !$this->db ? $this->db = new Database() : '';
    }

    public function getAccountByOffer($offerId) {
        if(!is_int($offerId)) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        $userAccount = $this->db->Select(['*', 'user_accounts', ['id' => $this->db->Select([[
                        'account_id'], 'powerplay_users', ['user_id' => $this->db->Select([[
                                'user_id'], 'powerplay_offers', ['id' => $offerId]])[0]->user_id]])[0]->account_id]])[0];
        if(!is_object($userAccount)) {
            $userId = $this->db->Select([['user_id'], 'powerplay_offers', ['id' => $offerId]])[0]->user_id;
            $this->CreateAccount((int)$userId, $userId, 0.00);
            return $this->getAccountByOffer($offerId);
        }
        return $userAccount;
    }

    public function getAccountByUser($userId) {
        if(!is_int($userId)) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        $userAccount = $this->db->Select(['*', 'user_accounts', ['id' => $this->db->Select([[
                        'account_id'], 'powerplay_users', ['user_id' => $userId]])[0]->account_id]])[0];
        if(!is_object($userAccount)) {
            $this->CreateAccount((int)$userId, $userId, 0.00);
        }
        return $userAccount;
    }

    public function UpdateAmount($accountId, $amount) {
        if(!is_float($amount)) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        $this->db->Update([['amount' => $amount], 'user_accounts', ['id' => $accountId]]);
    }

    public function CreateAccount($userId, $number, $amount) {
        if(!is_int($userId) or ! is_float($amount)) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        (int) $account = $this->db->Insert([['number' => $number, 'amount' => $amount],
            'user_accounts']);
        if($account === 0) {
            throw new PowerplayException(MOD_INT);
        }
        $this->db->Update([['account_id' => $account], 'powerplay_users', ['user_id' => $userId]]);
        return true;
    }

}
