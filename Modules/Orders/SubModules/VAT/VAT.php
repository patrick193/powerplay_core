<?php

namespace Modules\Orders\SubModules\VAT;

use PowerPlay\Database;
use PowerPlay\PowerplayException\PowerplayException;
/**
 * Description of VAT
 *
 * @author Developer Pohorielov Vladyslav
 */
class VAT {

    private $db;

    public function __construct() {
        !$this->db ? $this->db = new Database() : '';
    }

    /**
     * Method for Creat any data in table
     * @param array $args Array of all conditionals
     * @return mixed
     * */
    public function Create($args) {
        !$this->db ? $this->db = new Database() : '';
        $insertId = $this->db->Insert([['country_id' => $args['country_id'], 'currency_id' => $args['currency_id'],
        'vat_rate' => $args['vat_rate']], 'powerplay_vat']);
        if($insertId != 0 and ! is_bool($insertId)) {
            return $insertId;
        }
        return false;
    }

    /**
     * Method for Update any data from table
     * @param array $args Array of all conditionals
     * @return boolean
     * */
    public function Edit($args) {
        !$this->db ? $this->db = new Database() : '';
        $u = $this->db->Update([['country_id' => $args['country_id'], 'currency_id' => $args['currency_id'],
        'vat_rate' => $args['vat_rate']], 'powerplay_vat', ['vat_id' => $args['vat_id']]]);
        if($u) {
            if(($args['isLocked'])) {
                $this->db->Update([['isLocked' => $args['isLocked']], 'powerplay_vat',
                    ['vat_id' => $args['vat_id']]]);
            }
            return true;
        }
        return false;
    }

    /**
     * Method for Delete any data from table
     * @param array $args Array of all conditionals
     * @return boolean
     * */
    public function Delete($args) {
        !$this->db ? $this->db = new Database() : '';
        return $this->db->Delete(['powerplay_vat', ['vat_id' => $args['vat_id']]]);
    }

    /**
     * Method for Select any data from table
     * @param array $args Array of all conditionals
     * @return mixed
     * */
    public function SelectAll() {
        !$this->db ? $this->db = new Database() : '';
        $select = $this->db->Select(['*', 'powerplay_vat']);

        return $select; //just change
    }

    /**
     * Method for Select any data from table
     * @param array $args Array of all conditionals
     * @return mixed
     * */
    public function SelectOne($args) {
        !$this->db ? $this->db = new Database() : '';
        $this->db->setExecute(false);
        $q = $this->db->Select(['*', 'powerplay_vat', ['vat_id' => $args['vat_id']]]) . " LIMIT 1";
        $onlyOne = $this->db->Execute($q);
        return $onlyOne;
    }

    /**
     * 
     * @param int $countryId
     * @return mixed
     * @throws PowerplayException
     */
    public function FindByCountry($countryId) {
        if(!is_int($countryId) or $countryId == 0){
            throw new PowerplayException(MOD_EMTY);
        }
        $this->db->setExecute(false);
        $query = $this->db->Select(['*', 'powerplay_vat', ['country_id' => $countryId]]);
        $vat = $this->db->Execute($query, VAT::class)[0];
        if($vat){
            return $vat;
        }
        return false;
    }
}
