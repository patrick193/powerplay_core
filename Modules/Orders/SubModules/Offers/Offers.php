<?php

namespace Modules\Orders\SubModules\Offers;

use Modules\Orders\SubModules\Invoice\Invoice;
use Modules\Orders\SubModules\VAT\VAT;
use Modules\Products\Entity\ProductHr;
use Modules\PDFGenerator\PDFGenerator;
use PowerPlay\Database;
use PowerPlay\PowerplayException\PowerplayException;

/**
 * Description of Offers
 *
 * @author Developer Pohorielov Vladyslav
 */
class Offers {

    /**
     *
     * @var Database
     */
    private $db;

    /**
     *
     * @var \Modules\Roles\Users 
     */
    protected $user;

    public function __construct() {
        !$this->db ? $this->db = new Database() : '';
        $ses = new \PowerPlay\Session();
        if(!$this->user) {
            $this->user = @unserialize($ses->get('user_auth'));
        }
    }

    /**
     * PRODUCTS SHOULD BE IN FORMAT LIKE SHOWN BELOW
     * 
     * $args['products'] = array(1, 4);
     * $args['start_date'] = '2015-11-28 10:00:00';
     * $args['end_date'] = '2015-12-04 10:00:00';
     * $args['product_category'] = 'products_hr';
     * 
     * @param array $args
     * @throws PowerplayException
     */
    public function OfferCreate($args) {
        if(!is_array($args)) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        if(!isset($args['products'])) {
            throw new PowerplayException(MOD_EMPTY);
        }
//        $args['products'] = array(
//            array('table_name' => 'products_hr', 'id' => 1, 'start_date' => '01.01.2016',
//                'end_date' => '25.01.2016'),
//            array('table_name' => 'products_hr', 'id' => 4, 'start_date' => '02.02.2016',
//                'end_date' => '12.03.2016'),
//        );
        /**
         * 3225.6
         */
        $amount = 0;
        $productHr = new ProductHr();
        $vat = new VAT();
        $countryId = $this->user->getCountry()->countryId;
        $nvate = $vat->FindByCountry((int) $countryId);
        foreach($args['products'] as $product) {
            $sDate = new \DateTime($product['start_date']);
            $eDate = new \DateTime($product['end_date']);

            $diff = $sDate->diff($eDate)->days;
            $products = $productHr->FindById($product['id']);
            $amount += ($products->getPrice() * $diff);
        }
        $amount += ($amount * ($nvate->vat_rate / 100));

        $user = (isset($args['user_id']) and $this->CheckUser((int) $args['user_id'])) ? $args['user_id'] : $this->user->getUserId();

        // TODO products_hr checking
        if ($args['product_category'] == 'products_hr') {
            if (!$this->CheckProductsToReserve($args['products'], $args['start_date'], $args['end_date'])) {
                // TODO U CAN'T CHOOSE THIS DATE
                die("U CAN'T CHOOSE THIS DATE");
            }
        }

        $argumentsToInsert = array(
            'date' => date("Y:m:d"),
            'time' => date("H:m:s"),
            'isLocked' => 0,
            'amount' => $args['amount'],
            'currency_id' => 1,
            'description' => $args['desc'],
            'user_id' => $user,
            'step_id' => 1
        );

        $offerId = $this->db->Insert([$argumentsToInsert, 'powerplay_offers']);
        if($offerId) {
            if ($args['product_category'] == 'products_hr') {
            $this->InsertHrProducts($args, $offerId);
            }
            $pdf = new PDFGenerator();
            $pdf->__init__($offerId, null, 'Offer');
            $this->SendEmail($offerId,$user);
            $this->db->Update([['step_id' => 2], 'powerplay_offers', ['id' => $offerId]]);
        } else {
            throw new PowerplayException(MOD_UKNOWN_ERROR, 'user');
        }
    }

    /**
     * This method is storing products to offer and reservation
     * 
     * @param array $args
     * @param int $offerId
     */
    private function InsertHrProducts($args, $offerId)
    {
        $startDate = new \DateTime($args['start_date']);
        $endDate   = new \DateTime($args['end_date']);

        foreach ($args['products'] as $id) {
            $parametersToOffer = array(
                'offer_id'      => $offerId,
                'product_id'    => $id,
                'product_table' => 'products_hr'
            );

            $parametersToReserve = array(
                'product_id' => $id,
                'start_date' => $startDate->format('Y-m-d H:00:00'),
                'end_date'   => $endDate->format('Y-m-d H:00:00'),
                'offer_id'   => $offerId
            );

            $this->db->Insert([$parametersToOffer, 'products_to_offer']);
            $this->db->Insert([$parametersToReserve, 'product_hr_reserve']);
        }
    }

    /**
     * This method checks products and returns false 
     * if this offer can't be created
     * else true
     * 
     * @param array $products
     * @param string $startDate
     * @param string $endDate
     * @return boolean
     */
    private function CheckProductsToReserve($products, $startDate, $endDate)
    {
        $startDate = new \DateTime($startDate);
        $endDate   = new \DateTime($endDate);
        $startDate = $startDate->format('Y-m-d H:00:00');
        $endDate   = $endDate->format('Y-m-d H:00:00');

        foreach ($products as $productId) {
            $query = "SELECT COUNT(*) AS cnt FROM products_hr WHERE products_hr_id = $productId AND end_date > '$startDate'";

            if ($this->db->Execute($query)[0]->cnt) {
                return false;
            }

            $query = "SELECT COUNT(*) AS cnt FROM product_hr_reserve "
                    . "WHERE product_id = $productId "
                    . "AND isLocked IS NOT TRUE "
                    . "AND ('$startDate' > start_date AND '$startDate' < end_date) "
                    . "OR ('$endDate' > start_date AND '$endDate' < end_date) "
                    . "OR ('$startDate' < start_date AND '$endDate' > end_date)";

            if ($this->db->Execute($query)[0]->cnt) {
                return false;
            }
        }

        return true;
    }

    /**
     * Method for sending email when order created
     * 
     * @param int $id
     * @param int $user
     * @param boolean $order 
     */
    private function SendEmail($id, $user, $order = false) {

        $mailer = new \PowerPlay\Mailer\Mailer();
        $ymlParser = new \PowerPlay\YamlConfiguration();
        $ymlConfigs = $ymlParser->GetConfigurations(__DIR__ . "/Config/MailTemplate.yml");
        $to = $ymlConfigs->to;
        $subject = $ymlConfigs->subject;
        $message = $ymlConfigs->message;
        $company = $this->GetCompanyName($user);
        $argum['header'] = !$order ? \Config::$documentsPath ."/Offer_$id.pdf" : '';
        $argum['to'] = $this->getBy($to['field'], $to['table'], "user_id", $user);
        $argum['subject'] = $subject['text'];
        $argum['subject'] .= ' ' . $company;
        $argum['message'] = !$order ? $message['text'] : $message['text_order'];
        $argum['message'] .= ' ' . $this->getBy($message['field'], !$order ? $message['table'] : $message['table_order'], "id", (int) $id);

        $mailer->Send($argum);
    }

    public function GetCompanyName($userId) {
        return $this->user->getUpperParentInGroup((int) $userId)->getCompanyName();
    }

    /**
     * Method for geting value from table by conditions from configurations
     * 
     * @param string $field
     * @param string $table
     * @param string $conditionKey
     * @param string $conditionValue
     * @return string
     */
    private function getBy($field, $table, $conditionKey, $conditionValue) {
        $this->db->Connect();
        $query  = "SELECT $field FROM $table WHERE $conditionKey = $conditionValue";
        $result = $this->db->Execute($query);
        return end($result)->$field;
    }

    public function OfferBlock($offerId) {
        if(!is_int($offerId)) {
            throw new Exception(MOD_INT);
        }
        return $this->db->Delete(['powerplay_offers', ['id' => $offerId]]);
    }

    public function OrderAdd($args = null) {
        if(!is_array($args)) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }

        $count = $this->db->Count('*', 'powerplay_offers', ['isLocked' => 0, 'id' => $args['offer_id']]);
        $orderEx = $this->db->Select(['`id`', 'powerplay_orders', ['offer_id' => $args['offer_id']]])[0];

        if(is_object($orderEx)) {
            throw new PowerplayException(MOD_ORDER_PAID);
        }
        if($count != 0) {
            $ua = new \Modules\Orders\SubModules\Account\Account();
            $userAccount = $ua->getAccountByOffer((int) $args['offer_id']);

            $offer = $this->db->Select(['*', 'powerplay_offers', ['id' => $args['offer_id']]])[0];

            if(($userAccount->amount - $offer->amount) >= 0.00) {
                $amount = $userAccount->amount - $offer->amount;

                $order = $this->db->Insert([['offer_id' => $args['offer_id'], 'date_start' => date("Y-m-d"),
                'time_start' => date("H:m:s"), 'date_end' => date("Y-m-d"), 'time_end' => date("H:m:s"),
                'status_id' => 1], 'powerplay_orders']);

                $expense = $this->db->Insert([['date' => date("Y-m-d"), 'time' => date("H:m:s"),
                'account_id' => $userAccount->id, 'offer_id' => $args['offer_id'],
                'amount' => $offer->amount],
                    'account_expense']);

                $ua->UpdateAmount($userAccount->id, $amount);

                $user = (isset($args['user_id']) and $this->CheckUser((int) $args['user_id'])) ? $args['user_id'] : $this->user->getUserId();

                $this->SendEmail($order, $user, true);

                $this->db->Update([['step_id' => 3], 'powerplay_offers', ['id' => $args['offer_id']]]);

                $invoice = new Invoice();
                $ci = $invoice->CreateInvoice((int) $order);
            } else {
                throw new PowerplayException(MOD_NOT_ENOUGH . $userAccount->amount, 'user');
            }
        } else {
            throw new PowerplayException(MOD_OFFER_DOES_NOT_EXIST, 'user');
        }
    }

    private function CheckUser($userId) {
        if(!is_int($userId)) {
            throw new PowerplayException(MOD_INT);
        }
        $count = $this->db->Count('user_id', 'powerplay_users', ['user_id' => $userId]);
        if($count != 0 and ! is_null($count)) {
            return true;
        }
        return false;
    }

}
