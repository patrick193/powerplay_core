<?php

namespace Modules\Language\Helpers;

/**
 * LanguageModel
 *
 * @author Developer Pohorielov Vladyslav
 */
class LanguageModel {

    /**
     *
     * @var integer
     *  
     */
    protected $language_id;
    
    /**
     *
     * @var String 
     */
    protected $language_name;
    
    /**
     *
     * @var String 
     */
    protected $language_code;

    public function getLanguage_id() {
        return $this->language_id;
    }

    public function getLanguage_name() {
        return $this->language_name;
    }

    public function getLanguage_code() {
        return $this->language_code;
    }

    public function setLanguage_id($language_id) {
        $this->language_id = $language_id;
    }

    public function setLanguage_name($language_name) {
        $this->language_name = $language_name;
    }

    public function setLanguage_code($language_code) {
        $this->language_code = $language_code;
    }

}
