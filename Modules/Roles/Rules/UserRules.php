<?php

namespace Modules\Roles\Rules;

use PowerPlay\User;
use PowerPlay\ModuleLoader\Model;
use PowerPlay\Database;
use PowerPlay\Storage;
use Modules\Roles\System\UserInterface;
use Modules\Roles\Roles;
use PowerPlay\PowerplayException\PowerplayException;

/**
 * Description of UserRules
 *
 * @author Developer Pohorielov Vladyslav
 */
class UserRules extends User implements UserInterface {

    use traitUsers;

use \Modules\Roles\AuthUserTrait;

    private $db, $storage;

    public function __construct() {
        if(!$this->db) {
            $this->db = new Database();
        }
        !$this->storage ? $this->storage = new Storage('users') : '';
    }

    /**
     * Function to check access to the module
     * @param int $id
     * @param Model $module
     * @return boolean
     * @throws Exception
     */
    public function CheckAccess($id, Model $module) {
        if(!is_int($id)) {
            throw new PowerplayException(MOD_INT);
        }
        !$this->db ? $this->db = new Database() : '';
        $id = abs($id);
        $roleCode = $this->db->Select(['role_code', 'powerplay_roles', ['role_id' =>
                $this->db->Select(['role_id', 'powerplay_users', ['user_id' => $id]])[0]->role_id]]);
        if(!is_null($roleCode)) {
            $role = new Roles();
            $access = $role->CheckAccess($roleCode[0]->role_code, $module);
            return $access;
        }
        return false;
    }

    /**
     * Function to get all children by user
     * @param int $userId
     * @return mixed The function will return an array of users objects or false if could not find any users
     * @throws Exception
     */
    public function getChild($userId) {
        if(!is_int($userId) or empty($userId)) {
            throw new PowerplayException(MOD_INT);
        }
        !$this->db ? $this->db = new Database() : '';
        $userId = abs($userId);
        $this->db->setExecute(false);
        $query = $this->db->Select(['*', 'powerplay_users', ['parent_id' => $userId,
                'hide' => 0]]);
        $users = $this->db->Execute($query, UserRules::class);

        return $users;
    }

    /**
     * Function to get parent user
     * @param int $userId
     * @return mixed The function will return an array of users objects or false if could not find any users
     * @throws Exception
     */
    public function getParent($userId) {
        if(!is_int($userId) or empty($userId)) {
            throw new PowerplayException(MOD_INT);
        }
        !$this->db ? $this->db = new Database() : '';
        $userId = abs($userId);
        $this->db->setExecute(false);
        $q = $this->db->Select(['*', 'powerplay_users', ['user_id' =>
                "(" . $this->db->Select(['`parent_id`', 'powerplay_users', ['user_id' => $userId]]) . ")"]]);
        $q = str_replace("'", "", $q);
        $user = $this->db->Execute($q, UserRules::class);
        return $user;
    }

    /**
     * Function to find upper parrent in the grouop
     * @param int $userId
     * @return UserRules
     * @throws PowerplayException
     */
    public function getUpperParentInGroup($userId) {
        if(!is_int($userId) or $userId == 0) {
            throw new PowerplayException(MOD_USER_ID);
        }
        !$this->db ? $this->db = new Database() : '';

        $user = $this->Load(['user_id', $userId]);
        $parent = $this->getParent($userId)[0];
        if($parent != 0) {
            $parentRole = $parent->getRoleId();
            $role = new Roles();
            $parentRole = $role->FindById((int) $parentRole);
            $userRole = $role->FindById((int) $user->getRoleId());

            if((int) $parentRole->getGroupId() < (int) $userRole->getGroupId() or ( $userRole->getRoleCode() == $parentRole->getRoleCode())) {
                return $user;
            } elseif($parentRole->getGroupId() == $userRole->getGroupId()) {
                $parent = $this->getUpperParentInGroup((int) $parent->getUserId());
            } else {
                return $parent;
            }
            return $parent;
        } else {
            return $user;
        }
    }

    public function getAddress($userId) {
        if(!is_int($userId) or empty($userId)) {
            throw new PowerplayException(MOD_INT);
        }
        !$this->db ? $this->db = new Database() : '';
        $userId = abs($userId);

        $userAddress = $this->db->Select(['*', 'user_address', ['user_address_id' => $this->db->Select(
                        [['user_address_id'], 'powerplay_users', ['user_id' => $userId]])[0]->user_address_id]]);

        if($userAddress) {
            $address = [];
            $city = $this->db->Select(['*', 'cities', ['cityId' => $userAddress[0]->city_id]]);
            $region = $this->db->Select(['name', 'region', ['regionId' => $city[0]->regionId]]);
            $country = $this->db->Select(['name', 'country', ['countryId' => $city[0]->countryId]]);
            $address['country'] = $country;
            $address['region'] = $region[0]->name;
            $address['city'] = $city[0]->name;
            $address['street'] = $userAddress[0]->street;
            $address['postal_code'] = $userAddress[0]->postal_code;
            return $address;
        }
        return false;
    }

    /**
     * Create a new user
     * @return \Modules\Roles\Rules\UserRules
     */
    public function Create() {
        return new UserRules();
    }

    /**
     * Function to create or edit user
     * @param User $user
     * @return boolean true if all work finished correct
     * @throws Exception
     */
    public function Execute(User $user) {
        if(!$this->getSwitcher()) {
            throw new PowerplayException(MOD_SWITCHER);
        }
        if(is_null($user)) {
            throw new Exception('User is null.');
        }
        !$this->db ? $this->db = new Database() : '';
        if($this->db->Count('*', 'powerplay_users', ['user_id' => $user->getUserId()]) > 0) {
            $this->Switcher();
            return $this->Edit($user);
        } else {
            $this->Switcher();
            return $this->Add($user);
        }
    }

    /**
     * Function to add new user
     * @param User $user
     * @return boolean
     */
    private function Add(UserRules $user) {
        !$this->db ? $this->db = new Database() : '';

        $add = $this->db->Insert([['postal_code' => $user->getPostalCode(), 'city_id' => $user->getCityId(),
        'street' => $user->getStreet()], 'user_address']);

        $pass = $user->getUserPassword() ? $user->getUserPassword() : '';
        $res = $this->db->Insert([['user_first_name' => $user->getUserFirstName(),
        'user_last_name' => $user->getUserLastName(),
        'user_email' => $user->getUserEmail(), 'parent_id' => $user->getParentId(),
        'role_id' => $user->getRoleId(), 'isLocked' => $user->getIsLocked(),
        'language_id' => $user->getLanguageId(), 'company_name' => $user->getCompanyName(),
        'cell_phone' => $user->getCellPhone(),
        'user_password' => $pass,
        'user_address_id' => $this->db->LastInsert()],
            'powerplay_users']);

        $user->setUserId($res);
        return $user;
    }

    /**
     * Function to edit user
     * @param User $user
     * @return boolean
     */
    private function Edit(UserRules $user) {
        !$this->db ? $this->db = new Database() : '';

        $add = $this->db->Update([['postal_code' => $user->getPostalCode(), 'city_id' => $user->getCity(),
        'street' => $user->getStreet()], 'user_address',
            ['user_address_id' => $this->db->Select([['user_address_id'], 'powerplay_users',
                    ['user_id' => $user->getUserId()]])[0]->user_address_id]]);
        $pass = $user->getUserPassword() ? $user->getUserPassword() : '';

        $res = $this->db->Update([['user_first_name' => $user->getUserFirstName(),
        'user_last_name' => $user->getUserLastName(),
        'user_email' => $user->getUserEmail(), 'parent_id' => $user->getParentId(),
        'role_id' => $user->getRoleId(), 'isLocked' => $user->getIsLocked(),
        'language_id' => $user->getLanguageId(), 'company_name' => $user->getCompanyName(),
        'cell_phone' => $user->getCellPhone(),
        'user_password' => $pass,
            ],
            'user_password' => $pass,
            'powerplay_users', ['user_id' => $user->getUserId()]]);
        return $res;
    }

    /**
     * Function to getting users with parameters
     * @param array $param
     * @throws Exception
     */
    public function Load($param) {
        if(!is_array($param) or empty($param) or count($param) > 2) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        !$this->db ? $this->db = new Database() : '';
        $fields = explode(",", $param[0]);
        $args = explode(",", $param[1]);
        if(count($fields) === count($args)) {
            $query = 'SELECT * FROM `powerplay_users` WHERE ';
            for($i = 0, $c = count($fields); $i < $c; $i++) {
                $query .= "`" . trim($fields[$i]) . "` = '" . trim($args[$i]) . "'";
                $i < ($c - 1) ? $query .= " AND " : '';
            }
            $this->db->Connect();
            $users = $this->db->Execute($query, UserRules::class);
            $this->db->Close();
            if(count($users) === 1) {
                return $users[0];
            }
            return $users;
        } else {
            throw new PowerplayException(MOD_LOAD);
        }
    }

    /**
     * Function for checking is a child user id is a realy child
     * @param int $userParentId Parrent user id
     * @param int $userChildId Child user id
     * @throws PowerplayException
     */
    public function isChild($userParentId, $userChildId) {
        if(!is_int($userChildId) or ! is_int($userParentId)) {
            throw new PowerplayException(MOD_USER_ID);
        }
        $this->db->setExecute(false);
        $q = $this->db->Select(['*', 'powerplay_users', ['user_id' => $userChildId]]);
        $user = $this->db->Execute($q, UserRules::class)[0];

        if($user->getParentId() == $userParentId) {
            return true;
        } else {
            $upperParent = $this->getUpperParentInGroup($userChildId);
            if($upperParent->getUserId() == $userParentId) {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * Function for blocking a user
     * @param int $userId
     * @throws PowerplayException
     */
    public function BlockUser($userId) {
        if(!is_int($userId) or $userId == 0) {
            throw new PowerplayException(MOD_USER_ID);
        }
        $q = "update powerplay_users set isLocked = if (powerplay_users.isLocked > 0, 0, 1) where user_id = $userId";
        $this->db->Connect();
        $this->db->Execute($q);
    }

    /**
     * Function for hide a user
     * @param int $userId
     * @throws PowerplayException
     */
    public function HideUser($userId) {
        if(!is_int($userId) or $userId == 0) {
            throw new PowerplayException(MOD_USER_ID);
        }
        $q = "update powerplay_users set hide = if(powerplay_users.hide > 0, 0, 1) where user_id = $userId";
        $this->db->Connect();
        $this->db->Execute($q);
    }

    protected function Check($user_id) {
        !$this->db ? $this->db = new Database() : '';
        $action = 'create';
        if(($this->db->Count('*', 'powerplay_users', ['user_id' => $user_id])) > 0) {
            $action = 'edit';
        } else {
            $action = 'create';
        }
        return $action;
    }

}
