<?php

namespace Modules\Roles\Rules;

use PowerPlay\Database;

trait traitUsers
{

    protected $company_name,
            $street,
            $postal_code,
            $city,
            $cell_phone,
            $language_id,
            $user_address_id,
            $role,
            $children;

    public function getChildren()
    {
        return $this->children;
    }

    public function setChildren($children)
    {
        $this->children = $children;
    }

    public function getRole()
    {
        $roles = new \Modules\Roles\Roles();
        if(!$this->role) {
            $this->role = $roles->FindById((int) $this->getRoleId());
        }
        return $this->role;
    }

    public function setRole($role)
    {
        $this->role = $role;
    }

    public function getUserAddressId()
    {
        return $this->user_address_id;
    }

    public function setUserAddressId($user_address_id)
    {
        $this->user_address_id = $user_address_id;
    }

    public function getLanguageId()
    {
        return $this->language_id;
    }

    public function getLanguage()
    {
        $db     = new Database();
        $db->Connect();
        $result = $db->Select(['*', 'powerplay_language', ['language_id' => $this->language_id]]);

        return $result[0];
    }

    public function setLanguageId($languageId)
    {
        $this->language_id = $languageId;
        return $this;
    }

    public function getCompanyName()
    {
        return $this->company_name;
    }

    public function getStreet()
    {
        if(!$this->street) {
            $this->street = $this->db->Select(['street', 'user_address', ['user_address_id' => $this->user_address_id]])[0]->street;
        }
        return $this->street;
    }

    public function getPostalCode($find = false)
    {
        if($find === true) {
            $db     = new Database();
            $db->Connect();
            $result = $db->Execute("select postal_code from user_address where user_address_id = " . $this->user_address_id)[0]->postal_code;
            return $result;
        }
        return $this->postal_code;
    }

    public function getCity($find = false)
    {
        if($find === true) {
            $db     = new Database();
            $db->Connect();
            $result = $db->Execute("SELECT c.* FROM user_address ua "
                    . "INNER JOIN cities c ON c.cityId = ua.city_id "
                    . "WHERE user_address_id = " . $this->user_address_id);

            return $result[0];
        }
        return $this->city;
    }

    public function getRegion()
    {
        $db     = new Database();
        $db->Connect();
        $result = $db->Execute("SELECT r.* FROM user_address ua "
                . "INNER JOIN cities c ON c.cityId = ua.city_id "
                . "INNER JOIN region r ON r.regionId = c.regionId "
                . "WHERE user_address_id = " . $this->user_address_id);

        return $result[0];
    }

    public function getCountry()
    {
        $db     = new Database();
        $db->Connect();
        $result = $db->Execute("SELECT cc.* FROM user_address ua "
                . "INNER JOIN cities c ON c.cityId = ua.city_id "
                . "INNER JOIN country cc ON cc.countryId = c.countryId "
                . "WHERE user_address_id = " . $this->user_address_id);

        return $result[0];
    }

    public function getCellPhone()
    {
        return $this->cell_phone;
    }

    public function setCompanyName($companyName)
    {
        $this->company_name = $companyName;
        return $this;
    }

    public function setStreet($street)
    {
        $this->street = $street;
        return $this;
    }

    public function setPostalCode($postalCode)
    {
        $this->postal_code = $postalCode;
        return $this;
    }

    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    public function setCellPhone($cellPhone)
    {
        $this->cell_phone = $cellPhone;
        return $this;
    }

}
