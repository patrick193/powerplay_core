<?php

namespace Modules\Roles;

/**
 * @author Pavel Petrov
 */
trait AuthUserTrait {

    protected $countryId;
    protected $regionId;
    protected $cityId;
    protected $user_password;
    protected $via_social;
    protected $confirm_code;
    protected $photo;

    public function setRegionId($regionId) {
        $this->regionId = $regionId;
        return $this;
    }

    public function getRegionId() {
        return $this->regionId;
    }
    
    public function setCityId($cityId) {
        $this->cityId = $cityId;
        return $this;
    }

    public function getCityId() {
        return $this->cityId;
    }

    public function setCountryId($countryId) {
        $this->countryId = $countryId;
        return $this;
    }

    public function getCountryId() {
        return $this->countryId;
    }

    public function setUserPassword($userPassword) {
        $this->user_password = $userPassword;
        return $this;
    }

    public function getUserPassword() {
        return $this->user_password;
    }

    public function setViaSocial($social) {
        $this->via_social = $social;
        return $this;
    }

    public function getViaSocial() {
        return $this->via_social;
    }

    public function setConfirmCode($confirmCode) {
        $this->confirm_code = $confirmCode;
        return $this;
    }

    public function getConfirmCode() {
        return $this->confirm_code;
    }

    public function setPhoto($path) {
        $this->photo = $path;
        return $this;
    }

    public function getPhoto() {
        return $this->photo;
    }

}
