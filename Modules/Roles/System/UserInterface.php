<?php

namespace Modules\Roles\System;

use PowerPlay\ModuleLoader\Model;

/**
 *
 * @author Developer Pohorielov Vladyslav
 */
interface UserInterface {

    public function getParent($userId);

    public function getChild($userId);

    public function CheckAccess($id, Model $module);
}
