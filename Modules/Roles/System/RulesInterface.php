<?php

namespace Modules\Roles\System;

/**
 *
 * @author Developer Pohorielov Vladyslav
 */
interface RulesInterface {
    
    /**
     * Get a structure of roles
     */
    public function Dependencies();
    
    /**
     * Function to get a parent or all parents for the role
     * @param int $id
     */
    public function getParent($id);
    
    /**
     * Finction to get all children or one child for the role
     * @param int $id
     */
    public function getChild($id);
    
    /**
     * Function for determination can whether role create another role
     * @param int $id Role id who you want to check
     * @return boolean True if can and False if can not
     */
    public function CanCreate($id);
    
    
}
