<?php

namespace Modules\Login\Models;

use Modules\Login\Security\Security;

/**
 * Description of tMLogin
 *
 * @author Developer Pohorielov Vladyslav
 */
trait tMLogin {

    protected $client_id_f;
    protected $client_secret_f;
    protected $redirect_uri_f;
    protected $url_f;
    protected $client_id_g;
    protected $client_secret_g;
    protected $redirect_uri_g;
    protected $url_g;

    /**
     * Password
     * @var pwd 
     */
    protected $pwd;

    public function getClientIdF() {
        return $this->client_id_f;
    }

    public function getClientSecretF() {
        return $this->client_secret_f;
    }

    public function getRedirectUriF() {
        return $this->redirect_uri_f;
    }

    public function getUrlF() {
        return $this->url_f;
    }

    public function getClientIdG() {
        return $this->client_id_g;
    }

    public function getClientSecretG() {
        return $this->client_secret_g;
    }

    public function getRedirectUriG() {
        return $this->redirect_uri_g;
    }

    public function getUrlG() {
        return $this->url_g;
    }

    /**
     * Set a new pass
     * @param string $pwd
     * @return char
     */
    public function setPwd($pwd) {
        $this->pwd = Security::GetEncrypted($pwd);
        return $this;
    }

    /**
     * get password
     * @return type
     */
    public function getPwd() {
        return $this->pwd;
    }

}
