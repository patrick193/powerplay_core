<?php

namespace Modules\Login\Models;

use Modules\Login\Login;
use Modules\Roles\Users;
use PowerPlay\Mailer\Mailer;
use PowerPlay\PowerplayException\PowerplayException;

class Registration extends Login {

    /**
     * Function for first step in reg
     * @param string $email user email
     * @param string $pass User pass
     */
    public function RegistrationStepOne($email, $pass) {
        if(!$email or ! $pass) {
            return false;
        }
//        if(preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $email)) {
//            throw new PowerplayException(MOD_EMAIL);
//        }
        if(!$this->CheckUserEmail($email)) {
            throw new PowerplayException(MOD_USER_EXIST);
        }
        if(strlen($pass) < 6) {
            throw new PowerplayException(MOD_PASS_LEN);
        }

        $this->session->set('email', $email);
        $this->session->set('pass', md5(md5($pass)));

        return true;
    }

    /**
     * Function for reg second step
     * @param array $args Array of arguments for registration after second step
     * @return boolean
     * @throws PowerplayException
     */
    public function RegistrationStepTwo($args) {
        if(!is_array($args)) {
            throw new PowerplayException(MOD_EMPTY);
        }
        if(!$this->session->get('email') or !$this->session->get('pass')){
            throw new PowerplayException(MOD_ACCESS);
        }
        $isAgree = $args['agree'];
        $city = $args['city'];
        $region = $args['region'];
        $country = $args['country'];

//        if(!$isAgree) {
//            throw new PowerplayException(MOD_AGREE);
//        }
        if($this->CheckCity($city, $region, $country) === false) {
            throw new PowerplayException(MOD_DIFF_CITY);
        }
        $languageId = 1;
        $firstName = $args['fname'];
        $lastName = $args['lname'];
        $company = $args['company'];
        $street = $args['street'];
        $postalCode = $args['postal'];
        $cellPhone = $args['cell_phone'];
        $email = $this->session->get('email');
        $pass = $this->session->get('pass');
//        $this->session->Remove('email');
//        $this->session->Remove('pass');
        $user = new Users();
        $userRule = $user->Create();
        $userRule->setIsLocked(0);
        $userRule->setCellPhone($cellPhone);
        $userRule->setCityId($city);
        $userRule->setCompanyName($company);
        $userRule->setCountryId($country);
        $userRule->setLanguageId($languageId);
        $userRule->setParentId(8);
        $userRule->setPostalCode($postalCode);
        $userRule->setStreet($street);
        $userRule->setRegionId($region);
        $userRule->setUserEmail($email);
        $userRule->setUserPassword($pass);
        $userRule->setRoleId(4);
        $userRule->setUserFirstName($firstName);
        $userRule->setUserLastName($lastName);

        $this->session->set('user_auth', @serialize($userRule));

        $userCompleate = $user->Execute($userRule, 1);


        $this->session->set('user_auth', @serialize($userCompleate));
        $this->SendEmail();
        return true;
    }

    /**
     * Function for sending an emil after registration
     */
    protected function SendEmail() {
        $user = @unserialize($this->session->get('user_auth'));
        $code = md5($user->getUserEmail() . $user->getUserId()) . "$!" . $user->getUserId();

        $emailConfig = $this->yaml->GetConfigurations(__DIR__ . "/../Config/LoginCustomConfig_" . $this->session->get('language') . ".yml");
        $title = str_replace("{user}", $user->getUserFirstName() . " " . $user->getUserLastName(), $emailConfig->MessageTitle);
        $confirm = str_replace('{code}', $code, str_replace('{site}', \Config::getSite(), $emailConfig->ConfirmUrl));
        $messageEnd = str_replace("{site}", \Config::getSite(), $emailConfig->MessageEnd);

        $mailer = new Mailer();
        $mailer->Send(['to' => $user->getUserEmail(), 'subject' => $emailConfig->ConfirmSubject,
            'message' => $title . "\n" . $emailConfig->Message . $confirm . $messageEnd]);
    }

    /**
     * Function for confirm code
     * @param string $code Code from url wich we sent to user by email
     * @return boolean
     * @throws PowerplayException
     */
    public function ConfirmLink($code) {
        if(!$code) {
            throw new PowerplayException(MOD_EMPTY);
        }
        $parameters = explode("$!", $code);
        $userId = $parameters[1];
        $hash = $parameters[0];

        $users = new Users();
        $user = $users->Load(['user_id', $userId]);
        $userHash = md5($user->getUserEmail() . $user->getUserId());

        if($hash === $userHash) {
            $this->db->Update([['isLocked' => 0], 'powerplay_users', ['user_id' => $userId]]);
            $user->setIsLocked(0);
            $this->session->DestroySession();
            $this->session->set('user_auth', @serialize($user));
            return true;
        }
        return false;
    }

    /**
     * Function for checking user email in the database
     * @param string $email
     * @return boolean
     * @throws PowerplayException
     */
    private function CheckUserEmail($email) {
        if(!$email) {
            throw new PowerplayException(MOD_EMPTY);
        }

        $count = $this->db->Count("user_email", 'powerplay_users', ['user_email' => $email]);
        if($count > 0) {
            return false;
        }
        return true;
    }

    /**
     * 
     * @param int $cityId
     * @param int $regionId
     * @param int $countryId
     * @return boolean
     */
    private function CheckCity($cityId, $regionId, $countryId) {
        $city = $this->db->Count("*", 'cities', ['cityId' => $cityId, 'regionId' => $regionId,
            'countryId' => $countryId]);
        if($city > 0) {
            return true;
        }
        return false;
    }

}
