<?php

namespace Modules\Login\Models;

use Modules\Authorization\Models\tMLogin;
use PowerPlay\PowerplayException\PowerplayException;
use Modules\Authorization\Security\Security;
use Modules\Login\Login;

class MLogin extends Login{

    use tMLogin;


    public function __construct() {
        $socialConf = $this->yaml->GetConfigurations(\Bootstrap::getDir() . "Login/Config/LoginSocialConfig.yml");
        $this->client_id_f = $socialConf->ClientIdF;
        $this->client_secret_f = $socialConf->ClientSecretF;
        $this->redirect_uri_f = $socialConf->RedirectUriF;
        $this->url_f = $socialConf->UrlF;

        $this->client_id_g = $socialConf->ClientIdG;
        $this->client_secret_g = $socialConf->ClientSecretG;
        $this->redirect_uri_g = $socialConf->RedirectUriG;
        $this->url_g = $socialConf->UrlG;
    }

    /**
     * Filling object AuthUser during registration.
     * @param type $obj
     * @param type $args
     * @return type
     */
    protected function Filling($obj, $args) {
        $obj->setCompanyName($args['company']);
        $obj->setUserFirstName($args['fname']);
        $obj->setUserLastName($args['lname']);
        $obj->setStreet($args['street']);
        $obj->setPostalCode((int) $args['postal']);
        $obj->setCityId((int) $args['city']);
        $obj->setRegionId(abs((int) $args['region']));
        $obj->setCountryId(abs((int) $args['country']));
        $obj->setCellPhone(abs((string) $args['cell_phone']));
        ($obj->getViaSocial() == 1) ? $obj->setUserPassword(Security::GeneratePassword()) : '';
        $obj->setParentId(1);
        $obj->setLanguageId(abs((int) $args['language_id']));
        $obj->setRoleId(4);
        $obj->setIsLocked(1);
        $obj->setConfirmCode((string) md5(md5($obj->getUserEmail()) . rand(000000, 999999)));
        return $obj;
    }

    /**
     * the definition of what to check user
     * @param string $login
     * @throws PowerplayException
     */
    public function SelectionMethod($login) {
        if(is_null($login)) {
            throw new PowerplayException(MOD_EMPTY);
        }
        $user = new Modules\Roles\Users();
        $user->setUserPassword($this->pwd);
        if($this->checking->CheckEmail($login)) {
            $user->setUserEmail($login);
            $userL = $this->checking->CheckUser('user_email', $user->getUserEmail(), $user);
            return $this->User($userL);
        } else {
            $user->setUserId($login);
            $userL = $this->checking->CheckUser('user_id', $user->getUserId(), $user);
            return $this->User($userL);
        }
    }

    private function User($user) {
        if(is_object($user)) {
            $this->session->set('user_auth', serialize($user));
            return 1;
        }
        return 0;
    }

    

    /**
     * Authorization via social
     * @param array $args
     * @throws PowerplayException
     */
    public function AuthSocial($args) {
        if(is_null($args)) {
            throw new PowerplayException(MOD_EMPTY);
        }
        $user = new \Modules\Roles\Users();
        echo $this->checking->CheckUser('social', $args[0], $user);
    }

    
    /**
     * Selection method for working with Facebook
     * @param string $args
     */
    public function FacebookSelectionData($args) {
        if(isset($_GET['code'])) {
            $args['code'] = str_replace('code=', '', $_SERVER['QUERY_STRING']) . '#_=_';
            if(empty($args) or !is_array($args)) {
                throw new PowerplayException(MOD_EMPTY);
            }
            $params = array(
                'client_id' => $this->getClientIdF(),
                'client_secret' => $this->getClientSecretF(),
                'code' => $args['code'],
                'redirect_uri' => $this->getRedirectUriF()
            );
            $url = 'https://graph.facebook.com/oauth/access_token';
            $tokenInfo = null;
            parse_str(file_get_contents($url . '?' . http_build_query($params)), $tokenInfo);
            if(isset($tokenInfo['access_token'])) {
                if($this->session->get('Reg') == 1) {
                    $this->FacebookReg($tokenInfo['access_token']);
                } else {
                    $this->FacebookAuth($tokenInfo['access_token']);
                }
            }
        }
    }

    /**
     * Registration via Facebook
     * @param string $token
     * @throws PowerplayException
     */
    public function FacebookReg($token) {
        $userInfo = json_decode(file_get_contents('https://graph.facebook.com/v2.5/me?fields=first_name,last_name,email,work{employer},location&access_token=' . $token), TRUE);
        $user = new Modules\Roles\Users();
        if($this->db->Count('*', 'powerplay_users', ['user_email' => $userInfo['email']]) > 0) {
            throw new PowerplayException(MOD_USER_EXISTS, 'user');
        }
        if($photo = file_get_contents('https://graph.facebook.com/' . $userInfo['id'] . '/picture?type=large&access_token=' . $token)) {
            $path = Config::$imagePath . 'user_profile/' . $userInfo['email'] . '.jpg';
            file_put_contents($path, $photo);
            $user->setPhoto($path);
        } else {
            $user->setPhoto('');
        }
        $this->checking->CheckDir();
        $location = explode(',', $userInfo['location']['name']);
        (!isset($userInfo['first_name'])) ? $user->setUserFirstName('') : $user->setUserFirstName($userInfo['first_name']);
        (!isset($userInfo['last_name'])) ? $user->setUserLastName('') : $user->setUserLastName($userInfo['last_name']);
        $user->setUserEmail($userInfo['email']);
        (!isset($userInfo['work'][0]['employer']['name'])) ? $user->setCompanyName('') : $user->setCompanyName($userInfo['work'][0]['employer']['name']);
        $user->setLanguageId(1);
        if(isset($location[1])) {
            $query = 'SELECT `countryId` FROM `country` WHERE `name` LIKE (\'' . trim($location[1]) . '\') LIMIT 1';
            $this->db->Connect();
            $country = $this->db->Execute($query);
            $user->setCountryId($country[0]->countryId);
            $query = 'SELECT `cityId` FROM `cities` WHERE `name` LIKE (\'' . ($location[0]) . '\') AND `countryId` =' . $user->getCountryId() . ' LIMIT 1';
            $city = $this->db->Execute($query);
            $user->setCity($city[0]->cityId);
        } else {
            $user->setCountryId('');
            $user->setCity('');
        }
        $user->setRoleId(4);
        $user->setParentId(1);
        $user->setIsLocked(1);
        $user->setViaSocial(1);
        $this->session->set('user_auth', @serialize($user));
        // return $user; //Under question
    }

    /**
     * Authorization via Facebook
     * @param string $token
     */
    public function FacebookAuth($token) {
        $userInfoF = json_decode(file_get_contents('https://graph.facebook.com/v2.4/me?fields=email&access_token=' . $token), TRUE);
        $args[] = $userInfoF['email'];
        $this->AuthSocial($args);
    }

    /**
     * Selection method for working with Google
     * @param string $code
     * @throws PowerplayException
     */
    public function GoogleGetData($code) {
        if(!isset($code)) {
            throw new PowerplayException(MOD_EMPTY_DATA);
        }
        $params = array(
            "code" => $code,
            "client_id" => $this->getClientIdG(),
            "client_secret" => $this->getClientSecretG(),
            "redirect_uri" => $this->getRedirectUriG(),
            "grant_type" => 'authorization_code'
        );
        $url = 'https://accounts.google.com/o/oauth2/token';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query($params)));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($curl);
        curl_close($curl);
        $tokenInfo = json_decode($result, true);
        $userInfo = json_decode(file_get_contents('https://www.googleapis.com/oauth2/v1/userinfo?access_token=' . $tokenInfo['access_token']), TRUE);
        if($this->session->get('Reg') == 1) {
            $this->GoogleReg($userInfo);
        } else {
            $args[] = $userInfo['email'];
            $this->AuthSocial($args);
        }
    }

    /**
     * Authorization via Google
     * @param array $args
     * @throws PowerplayException
     */
    private function GoogleReg($args) {
        if($this->db->Count('*', 'powerplay_users', ['user_email' => $args['email']]) > 0) {
            throw new PowerplayException(MOD_USER_EXISTS, 'user');
        }
        $user = new AuthUser();
        (empty($args['given_name'])) ? $user->setUserFirstName('') : $user->setUserFirstName($args['given_name']);
        (empty($args['family_name'])) ? $user->setUserLastName('') : $user->setUserLastName($args['family_name']);
        $user->setUserEmail($args['email']);
        $user->setViaSocial(1);
        $this->checking->CheckDir();
        if(!empty($args['picture'])) {
            $path = Config::$imagePath . 'user_profile/' . $args['email'] . '.jpg';
            copy($args['picture'], $path);
            $user->setPhoto($path);
        } else {
            $user->setPhoto('');
        }
        $this->session->set('user_auth', @serialize($user));
//        return $user;  //Under question
    }

}
