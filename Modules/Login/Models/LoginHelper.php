<?php

namespace Modules\Login\Models;

use Modules\Roles\Users;
use Modules\Login\Login;
use PowerPlay\Mailer\Mailer;
use PowerPlay\PowerplayException\PowerplayException;

class LoginHelper extends Login {

    /**
     * Function for log in
     * @param email $email Email of user who want log in
     * @param type $password Password
     * @return boolean
     * @throws PowerplayException
     */
    public function Login($email, $password) {

        if(!$email or ! $password) {
            throw new PowerplayException(MOD_EMPTY);
        }
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new PowerplayException(MOD_MAIL);
        }
        $users = new Users();
        $user = $users->Load(['user_email, user_password', $email . ", " . md5(md5($password))]);

        if(is_object($user)) {
            $this->session->set('user_auth', @serialize($user));
            return true;
        }

        return false;
    }

    
    /**
     * Function forgot password. Link generated from user email and id in the system. <br>
     * Email writed in the session, and after following the link emails shoulb be compared
     * @param string $email
     * @return boolean
     * @throws PowerplayException
     */
    public function ForgotPass($email) {
        if(!$email or ! filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new PowerplayException(MOD_EMAIL);
        }
        $users = new Users();
        $user = $users->Load(['user_email', $email]);
        if(!is_object($user)) {
            throw new PowerplayException(MOD_USER);
        }
        $this->session->set('email',$email);
        $link = md5($user->getUserEmail()) . "!$" . md5($user->getUserId());
        $linkCansel = md5("cancel" . $user->getUserEmail());
        
        $config = $this->yaml->GetConfigurations(__DIR__ . "/../Config/LoginCustomConfig_" . $this->session->get('language') . ".yml");
        $message = str_replace("{user}", $user->getUserFirstName() . " " . $user->getUserLastName(),
                str_replace("{site}", \Config::getSite(), str_replace("{link}", $link, str_replace("linkCancel", $linkCansel, $config->ForgotPassText))));
        $to = $user->getUserEmail();
        $subject = $config->ForgotPassSubject;
        
        $mailer = new Mailer();
        $mailer->Send(['to' => $to, 'message' => $message, 'subject' => $subject]);
        return true;
    }
    
    /**
     * Function for compare a link with hash
     * @param string $link
     * @return int
     * @throws PowerplayException
     */
    public function ConfirmLink($link) {
        if(!$link){
            throw new PowerplayException(MOD_EMTY);
        }
        $params = explode("!$", $link);
        $userEmail = $this->session->get('email');
        if(!$userEmail or !  filter_var($userEmail, FILTER_VALIDATE_EMAIL)){
            throw new PowerplayException(MOD_EMAIL);
        }
        if($params[0] === md5($userEmail)){
            $users = new Users();
            $user = $users->Load(['user_email', $userEmail]);
            if(!is_object($user)){
                throw new PowerplayException(MOD_EMPTY);
            }
            $hash = md5($user->getUserEmail()) . "!$" . md5($user->getUserId());
            if($hash === $link){
                return true;
            }
        }
        return false;
    }

    /**
     * Function for update pss
     * @param string $pass
     * @return boolean
     * @throws PowerplayException
     */
    public function ChangePass($pass) {
        if(!$pass){
            throw new PowerplayException(MOD_EMPTY);
        }
        $userEmail = $this->session->get('email');
        if(!$userEmail){
            return false;
        }
        
        $this->db->Update([['user_password' => md5(md5($pass))], 'powerplay_users', ['user_email' => $userEmail]]);
        $this->session->DestroySession();
        return true;
    }
}
