<?php

namespace Modules\Authorization\Security;

use PowerPlay\PowerplayException\PowerplayException;

/**
 * Static class for encryption
 * 
 * @author Pavel Petrov
 */
class Security {

    private function __construct() {
        
    }

    /**
     * Method returns encrypted stryng
     * 
     * @param string $password
     * @return string
     */
    public static function GetEncrypted($password) {
        if (!is_string($password)) {
            throw new PowerplayException(MOD_STRING);
        }
        if (!ctype_xdigit($password)) {
            return md5(md5($password));
        } else {
            return md5($password);
        }
    }

    /**
     * Method returns random not encrypted password
     * @return string
     */
    public static function GeneratePassword() {
        $length   = 8;
        $chars    = 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789';
        $numChars = strlen($chars);
        $string   = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= substr($chars, rand(1, $numChars) - 1, 1);
        }
        return $string;
    }

}
