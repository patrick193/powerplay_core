<?php

namespace Modules\Login;

use PowerPlay\Module;
use PowerPlay\Session;
use PowerPlay\PowerplayException\PowerplayException;
use Modules\Authorization\Models\MAuthorization;
use Modules\Login\Models\Registration;

class Login extends Module {

    private $dir = 'login/';
    protected $mAuth;

    /**
     * Authorization user via Email or Id
     * @param array $args
     */
    public function actionAuth($args) {
        if(empty($args)) {
            $this->Render('', '', 'login/EnterPage/index');
        }
        $loginHelper = new Models\LoginHelper();
        if($loginHelper->Login($args['login'], $args['pwd'])) {
            $this->redirect('usermanagement');
        } else {
            $this->redirect('');
        }
    }

    public function actionRegistration($args) {
        if(!$args['email'] or ! $args['pwd']) {
            throw new PowerplayException(MOD_EMPTY);
        }
        $registration = new Registration();
        $registration->RegistrationStepOne($args['email'], $args['pwd']);
        $this->display($this->dir . "step/index");
    }

    public function actionStepTwo($args) {
        if(!is_array($args)) {
            throw new PowerplayException(MOD_EMPTY);
        }
        $helper = new Registration();
        $helper->RegistrationStepTwo($args);
        $this->actionPage(['step3']);
    }

    public function actionMailValid($args) {
        if(!is_array($args)) {
            throw new PowerplayException(MOD_EMPTY);
        }
    }

    public function actionPhoneValid() {
        
    }

    public function actionPage($args) {
        if(!$args['page'] or empty($args['page'])) {
            $args['page'] = 'index';
        }
        $this->display($this->dir . $args['page'] . "/index");
    }

    public function actionConfirmLink($args) {
        if(!$args['code']) {
            throw new PowerplayException(MOD_EMPTY);
        }
        $registration = new Registration();
        if($registration->ConfirmLink($args['code'])) {
            $this->redirect('usermanagement');
        } else {
            $this->redirect('login');
        }
    }

    public function actionForgotPass($args) {
        if(!$args['email']) {
            throw new PowerplayException(MOD_EMPTY);
        }
        $lh = new Models\LoginHelper();
        if($lh->ForgotPass($args['email'])) {
            echo 'sent';
        } else {
            $this->actionPage(['page' => 'forgot']);
        }
    }

    public function actionFConfirm($args) {
        if(!$args['link']) {
            throw new PowerplayException(MOD_EMPTY);
        }

        $lh = new Models\LoginHelper();
        if($lh->ConfirmLink($args['link'])) {
            $this->actionPage(['page' => 'confirmLink']);
        } else {
            $this->actionPage(['page' => 'forgot']);
        }
    }

    public function actionChangePass($args) {
        if(!$args['pass']) {
            throw new PowerplayException(MOD_EMPTY);
        }

        $lh = new Models\LoginHelper();
        if($lh->ChangePass($args['pass'])) {
            $this->redirect('');
        } else {
            $this->redirect('login/page/forgot');
        }
    }

    /**
     * Generate URL for Facebook depending on the data to be obtained
     * @param string $type. 
     */
    protected function FacebookUrl($type = 'Auth') {
        $params = array(
            'client_id' => $this->mAuth->getClientIdF(),
            'redirect_uri' => $this->mAuth->getRedirectUriF(),
            'response_type' => 'code',
            'scope' => ($type == 'Reg') ? 'email,user_work_history,user_birthday,user_location,user_photos' : 'email'
        );
        ($type == 'Reg') ? $this->sess->set('Reg', 1) : $this->sess->set('Reg', 0);
        header('Location: ' . $this->mAuth->getUrlF() . '?' . urldecode(http_build_query($params)));
    }

    /**
     * Generate URL for Google
     */
    protected function GoogleUrl() {
        $params = array(
            'response_type' => 'code',
            'client_id' => $this->mAuth->getClientIdG(),
            'redirect_uri' => $this->mAuth->getRedirectUriG(),
            'scope' => 'email'
        );
        header('Location: ' . $this->mAuth->getUrlG() . '?' . urldecode(http_build_query($params)));
    }

    public function actionMain() {
        $this->Render('', '', 'login/EnterPage/index');
    }

    /**
     * 1 step of registration via Facebook.
     */
    public function actionFacebookReg() {
        $this->mAuth = new MAuthorization();
        $this->sess = new Session();
        $this->FacebookUrl('Reg');
    }

    /**
     * Authorization via Facebook
     */
    public function actionFacebookAuth() {
        $this->mAuth = new MAuthorization();
        $this->sess = new Session();
        $this->FacebookUrl();
    }

    /**
     * method which returns the call Fasebook
     * @param array $args
     */
    public function actionFacebook($args) {
        if(empty($args['code'])) {
            $this->Render('', '', 'login/Reg-with-social/index.tpl');
        } else {
            $this->mAuth = new MAuthorization();
            $user = $this->mAuth->FacebookSelectionData($args);
            $this->Render('data', 'sdfsdf', 'login/Regist-step-2/index');
        }
    }

    /**
     * 1 step of registration via Google.
     */
    public function actionGoogleReg() {
        $this->mAuth = new MAuthorization();
        $this->sess = new Session();
        $this->sess->set('Reg', 1);
        $this->GoogleUrl();
    }

    /**
     * Authorization via Google
     */
    public function actionGoogleAuth() {
        $this->mAuth = new MAuthorization();
        $this->sess = new Session();
        $this->sess->set('Reg', 0);
        $this->GoogleUrl();
    }

    /**
     * method which returns the call Google
     * @param array $args
     */
    public function actionGoogle($args) {
        if(empty($args) or ! is_array($args)) {
            throw new PowerplayException(MOD_EMPTY);
        }
        $this->mAuth = new MAuthorization();
        $code = $args['code'] . "/" . $args['code1'] . '#';
        $this->mAuth->GoogleGetData($code);
        $this->Render('data', 'sdfsdf', 'login/Regist-step-2/index');
    }

}
