<?php

namespace Modules\Geo;

use PowerPlay\Module;
use Modules\Geo\Helpers\GeoHelper;
use PowerPlay\PowerplayException\PowerplayException;

/**
 * @author Pavel Petrov
 */
class Geo extends Module
{

    public function actionCountry($args)
    {
        if (!isset($args['key'])) {
            throw new PowerplayException(MOD_EMPTY);
        }

        $geoHelper = new GeoHelper($this->db);

        echo $geoHelper->getCountryByName($args['key']);
    }

    public function actionRegion($args)
    {
        if (!isset($args['country_id'])) {
            throw new PowerplayException(MOD_EMPTY);
        }
        if (!isset($args['key'])) {
            throw new PowerplayException(MOD_EMPTY);
        }

        $geoHelper = new GeoHelper($this->db);
        
        echo $geoHelper->getRegionByName($args['country_id'], $args['key']);
    }
    
    public function actionCity($args)
    {
        if (!isset($args['region_id'])) {
            throw new PowerplayException(MOD_EMPTY);
        }
        if (!isset($args['key'])) {
            throw new PowerplayException(MOD_EMPTY);
        }
        
        $geoHelper = new GeoHelper($this->db);
        echo $geoHelper->getCityByName($args['region_id'], $args['key']);
    }

    public function Render($variableName, $variableValue, $template)
    {
        
    }

}
