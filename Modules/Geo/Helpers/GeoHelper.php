<?php

namespace Modules\Geo\Helpers;

use PowerPlay\Database;
use PowerPlay\PowerplayException\PowerplayException;

/**
 * @author Pavel Petrov
 */
class GeoHelper
{

    private $db;

    public function __construct(Database $db)
    {
        $this->db = $db;
        $this->db->Connect();
    }

    public function getCountryByName($name)
    {
        $query = "SELECT countryId as id, name as name FROM country WHERE name LIKE '$name%'";
        return json_encode(['items' => $this->db->Execute($query)]);
    }

    public function getRegionByName($countryId, $name)
    {
        $query = "SELECT regionId as id, name as name FROM region WHERE countryId = $countryId AND name LIKE '$name%'";
        return json_encode(['items' => $this->db->Execute($query)]);
    }

    public function getCityByName($regionId, $name)
    {
        $query = "SELECT cityId as id, name as name FROM cities WHERE regionId = $regionId AND name LIKE '$name%'";
        return json_encode(['items' => $this->db->Execute($query)]);
    }

}
