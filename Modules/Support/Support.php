<?php

namespace Modules\Support;

use PowerPlay\Module;
use Modules\Support\SubModules\Tickets\Tickets;
use Modules\Support\SubModules\FAQ\tFAQ;
use Modules\Support\SubModules\FAQ\tFAQCategory;
use PowerPlay\PowerplayException\PowerplayException;

/**
 * Description of Support
 *
 * @author Developer Pohorielov Vladyslav
 */
class Support extends Module
{

    use tFAQ,
        tFAQCategory;

    public function actionMain()
    {
        
    }

    public function actionTicketCategory($args = null)
    {
        $page       = isset($args['page']) && $args['page'] > 0 ? $args['page'] : 1;
        $tickets    = new Tickets();
        $categories = $tickets->getPaginatedCategories($page, $args['category']);

        $this->moduleName  = 'Ticket categories';
        $this->breadscrumb = ['home' => '/dashboard', 'support' => '/support/ticket/all',
            'tickets categories' => ''];

        $userHelper = new \Modules\UserManagement\Helpers\UserHelper();
        $users      = $userHelper->getAll(true);
        $this->addVariable('users', $users);
        $this->addVariable('current_page', $page <= $categories['page_num'] ? $page : $categories['page_num']);

        $this->Render('categories', $categories, 'Support/Tickets/New-Ticket/index');
    }

    public function actionSearchCategoriesByName($args)
    {
        $searchQuery = $args['query'];

        $tickets    = new Tickets();
        $categories = $tickets->getCategoriesByName($searchQuery);
        $categories = json_encode(['items' => $categories]);
        echo $categories;
    }

    public function actionCreateTicket($args)
    {
        $tickets    = new Tickets();
        $categories = $tickets->getAllCategories();
        $priority   = $tickets->getAllPriority();

        $this->addVariable('priority', $priority);
        $this->addVariable('categoryId', $args['category_id']);

        $this->moduleName  = 'Support';
        $this->breadscrumb = ['home' => '/dashboard', 'support' => '/support/ticket/all',
            'create ticket' => ''];

        $userHelper = new \Modules\UserManagement\Helpers\UserHelper();
        $users      = $userHelper->getAll(true);
        $this->addVariable('users', $users);

        $this->Render('categories', $categories, 'Support/Tickets/Create-Ticket/index');
    }

    public function actionAddTicket($args)
    {
        if(!is_array($args)) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        $ticket   = new Tickets();
        $ticketId = $ticket->AddTicket($args);
        $this->redirect("support/ticket/show/$ticketId");
    }

    public function actionAnswerTicket($args)
    {
        $ticket = new Tickets();
        $ticket->AnswerTicket($args);
    }

    public function actionStatusTicket($args)
    {
        $ticket = new Tickets();
        $ticket->TicketClose((int) $args['ticket_id']);
    }

    public function actionChangeTicket($args)
    {
        $ticket = new Tickets();
        $ticket->ChangeSupport((int) $args['ticket_id'], (int) $args['support_id']);
    }

    public function actionTicketsAll($args = [])
    {
        $ticket     = new Tickets();
        $page       = isset($args['page']) && $args['page'] > 0 ? $args['page'] : 1;
        $categoryId = isset($args['category_id']) ? (int) $args['category_id'] : null;
        $date       = isset($args['date']) ? new \DateTime($args['date']) : null;
        $orderField = isset($args['order_field']) ? (string) $args['order_field'] : null;
        $orderRule  = isset($args['order_rule']) ? (string) $args['order_rule'] : null;

        $result = $ticket->getAllTickets($categoryId, $date, $orderField, $orderRule, $page);

        if($categoryId) {
            $this->session->set('ticket_category', $categoryId);
        } elseif(isset($args['category_id'])) {
            $this->session->remove('ticket_category');
        } else {
            $categoryId = $this->session->get('ticket_category');
        }

        if($page) {
            $this->session->set('ticket_page', $page);
        }

        $tickets    = new Tickets();
        $categories = $tickets->getAllCategories();
        $this->addVariable('categories', $categories);
        $this->addVariable('current_page', $page <= $result['page_num'] ? $page : $result['page_num']);

        $this->moduleName  = 'My tickets';
        $this->breadscrumb = ['home' => '/dashboard', 'support' => '/support/ticket/all',
            'my tickets' => ''];

        $userHelper = new \Modules\UserManagement\Helpers\UserHelper();
        $users      = $userHelper->getAll(true);
        $this->addVariable('users', $users);

        $this->Render('result', $result, 'Support/Tickets/Supp-Tickets/index');
    }

    public function actionShowAddForm()
    {
        $ticket = new Tickets();
        $ticket->Render('addForm');
    }

    public function actionShowAnswerForm()
    {
        $tickets = new Tickets();
        $tickets->Render();
    }

    public function actionShowTicket($args)
    {
        $tickets = new Tickets();
        $ticket  = $tickets->getTicket((int) $args['id']);
        $answers = $tickets->getAnswers($ticket->getTicketId());

        $this->addVariable('answers', $answers);

        $this->moduleName  = 'Tickets';
        $this->breadscrumb = ['home' => '/dashboard', 'support' => '/support/ticket/all',
            'ticket' => ''];

        $userHelper = new \Modules\UserManagement\Helpers\UserHelper();
        $users      = $userHelper->getAll(true);
        $this->addVariable('users', $users);

        $this->Render('ticket', $ticket, 'Support/Tickets/Show-Ticket/index');
    }

}
