<?php

namespace Modules\Support\SubModules\FAQ;

use Modules\Support\SubModules\FAQ\FAQ;
use PowerPlay\PowerplayException\PowerplayException;

/**
 * @author Pavel Petrov
 */
trait tFAQ
{

    public function actionShowFaq($args = null)
    {
        $faq = new FAQ();

        $search     = $args['search'];
        $categoryId = isset($args['category_id']) ? $args['category_id'] : null;
        $page       = isset($args['page']) && $args['page'] > 0 ? $args['page'] : 1;

        if ($search) {
            $this->session->set('faq_search', $search);
        } elseif (isset($args['search']) && !is_null($args['search'])) {
            $this->session->remove('faq_search');
        } else {
            $search = $this->session->get('faq_search');
        }
        if ($categoryId) {
            $this->session->set('faq_category', $categoryId);
        } elseif (isset($args['category_id'])) {
            $this->session->remove('faq_category');
        } else {
            $categoryId = $this->session->get('faq_category');
        }

        $faqs = $faq->ShowFaq($search, (int) $categoryId, (int) $page);

        $faqCategory = new FAQCategory();
        $categories  = $faqCategory->ShowFaqCategory();

        $userHelper = new \Modules\UserManagement\Helpers\UserHelper();
        $users = $userHelper->getAll(true);
        
        $this->addVariable('users', $users);
        $this->addVariable('current_page', $page <= $faqs['page_num'] ? $page : $faqs['page_num']);
        $this->addVariable('categories', $categories);
        
        $this->moduleName  = 'FAQ';
        $this->breadscrumb = ['home' => '/dashboard', 'support' => '/support/ticket/all', 'faq' => ''];
        
        $this->Render('faqs', $faqs, 'Support/FAQ/index');
    }

    public function actionAddAnswer($args)
    {
        if (!is_array($args)) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        if (!isset($args['faq_category_id'])) {
            throw new PowerplayException(MOD_SET_UP_DATA);
        }


        $faq           = new FAQ();
        $faqCategoryId = (int) $args['faq_category_id'];
        $faqQuestion   = $args['faq_question'];
        $faqAnswer     = $args['faq_answer'];

        if (empty($faqCategoryId)) {
            throw new PowerplayException(MOD_INT);
        }

        $result = $faq->AddAnswer($faqCategoryId, $faqQuestion, $faqAnswer);

        if ($result) {
            die('success');
            // RENDER THAT DATA IS SUCCESSFULLY STORED
        }
    }


    public function actionEditAnswer($args)
    {
        if (!$args['faq_id'] or ! $args['faq_category_id'] or ! $args['faq_question'] or ! $args['faq_answer']) {
            throw new PowerplayException(MOD_SET_UP_DATA);
        }


        $faq           = new FAQ();
        $faqId         = (int) $args['faq_id'];
        $faqCategoryId = (int) $args['faq_category_id'];
        $faqQuestion   = $args['faq_question'];
        $faqAnswer     = $args['faq_answer'];

        if (!is_int($faqId) or ! is_int($faqCategoryId)) {
            throw new PowerplayException(MOD_INT);
        }
        if (empty($faqId) or empty($faqCategoryId)) {
            throw new PowerplayException(MOD_EMPTY);
        }

        $result = $faq->EditAnswer($faqId, $faqCategoryId, $faqQuestion, $faqAnswer);

        if ($result) {
            die('success');
            // RENDER THAT DATA IS SUCCESSFULLY UPDATED
        }
    }


    public function actionRemoveAnswer($args)
    {
        if (!isset($args['faq_id'])) {
            throw new PowerplayException(MOD_SET_UP_DATA);
        }
        $faqId = (int) $args['faq_id'];
        if (empty($faqId)) {
            throw new PowerplayException(MOD_INT);
        }


        $faq    = new FAQ();
        $result = $faq->RemoveAnswer($faqId);

        if ($result) {
            die('success');
            // RENDER THAT DATA IS SUCCESSFULLY REMOVED
        }
    }

}
