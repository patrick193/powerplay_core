<?php

namespace Modules\Support\SubModules\FAQ;

use PowerPlay\Database;
use PowerPlay\PowerplayException\PowerplayException;
use PowerPlay\Pagination\Pagination;

/**
 * @author Developer Pavel Petrov
 */
class FAQ {

    private $db;

    public function __construct() {
        date_default_timezone_set('UTC');
        !$this->db ? $this->db = new Database() : '';
    }

    /**
     * Method for select powerplay_faq items, can take a search string,
     * category_id, page
     * 
     * @param string $search
     * @param int $faqCategoryId
     * @param int $page
     * @return array
     * @throws PowerplayException
     */
    public function ShowFaq($search = null, $faqCategoryId = null, $page = null) {
        $query = "SELECT * FROM powerplay_faq WHERE isLocked != '1'";
        if ($search) {
            $query .=" AND powerplay_faq_answer LIKE '%$search%'"
                    . " OR powerplay_faq_question LIKE '%$search%'";
        }
        if ($faqCategoryId) {
            if (!is_int($faqCategoryId)) {
                throw new PowerplayException(MOD_INT);
            }
            $query .=" AND category_id = $faqCategoryId";
        }
        return Pagination::paginate($query, $page);
    }

    /**
     * Method for insert new FAQ item
     * 
     * @param int $faqCategoryId
     * @param string $faqQuestion
     * @param string $faqAnswer
     * @return mixed
     */
    public function AddAnswer($faqCategoryId, $faqQuestion, $faqAnswer) {
        $session = new \PowerPlay\Session();
        $userId = @unserialize($session->get('user_auth'))->getUserId();
        $values = array(
            'category_id' => $faqCategoryId,
            'powerplay_faq_answer' => $faqAnswer,
            'powerplay_faq_question' => $faqQuestion,
            'created_at' => date("y-m-d H:i:s"),
            'created_by' => $userId ? : 1,
            'updated_at' => date("y-m-d H:i:s"),
            'updated_by' => $userId ? : 1
        );

        return $this->db->Insert([$values, 'powerplay_faq']);
    }

    /**
     * Method for update FAQ item
     * 
     * @param int $faqId
     * @param int $faqCategoryId
     * @param string $faqQuestion
     * @param string $faqAnswer
     * @return mixed
     */
    public function EditAnswer($faqId, $faqCategoryId, $faqQuestion, $faqAnswer) {
        $session = new \PowerPlay\Session();
        $userId = @unserialize($session->get('user_auth'))->getUserId();
        $values = array(
            'category_id' => $faqCategoryId,
            'powerplay_faq_question' => $faqQuestion,
            'powerplay_faq_answer' => $faqAnswer,
            'updated_at' => date("y-m-d H:i:s"),
            'updated_by' => $userId ? : 1
        );

        return $this->db->Update([$values, 'powerplay_faq', ['powerplay_faq_id' => $faqId]]);
    }

    /**
     * Method for remove FAQ item
     * 
     * @param int $faqId
     * @return mixed
     */
    public function RemoveAnswer($faqId) {
        return $this->db->Delete(['powerplay_faq', ['powerplay_faq_id' => $faqId]]);
    }

}
