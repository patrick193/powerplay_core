<?php

namespace Modules\Support\SubModules\FAQ;

use PowerPlay\Database;
use PowerPlay\Pagination\Pagination;

/**
 * @author Developer Pavel Petrov
 */
class FAQCategory
{

    private $db;

    public function __construct()
    {
        !$this->db ? $this->db = new Database() : '';
    }

    /**
     * Method for select FaqCategory items
     * 
     * @param string $search
     * @param int $page
     * @return array
     */
    public function ShowFaqCategory($search = null, $page = null)
    {
        $query = "SELECT * FROM powerplay_faq_category WHERE isLocked != '1'";
        if($search) {
            $query .=" AND category_name LIKE '%$search%'";
        }
        return Pagination::paginate($query, $page);
    }

    /**
     * Method for insert ner category item for FAQ
     * 
     * @param string $faqCategoryName
     * @return mixed
     */
    public function AddFaqCategory($faqCategoryName)
    {
        return $this->db->Insert([['category_name' => $faqCategoryName], 'powerplay_faq_category']);
    }

    /**
     * Method for update category item for FAQ
     * 
     * @param int $faqCategoryId
     * @param string $faqCategoryName
     * @return mixed
     */
    public function EditFaqCategory($faqCategoryId, $faqCategoryName)
    {
        return $this->db->Update([['category_name' => $faqCategoryName], 'powerplay_faq',
                    ['faq_category_id' => $faqCategoryId]]);
    }

    /**
     * Method for remove category
     * 
     * @param int $faqCategoryId
     * @return mixed
     */
    public function RemoveFaqCategory($faqCategoryId)
    {
        return $this->db->Delete(['powerplay_faq_category', ['faq_category_id' => $faqCategoryId]]);
    }

}
