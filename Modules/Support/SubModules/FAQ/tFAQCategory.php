<?php

namespace Modules\Support\SubModules\FAQ;

use Modules\Support\SubModules\FAQ\FAQCategory;
use PowerPlay\PowerplayException\PowerplayException;

/**
 * @author Pavel Petrov
 */
trait tFAQCategory {

    public function actionAddFaqCategory($args) {
        if (!isset($args['faq_category_name'])) {
            throw new PowerplayException(MOD_SET_UP_DATA);
        }
        $faqCategory = new FAQCategory();
        $result = $faqCategory->AddFaqCategory($args['faq_category_name']);

        if ($result) {
            die('success');
            // RENDER THAT DATA IS SUCCESSFULLY STORED
        }
    }

    public function actionShowFaqCategory($args) {
        $faqCategory = new FAQCategory();
        $result = $faqCategory->ShowFaqCategory($args['search'], $args['page']);
        if ($result) {
            echo '<pre>' . print_r($result, 1) . '</pre>';
            die;
        }
    }

    public function actionEditFaqCategory($args) {
        if (!isset($args['faq_category_id']) or ! isset($args['faq_category_name'])) {
            throw new PowerplayException(MOD_SET_UP_DATA);
        }
        $faqCategory = new FAQCategory();
        $result = $faqCategory->EditFaqCategory($args['faq_category_id'], $args['faq_category_name']);
        if ($result) {
            die('success');
            // RENDER THAT DATA IS SUCCESSFULLY UPDATED
        }
    }

    public function actionRemoveFaqCategory($args) {
        if (!isset($args['faq_category_id'])) {
            throw new PowerplayException(MOD_SET_UP_DATA);
        }
        $categoryId = (int) $args['faq_category_id'];
        if (empty($categoryId)) {
            throw new PowerplayException(MOD_INT);
        }
        $faqCategory = new FAQCategory();
        $result = $faqCategory->RemoveFaqCategory($categoryId);

        if ($result) {
            die('success');
            // RENDER THAT DATA IS SUCCESSFULLY REMOVED
        }
    }

}
