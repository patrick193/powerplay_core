<?php

namespace Modules\Support\SubModules\Tickets\Helpers;

use PowerPlay\Database;
use PowerPlay\PowerplayException\PowerplayException;

/**
 * Description of MailHelper
 *
 * @author Developer Pohorielov Vladyslav
 */
class MailHelper {

    private $tableName, $fieldName, $ticketId, $fieldSearch, $tableNameSearch, $whereConditional, $str, $object;
    private $db;

    /**
     * Function to set all parameters up
     * @param string $str Param from config file
     * @param int $ticketId Ticket id in the database
     * @return boolean
     * @throws Exception
     */
    public function setParameters($str, $ticketId, $object) {
        if(!is_string($str) or ! is_int($ticketId)) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        if(!is_object($object)) {
            $session = new \PowerPlay\Session();
            $user = @unserialize($session->get('user_auth'));
            if($user) {
                $this->object = $user;
            }
        } else {
            $this->object = $object;
        }
        $this->str = $str;
        if(strpos($str, "{") !== false) {
            preg_match_all("|\{([^)]+?)\}|", $str, $info);
            for($i = 0, $count = count($info[1]); $i < $count; $i++) {
                if($info[1][$i]) {
                    if(strpos($info[1][$i], "|")) {
                        $fields = explode("|", $info[1][$i]);
                        $param = explode(".", $fields[0]);
                        $search = explode(".", $fields[1]);
                        $conditionals = explode("=", $search[1]);
                        $this->fieldSearch[$i] = $conditionals[0]; // for diferent conitionals
                        $this->whereConditional[$i] = $conditionals[1]; // for diferent conitionals
                        $this->tableNameSearch[$i] = $search[0]; // for diferent conitionals
                    } else {
                        $param = explode(".", $info[1][$i]);
                    }
                    $this->tableName[$i] = $param[0];
                    $this->fieldName[$i] = $param[1];
                    $this->ticketId[$i] = $ticketId;
                    
                } else {
                    throw new PowerplayException(MOD_WRONG);
                }
            }
        }
        return false;
    }

    public function getData() {
        if(!$this->tableName or ! $this->fieldName or ( count($this->fieldName) !== count($this->tableName))) {
            throw new PowerplayException('Set up all data please.');
        }
        $this->db = new Database();
        for($i = 0, $c = count($this->tableName); $i < $c; $i++) {
            $conditionals = function() use($i) {
                if(isset($this->fieldSearch[$i])) {
                    $queryResult = '';
                    if($this->object) {
                        $whereCond = function() use($i) {
                            if($this->whereConditional[$i]) {
                                $method = 'get' . ucfirst($this->ConvertString($this->whereConditional[$i]));
                                $d = $this->object->$method();
                                if($d == 0) {
                                    $d = 1;
                                }
                                return $d;
                            }
                        };
                        $get = $this->fieldSearch[$i];
                        $queryResult = $this->db->Select([$this->fieldSearch[$i],
                                    $this->tableNameSearch[$i], [$this->whereConditional[$i] => $whereCond()]])[0]->$get;
                        $queryResult = [$this->fieldSearch[$i] => $queryResult];
                    } else {
                        throw new PowerplayException(MOD_OBJECT_DID_NOT_FIND);
                    }
                    return $queryResult;
                } else {
                    $method = 'get' . ucfirst($this->ConvertString($this->fieldName[$i]));
                    return [$this->fieldName[$i] => $this->object->$method()];
                }
            };
            $method = $this->fieldName[$i];

            $data = $this->db->Select([[$this->fieldName[$i]], $this->tableName[$i],
                        $conditionals()])[0]->$method;

            if($this->str and is_string($data)) {
                $this->Replace($data, $this->tableName[$i], $this->fieldName[$i], $this->whereConditional[$i], $this->tableNameSearch[$i], $this->fieldSearch[$i]);
            }
        }
        unset($this->fieldName, $this->fieldSearch, $this->object, $this->tableName, $this->tableNameSearch, $this->ticketId, $this->whereConditional);
        return $this->str;
    }

    private function Replace($data, $tableName, $fieldName, $whereConditional = null, $tableNameSearch = null, $fieldSearch = null) {
        if(!is_string($data)) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }

        if(!is_null($whereConditional) or ! is_null($tableNameSearch) or ! is_null($fieldSearch)) {
            $c = count($this->tableNameSearch);
            if(strpos($this->str, "{" . $tableName . "." . $fieldName . "|" . $tableNameSearch . "." . $fieldSearch . "=" . $whereConditional . "}")) {
                $this->str = str_replace("{" . $tableName . "." . $fieldName . "|" . $tableNameSearch . "." . $fieldSearch . "=" . $whereConditional . "}", $data, $this->str);
            }
        } else {
            if(strpos($this->str, "{" . $tableName . "." . $fieldName . "}")) {
                $this->str = str_replace("{" . $tableName . "." . $fieldName . "}", $data, $this->str);
            }
        }
    }

    private function ConvertString($str) {
        if(!is_string($str)) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        $parms = explode("_", $str);
        $data = $parms[0];
        for($i = 1, $count = count($parms); $i < $count; $i++) {
            $data .= ucfirst($parms[$i]);
        }

        return $data;
    }

}
