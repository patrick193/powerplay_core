<?php

namespace Modules\Support\SubModules\Tickets\Helpers\TicketModels;


/**
 * Description of TicketStatus
 *
 * @author Developer Pohorielov Vladyslav
 */
class TicketStatus {

    protected $ticket_status_id;
    
    protected $status_name;
    
    public function getTicketStatusId() {
        return $this->ticket_status_id;
    }

    public function getStatusName() {
        return $this->status_name;
    }

    public function setTicketStatusId($ticket_status_id) {
        $this->ticket_status_id = $ticket_status_id;
    }

    public function setStatusName($status_name) {
        $this->status_name = $status_name;
    }

}
