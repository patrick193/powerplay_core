<?php

namespace Modules\Support\SubModules\Tickets\Helpers\TicketModels;

/**
 * Description of Ticket_answer
 *
 * @author Developer Pohorielov Vladyslav
 */
class TicketMessage {

    protected $ticket_message_id;
    protected $message;
    protected $subject;
    protected $user_id;

    public function getTicketMessageId() {
        return $this->ticket_message_id;
    }

    public function getMessage() {
        return $this->message;
    }

    public function getSubject() {
        return $this->subject;
    }

    public function getUserId() {
        return $this->user_id;
    }

    public function setTicketMessageId($ticket_message_id) {
        $this->ticket_message_id = $ticket_message_id;
    }

    public function setMessage($message) {
        $this->message = $message;
    }

    public function setSubject($subject) {
        $this->subject = $subject;
    }

    public function setUserId($user_id) {
        $this->user_id = $user_id;
    }



}
