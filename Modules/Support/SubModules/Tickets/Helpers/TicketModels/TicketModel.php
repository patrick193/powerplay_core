<?php

namespace Modules\Support\SubModules\Tickets\Helpers\TicketModels;

use Modules\Support\SubModules\Tickets\Helpers\TicketModels\TicketAnswers;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TicketModel
 *
 * @author Developer Pohorielov Vladyslav
 */
abstract class TicketModel extends TicketAnswers {

    protected $ticket_id;
    protected $ticket_status_id;
    protected $ticket_category_id;
    protected $user_support_id;
    protected $ticket_priority_id;
    protected $date;
    protected $time;
    protected $ticket_message_id;

    public function getTicketId() {
        return $this->ticket_id;
    }

    public function getTicketStatusId() {
        return $this->ticket_status_id;
    }
    
    public function getCountry() {
        $users = new \Modules\Roles\Users();
        $user = $users->Load(['user_id', $this->user_id]);
        $country = $user->getCountry()->name;
        return $country;
    }

    public function getTicketCategoryId() {
        return $this->ticket_category_id;
    }
    
    public function getCategory() {
        $db = new \PowerPlay\Database();
        $category = $db->Select(['category_name', 'ticket_category', ['ticket_category_id' => $this->ticket_category_id]])[0]->category_name;
        return $category;
    }

    public function getUserSupportId() {
        return $this->user_support_id;
    }

    public function getTicketPriorityId() {
        return $this->ticket_priority_id;
    }

    public function getDate() {
        return $this->date;
    }

    public function getTime() {
        return $this->time;
    }

    public function getTicketMessageId() {
        return $this->ticket_message_id;
    }

    public function setTicketId($ticket_id) {
        $this->ticket_id = $ticket_id;
    }

    public function setTicketStatusId($ticket_status_id) {
        $this->ticket_status_id = $ticket_status_id;
    }

    public function setTicketCategoryId($ticket_category_id) {
        $this->ticket_category_id = $ticket_category_id;
    }

    public function setUserSupportId($user_support_id) {
        $this->user_support_id = $user_support_id;
    }

    public function setTicketPriorityId($ticket_priority_id) {
        $this->ticket_priority_id = $ticket_priority_id;
    }

    public function setDate($date) {
        $this->date = $date;
    }

    public function setTime($time) {
        $this->time = $time;
    }

    public function setTicketMessageId($ticket_message_id) {
        $this->ticket_message_id = $ticket_message_id;
    }

    public function getStatus() {
        $db = new \PowerPlay\Database();
        $status = $db->Select(['status_name', 'ticket_status', ['ticket_status_id' => $db->Select(['ticket_status_id',
                            'powerplay_tickets', ['ticket_id' => $this->ticket_id]])[0]->ticket_status_id]])[0]->status_name;
        return $status;
    }

    abstract public function AddTicket($args);

    abstract public function AnswerTicket($args);

    abstract public function getTicket($ticketId);
}
