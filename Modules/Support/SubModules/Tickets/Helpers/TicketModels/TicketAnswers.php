<?php

namespace Modules\Support\SubModules\Tickets\Helpers\TicketModels;

use Modules\Support\SubModules\Tickets\Helpers\TicketModels\TicketMessage;
/**
 * Description of TicketAnswers
 *
 * @author Developer Pohorielov Vladyslav
 */
class TicketAnswers extends TicketMessage{

    protected $ticket_answers_id;
    protected $ticket_message_id;
    protected $ticket_question;
    protected $ticket_answer;
    protected $answer_user_id;
    protected $answer_date;

    public function getTicketAnswersId() {
        return $this->ticket_answers_id;
    }

    public function getTicketMessageId() {
        return $this->ticket_message_id;
    }

    public function getTicketQuestion() {
        return $this->ticket_question;
    }

    public function getTicketAnswer() {
        return $this->ticket_answer;
    }

    public function getAnswerUserId() {
        return $this->answer_user_id;
    }

    public function getAnswerDate() {
        return $this->answer_date;
    }

    public function setTicketAnswersId($ticket_answer_id) {
        $this->ticket_answers_id = $ticket_answer_id;
    }

    public function setTicketMessageId($ticket_message_id) {
        $this->ticket_message_id = $ticket_message_id;
    }

    public function setTicketQuestion($ticket_question) {
        $this->ticket_question = $ticket_question;
    }

    public function setTicketAnswer($ticket_answer) {
        $this->ticket_answer = $ticket_answer;
    }

    public function setAnswerUserId($user_id) {
        $this->answer_user_id = $user_id;
    }

    public function setAnswerDate($answer_date) {
        $this->answer_date = $answer_date;
    }

}
