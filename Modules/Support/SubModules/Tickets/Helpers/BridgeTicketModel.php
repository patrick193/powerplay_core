<?php

namespace Modules\Support\SubModules\Tickets\Helpers;

use Modules\Support\SubModules\Tickets\Helpers\TicketModels\TicketModel;
use PowerPlay\Database;
use PowerPlay\Session;
use PowerPlay\YamlConfiguration;
use PowerPlay\Storage;
use PowerPlay\PowerplayException\PowerplayException;
use PowerPlay\Pagination\Pagination;

/**
 * Description of Tickets
 *
 * @author Developer Pohorielov Vladyslav
 */
class BridgeTicketModel extends TicketModel
{

    private $db, $user;

    public function __construct()
    {
        $this->db   = new Database();
        $session    = new Session();
        $this->user = unserialize($session->get('user_auth'));
        if(!$this->user) {
            throw new PowerplayException('We can not find any users');
        }
        date_default_timezone_set('UTC');
    }

    public function getAllCategories()
    {
        $this->db->Connect();
        return $this->db->Execute("SELECT * FROM ticket_category");
    }

    public function getPaginatedCategories($page, $categoryId = null)
    {
        if($categoryId) {
            return Pagination::paginate("SELECT * FROM ticket_category WHERE ticket_category_id = $categoryId", $page);
        } else {
            return Pagination::paginate("SELECT * FROM ticket_category", $page);
        }
    }

    public function getCategoriesByName($query)
    {
        $this->db->Connect();
        return $this->db->Execute("SELECT ticket_category_id AS id, category_name AS name FROM ticket_category WHERE category_name LIKE '$query%'");
    }

    public function getAllPriority()
    {
        return $this->db->Select(['*', 'ticket_priority']);
    }

    /**
     * Function for adding a new ticket
     * @param array $args
     * @return boolean
     * @throws Exception
     */
    public function AddTicket($args)
    {
        if(!is_array($args)) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        $inserMessage = $this->db->Insert([['message' => str_replace("_", " ", $args['message']),
        'subject' => $args['subject'], 'user_id' => $this->user->getUserId()], 'ticket_message']);

        $ticketMessageId = $this->db->InsertId();


        $insertTicket = $this->db->Insert([['ticket_status_id'   => 4, 'ticket_category_id' => $args['category_id'],
        'user_support_id'    => $this->user->getParentId(), 'ticket_priority_id' => $args['priority_id'],
        'ticket_message_id'  => $ticketMessageId, 'date'               => date("y-m-d H:i:s")],
            'powerplay_tickets']);

        $ticketId = $this->db->LastInsert();

        $insertAnswer = $this->db->Insert([['ticket_message_id' => $ticketMessageId,
        'ticket_question'   => $args['message']],
            'ticket_answers']);
        if(!$inserMessage or ! $insertTicket or ! $ticketMessageId) {
            throw new PowerplayException(MOD_WRONG);
        }

        $this->setTicketCategoryId($args['category_id']);
        $this->setTicketPriorityId($args['priority_id']);
        $this->setDate(date("y-m-d"));
        $this->setTime(date("H:i:s"));
        $this->setTicketStatusId(4);
        $this->setTicketId($ticketId);
        $this->setUserSupportId($args['user_support_id']);
        $this->setTicketMessageId($ticketMessageId);

        $this->UserNotificate((int) $this->user->getParentId(), $ticketId);
        return $ticketId;
    }

    /**
     * Function to answer on the ticket
     * @param array $args
     * @return boolean
     * @throws Exception
     */
    public function AnswerTicket($args)
    {
        if(!is_array($args) or ( empty($args['ticket_id']) or ! isset($args['ticket_id']))) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        $storage = new Storage();
        $role    = $storage->getValue('Roles');
        if($role !== null) {
            $roleObjet = $role->getModule()->FindById((int) $this->user->getRoleId());
        } else {
            $roleObjet = new \Modules\Roles\Roles();
        }
        $status = $this->getRules($roleObjet);

        $messageId = $this->db->Select(['MAX(`ticket_answers_id`)', 'ticket_answers',
            ['ticket_message_id' => $this->db->Select([['ticket_message_id'], 'powerplay_tickets',
                    ['ticket_id' => $args['ticket_id']]]) [0]->ticket_message_id]]); //find a message id in the db

        $method         = key($messageId[0]);
        $ticketAnswer   = $this->db->Select(['`ticket_answer`', 'ticket_answers',
            ['ticket_answers_id' => $messageId[0]->$method]]); //find an answer
        $ticketAnswer === true ? $ticketQuestion = $this->db->Select(['`message`',
                            'ticket_message', ['ticket_message_id' => $this->db->Select([[
                                'ticket_message_id'], 'powerplay_tickets', ['ticket_id' => $args['ticket_id']]]) [0]->ticket_message_id]]) [0]->message : ''; //ticket answer

        if($roleObjet->getGroupId() == 1 or $roleObjet->getRoleCode() == '2B') {

            $ticketA = $this->db->Select(['`ticket_answer`', 'ticket_answers', ['ticket_answers_id' => $messageId[0]->$method]])[0]->ticket_answer;

            if(!empty($ticketA)) {
                $insertAnswer = $this->db->Insert([['ticket_message_id' => $this->db->Select([[
                'ticket_message_id'], 'powerplay_tickets', ['ticket_id' => $args['ticket_id']]]) [0]->ticket_message_id,
                'ticket_answer'     => $args['answer'],
                'answer_user_id'    => $this->user->getUserId(), 'answer_date'       => date("y-m-d H:i:s")],
                    'ticket_answers']);
            } else {
                $insertAnswer = $this->db->Update([['ticket_answer'  => $args['answer'],
                'answer_user_id' => $this->user->getUserId(), 'answer_date'    => date("Y-m-d H:m:s")],
                    'ticket_answers', ['ticket_answers_id' => $messageId[0]->$method]]);
            }

            $userId = $this->db->Select([['user_id'], 'ticket_message', ['ticket_message_id' => $this->db->Select([[
                            'ticket_message_id'], 'powerplay_tickets', ['ticket_id' => $args['ticket_id']]]) [0]->ticket_message_id]]) [0]->user_id;
            $this->UserNotificate((int) $userId, $args['ticket_id'], 'Answered');
        } else {
            $insertAnswer = $this->db->Insert([['ticket_message_id' => $this->db->Select([[
            'ticket_message_id'], 'powerplay_tickets', ['ticket_id' => $args['ticket_id']]]) [0]->ticket_message_id,
            'ticket_question'   => $args['answer'],
            'answer_user_id'    => $this->user->getUserId(), 'answer_date'       => date("y-m-d H:i:s")],
                'ticket_answers']);
        }

        if($insertAnswer) {
            $updateStatus = $this->StatusTicket(['ticket_id' => $args['ticket_id'],
                'status'    => $status]);
        }

        $ticket = $this->getTicket((int) $args['ticket_id']);


        if($ticket) {
            return $ticket;
        } else {
            throw new PowerplayException('Somthing is wrong. Please contact with Developers');
        }
    }

    /**
     * Function to change a ticket's status
     * @param array $args
     * @return type
     * @throws Exception
     */
    public function StatusTicket($args)
    {
        if(!is_array($args) or ( empty($args['ticket_id']) or ! isset($args['ticket_id']))) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        if(!isset($args['status']) or empty($args['status'])) {
            $storage = new Storage();
            $role    = $storage->getValue('Roles');
            if($role !== null) {
                $roleObjet = $role->getModule()->FindById((int) $this->user->getRoleId());
            } else {
                $roleObjet = new \Modules\Roles\Roles();
            }
            $args['status'] = $this->getRules($roleObjet);
        }
        $statusBefore = $this->db->Select(['`ticket_status_id`', 'powerplay_tickets',
                    ['ticket_id' => $args['ticket_id']]]) [0]->ticket_status_id;

        if($args['status']['StatusBeforAnswer'] == $statusBefore or $statusBefore == 4) {
            $statusAfter = $args['status']['StatusAfterAnswer'];
        } else {
            $statusAfter = 3;
        }
        $status = $this->db->Update([['ticket_status_id' => $statusAfter], 'powerplay_tickets',
            ['ticket_id' => $args['ticket_id']]]);

        if(!$status) {
            throw new PowerplayException(MOD_DATABASE_CONNECTION);
        }
        return $status;
    }

    /**
     * Function for getting one ticket
     * @param int $ticketId
     * @return array
     * @throws Exception
     */
    public function getTicket($ticketId)
    {
        if(!is_int($ticketId)) {
            throw new PowerplayException(MOD_INT);
        }
        $this->db->setExecute(false);

        $select = $this->db->Select([['ticket_message_id'],
            'powerplay_tickets', ['ticket_id' => $ticketId]]);

        $join = $this->db->Join([['ticket_answers', 'powerplay_tickets'], 'right',
            ['ticket_answers.ticket_message_id' => '']]);
        $join = str_replace("=", " IN ", $join) . '(' . $select . ')';
        $join .= " LEFT JOIN `ticket_message` ON `ticket_message`.`ticket_message_id` IN (" . $select . ") WHERE `powerplay_tickets`.`ticket_id` = $ticketId";
        $join = $this->db->Execute($join, BridgeTicketModel::class)[0];

        if($join) {
            return $join;
        } else {
            throw new PowerplayException(MOD_TICKET_NOT_FIND, 404);
        }
    }

    public function getAnswers($ticketId)
    {
        if(!$ticketId or $ticketId == 0) {
            throw new PowerplayException(MOD_EMPTY);
        }
//        $this->db->setExecute(false);
        $mid     = $this->db->Select(['ticket_message_id',
                    'powerplay_tickets', ['ticket_id' => $ticketId]])[0]->ticket_message_id;
        $answers = $this->db->Select(['*', 'ticket_answers', ['ticket_message_id' => $mid]]);
        return $answers;
    }

    /**
     * Function for getting all tickets in the system
     * @return array
     * @throws Exception
     */
    public function getAllTickets($categoryId = null, $date = null, $orderField = null, $orderRule = null, $page = null)
    {
        !$this->db ? $this->db = new Database() : '';

        $query = 'SELECT t.date, t.ticket_id, tm.subject, ts.status_name, tc.category_name FROM powerplay_tickets AS t '
                . 'INNER JOIN ticket_message tm ON t.ticket_message_id = tm.ticket_message_id '
                . 'INNER JOIN ticket_status ts ON ts.ticket_status_id = t.ticket_status_id '
                . 'INNER JOIN ticket_category tc ON tc.ticket_category_id = t.ticket_category_id';

        $query .= $this->addJoin($orderField);
        $query .= $this->setFilters($categoryId, $date);
        $query .= $this->setSort($orderField, $orderRule);

        return Pagination::paginate($query, $page);
    }

    /**
     * Method to set filters
     * 
     * @param string $query
     * @param int $categoryId
     * @param DateTime $date
     * 
     * @return string $query
     */
    private function setFilters($categoryId, $date)
    {
        $query = '';
        if($categoryId) {
            $query .= " WHERE t.ticket_category_id = '$categoryId'";
            if($date) {
                $date = $date->format('y-m-d');
                $query .= " AND date = '$date'";
            }
        }

        if($date and ! $categoryId) {
            $date = $date->format('y-m-d');
            $query .= " WHERE date = '$date'";
        }
        return $query;
    }

    /**
     * Method adds JOIN for query by orderField
     * 
     * @param string $query
     * @param string $orderField
     * 
     * @return string $query
     */
    private function addJoin($orderField)
    {
        $query = '';
        switch($orderField) {
            case 'priority':
                $query.=' INNER JOIN ticket_priority AS tp ON tp.ticket_priority_id = t.ticket_priority';
                break;
            case 'status':
                $query.=' INNER JOIN ticket_status AS ts ON ts.ticket_status_id = t.ticket_status_id';
                break;
            case 'category':
                $query.=' INNER JOIN ticket_category AS tc ON tc.ticket_category_id = t.ticket_category_id';
                break;
        }
        return $query;
    }

    /**
     * Method adds sorts to the $query by $orderField
     * 
     * @param string $query
     * @param string $orderField
     * @param string $orderRule
     * 
     * @return string $query
     */
    private function setSort($orderField, $orderRule)
    {
        $query = '';
        switch($orderField) {
            case 'priority':
                $query .= " ORDER BY tp.priority_name $orderRule";
                break;
            case 'status':
                $query .= " ORDER BY ts.status_name $orderRule";
                break;
            case 'category':
                $query .= " ORDER BY tc.category_name $orderRule";
                break;
            case 'date':
                $query .= " ORDER BY date, time $orderRule";
                break;
            case 'ticketId':
                $query .= " ORDER BY t.ticket_id $orderRule";
                break;
            default :
                $query .= " ORDER BY t.ticket_id";
                break;
        }

        return $orderRule ? $query : $query . " DESC";
    }

    /**
     * Function to close one ticket(change a status)
     * @param int $ticketId
     * @return boolean
     * @throws Exception
     */
    public function TicketClose($ticketId)
    {
        if(!is_int($ticketId)) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        $this->db->Update([['ticket_status_id' => 1], 'powerplay_tickets', ['ticket_id' => $ticketId]]);
        $userId = $this->db->Select([['user_id'], 'ticket_message', ['ticket_message_id' => $this->db->Select([[
                        'ticket_message_id'], 'powerplay_tickets', ['ticket_id' => $ticketId]]) [0]->ticket_message_id]]) [0]->user_id;
        $this->UserNotificate((int) $userId, $ticketId, 'Closed');
        return true;
    }

    /**
     * Function for changing a support on the ticket. <br>
     * Support can be enyone frome 1 group(1A, 1S) or a parent of the creator
     * @param int $ticketId
     * @param int $supportId
     * @throws Exception
     */
    public function ChangeSupport($ticketId, $supportId)
    {
        if(!is_int($ticketId) or ! is_int($supportId)) {
            throw new PowerplayException(MOD_WRONG_PARAMETERS_TYPE);
        }
        $user    = new \Modules\Roles\Users();
        $support = $user->Load(['user_id', (int) $supportId]);
        $dbUser  = $this->db->Select([['user_id'], 'ticket_message', ['ticket_message_id' => $this->db->Select(['ticket_message_id',
                            'powerplay_tickets', ['ticket_id' => $ticketId]]) [0]->ticket_message_id]]) [0]->user_id;
        $dbUser  = $user->Load(['user_id', (int) $dbUser]);
        if(!is_object($dbUser) or ! is_object($support)) {
            throw new PowerplayException('We can not find any user on this ticket.');
        }
        $roles = new \Modules\Roles\Roles();
        $role  = $roles->FindById((int) $support->getRoleId());
        unset($user, $roles);
        if($role->getGroupId() == 1 or $dbUser->getParentId() == $support->getUserId()) {
            $this->db->Update([['user_support_id' => $supportId], 'powerplay_tickets',
                ['ticket_id' => $ticketId]]);
        } else {
            throw new PowerplayException(MOD_TICKET_POSSIBILITY);
        }
    }

    /**
     * Function for generating a notify to user
     * @param int $userId
     * @param int $ticketId
     * @param string $messageType
     * @throws PowerplayException
     */
    public function UserNotificate($userId, $ticketId, $messageType = 'Added')
    { //it is just a dummy
        if(!is_int($userId)) {
            throw new PowerplayException(MOD_USER_ID);
        }
        $userEmail = $this->db->Select([['user_email'], 'powerplay_users', ['user_id' => $userId]])[0]->user_email;

        $yaml = new \PowerPlay\YamlConfiguration();

        $language = $this->db->Select([['language_code'], 'powerplay_language', ['language_id' => $this->db->Select([[
                        'language_id'], 'powerplay_users', ['user_id' => $userId]])[0]->language_id]])[0]->language_code;

        $mailConfig = $yaml->GetConfigurations(\Bootstrap::getDir() . "/Support/SubModules/Tickets/Config/MailNotificationConfig_" . $language . ".yml");
        $mailHelper = new MailHelper();

        $object = $this->getObjectType($ticketId, $messageType);

        $mailHelper->setParameters($mailConfig->AllowedTemplates[$messageType]['Text'], (int) $ticketId, $object);
        $message = $mailHelper->getData();


        if(strpos($mailConfig->header, "{") === false and strpos($mailConfig->header, "}") === false) {
            $header = $mailConfig->header;
        } else {
            $mailHelper->setParameters($mailConfig->header, (int) $ticketId);
            $header = $mailHelper->getData();
        }

        $mailHelper->setParameters($mailConfig->subject, (int) $ticketId);
        $subject = $mailHelper->getData();

        $mailer = new \PowerPlay\Mailer\Mailer();

        $args = ['message' => $message, 'header'  => $header, 'to'      => $userEmail,
            'subject' => $subject];
        $mailer->Send($args);

        $insert = $this->db->Insert([['to'                   => $userEmail, 'notification'         => $message,
        'notification_subject' => $subject, 'date'                 => date("y-m-d H:i:s")],
            'user_notification']);
    }

    public function getObjectType($ticketId, $messageType)
    {
        if($messageType) {
            switch($messageType) {
                case 'Added':
                    $ticket = $this->getTicket((int) $ticketId);
                    return $ticket;
                case 'Closed':
                    $ticket = $this->getTicket((int) $ticketId);
                    return $ticket[0];
                case 'Answered':
                    $ticket = $this->getTicket((int) $ticketId);

                    return $ticket;
                default :
                    return true;
            }
        }
    }

    /**
     * Function for geting a rules for group
     * @param \Modules\Roles\Rules\RoleRules $role
     * @return type
     * @throws Exception
     */
    private function getRules(\Modules\Roles\Rules\RoleRules $role)
    {
        if(!is_object($role)) {
            throw new PowerplayException(MOD_WRONG_ROLE_CODE);
        }
        $yaml   = new YamlConfiguration();
        $config = $yaml->GetConfigurations(\Bootstrap::getDir() . "/Support/SubModules/Tickets/Config/TicketCustomConfig.yml");
        if($config) {
            if(isset($config->Groups[$role->getGroupId()])) {
                return $config->Groups[$role->getGroupId()];
            }
        }
    }

}
