<?php

namespace Modules\Support\SubModules\Tickets;

use Modules\Support\SubModules\Tickets\Helpers\BridgeTicketModel;

/**
 * оцкнки работы сапортов
 *
 * @author Developer Pohorielov Vladyslav
 */
class Tickets extends BridgeTicketModel {

    public function __construct() {
        parent::__construct();
    }

}
