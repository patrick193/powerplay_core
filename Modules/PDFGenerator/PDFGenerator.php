<?php

namespace Modules\PDFGenerator;

use Modules\PDFGenerator\Helpers\PDFHelper;
use PowerPlay\PowerplayException\PowerplayException;
use PowerPlay\YamlConfiguration;
use PowerPlay\Module;

class PDFGenerator extends Module{

    public function __init__($id, array $products = null, $type = "Invoice") {
        if(!  is_int($id)) {
            throw new PowerplayException(MOD_EMPTY);
        }
        $yaml = new YamlConfiguration();
        $config = $yaml->GetConfigurations(__DIR__ . "/Config/PDFConfig_" . $this->session->get('language') . ".yml");

        if(is_null($products)){
            $products = $this->getProducts($id);
            
            if(!$products){
                throw new PowerplayException(MOD_PRODUCTS);
            }
        }
        $pdf = new PDFHelper('P', 'mm', 'A4');
        $pdf->AddPage();
        $pdf->addCurrentCompany($config->CurrentCompany);
        $pdf->addSociete($config->Owner, $config->Address);
        $pdf->addReference($id, "", $type);
        $cols = array(
            "$config->first" => 10,
            "$config->second" => 30,
            "$config->third" => 80,
            "$config->four" => 20,
            "$config->five" => 22,
            "$config->six" => 22);
        $pdf->addCols($cols);
        $cols = array(
            "$config->first" => '"' . strtoupper($config->positions["first"]) . '"',
            "$config->second" => '"' . strtoupper($config->positions["second"]) . '"',
            "$config->third" => '"' . strtoupper($config->positions["third"]) . '"',
            "$config->four" => '"' . strtoupper($config->positions["four"]) . '"',
            "$config->five" => '"' . strtoupper($config->positions["five"]) . '"',
            "$config->six" => '"' . strtoupper($config->positions["six"]) . '"');
        $pdf->addLineFormat($cols);

        $y = $config->YPosition;
        if(!is_array($products)) {
            
        } else {
            $i = 1;
            $total = 0;
            foreach($products as $product) {
                $sDate = new \DateTime($product->getStartDate());
                $eDate = new \DateTime($product->getEndDate());
                $days = $sDate->diff($eDate);
                $line = array(
                    "$config->first" => $i,
                    "$config->second" => $product->getName(),
                    "$config->third" => $product->getDescription(),
                    "$config->four" => $days->days,
                    "$config->five" => $product->getPrice(),
                    "$config->six" => $product->getPrice() * $days->days
                );
                $size = $pdf->addLine($y, $line);
                $total += $product->getPrice() * $days->days;
                $y += $size + 2;
                $i++;
            }
        }
        $pdf->tableEnd($y, $config->Amount, $total, $config->Tax . ' 12%', $total * 0.12, $total + ($total * 0.12));

        $pdf->AddPage();
        $p = $pdf->Output(\Config::$documentsPath ."/".$type ."_$id.pdf", 'F');

#####################
    }
    
    private function getProducts($offerId) {
        
        $this->db->setExecute(false);
        $q = $this->db->Select(['*', 'products_hr', ['products_hr_id' => $this->db->Select(['`product_id`', 'products_to_offer', ['offer_id' => $offerId]])]]);
        $q = rtrim(str_replace("= 'SELECT ", " IN (SELECT", $q), "'") . "')";
        
        $productsBD = $this->db->Execute($q, \Modules\Products\Entity\ProductHr::class);

        return $productsBD;
    }

}
