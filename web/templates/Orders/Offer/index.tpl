<!DOCTYPE html>
<html lang="en">
    {assign var='user' value=$smarty.session.register.user_auth|base64_decode|unserialize}
    <head>
        <meta charset="UTF-8">
        <title>Dashboard</title>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/web/templates/main/css/main-style.css">
        <link rel="stylesheet" href="/web/templates/main/css/Offer/jquery.fs.selecter.css">
        <link rel="stylesheet" href="/web/templates/main/css/Offer/style-ch-r.css">
        <link rel="stylesheet" href="/web/templates/main/css/Offer/style.css">
        <script src="/web/templates/main/js/jquery-1.11.3.min.js"></script>
        <!-- FOOTER END -->
        <script src="/web/templates/main/js/jquery.fs.selecter.min.js"></script>
        <script src="/web/templates/main/js/main-ds.js"></script>
        <script src="/web/templates/main/js/Offer/baron.js"></script>
        <script src="/web/templates/main/js/Offer/icheck.min.js"></script>
        <script src="/web/templates/main/js/Offer/jquery.maskedinput.min.js"></script>
        <script src="/web/templates/main/js/Offer/validate.js"></script>
        <script src="/web/templates/main/js/Offer/main.js"></script>

                {literal}
        <script>
            $(function () {
                var dataCustomer; /* all customers after ajax query */
                /* ajax query*/
                $(".select-customer-block").click(function () {
                    console.log('asdasd');
                    var user_id = {$user->getUserId()};
                    $.ajax({
                        url: '/usermanagement/get/children/',
                        dataType: 'json',
                        method: 'get',
                        data: {
                            'user': user_id
                        },
                        success: function (data) {
                            dataCustomer = data;
                            fillPopUp();
                        },
                        error: function (error) {
                            console.log("ERROR " + error);
                        }
                    });
                });

                /* function for filling up a form for selecting a customer */
                function fillPopUp() {
                    var dataA = dataCustomer;
                    $('.staff-popup-block').hide();
                    $('.popup-form').hide();
                    $('#popup-form').animate({
                        top: '-789px'
                    });
                    $('.step2-insert-menu').hide();
                    $('.offer-popup-all,.select-customer-popup-block').fadeIn(300);
                    if ($('.customer-popup-form ').height() <= 180) {
                        $('.select-customer-popup-block .scroll-bsn').css("display", "none")
                    } else {
                        $('.select-customer-popup-block .scroll-bsn').css("display", "block")
                    }
                    $.each(dataA, function (i, data) {
                        $(".customer-popup-form").append("<label for='customer-tr-" + data.user_id + "'><input type = 'radio' id = 'customer-tr-" + data.user_id
                                + "' name = '#' checked><span class = 'customer-name'> "
                                + data.user_id + " (" + data.user_first_name + " " + data.user_last_name
                                + ") </span><span class = 'customer-company' > "
                                + data.company_name + " </span><span class = 'customer-address' > "
                                + data.company_name + "<br>"
                                + data.user_first_name + " " + data.user_last_name
                                + " Street 11 65760 Eschborn </span></label>");
                    });
                    addScroll();
                }

                /** function for getting a scroll and radio buttons from the lib*/
                function addScroll() {
                    window.scroll = baron({
                        root: '.wrapper',
                        scroller: '.scroller',
                        bar: '.scroller__bar',
                        barOnCls: 'baron'
                    });
                    $('input').iCheck({
                        checkboxClass: 'checkbox-plg',
                        radioClass: 'iradio-plg',
                        increaseArea: '20%'
                    });
                }

                /* function for filling up a selected customer */
                $(".accept-customer-btn").click(function () {
                    var customerId = $(".customer-popup-form").find('.checked').children('input').attr('id').split('-')[2];
                    console.log(customerId);
                    //alert(customerId);
                    $(".step1-info-user").append(function () {
                        if (dataCustomer != 'undefined') {
                            $.each(dataCustomer, function (i, data) {
                                if (data.user_id == customerId) {
                                    $(".step1-info-user").children().remove('span');
                                    $(".step1-info-user").append("<span><span>" + data.company_name + "</span>"
                                            + "<span>" + data.user_first_name + " " + data.user_last_name + "</span>"
                                            + "<span>" + data.user_address_id + "</span>"
                                            + "<span>Street 11 65760 Eschborn</span>"
                                            + "<a href='#' class='change-customer'>Change</a>"
                                            + "<a href='#' >These customers</a>"
                                            );
                                    $(".change-customer").click(function () {
                                        if (dataCustomer != 'undefined') {
                                            fillPopUp();
                                        }
                                    });
                                }
                            })
                        }
                    });
                });
            });
        </script>
        {*        {/literal}*}
    </head>

    <body>
        <!-- Popup SMS -->
        <div id="sms-popup">
            <div class="top-sms-popup">
                <div class="logo-sms-img-popup">
                    &nbsp;
                </div>
                <div class="sms-info-block-popup">
                    <span class="from-block-popup">from VIRTURO</span>
                    <span class="time-sms-block-popup">07.08.2015 at 17:45</span>
                </div>
            </div>
            <div class="sms-popup-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna</p>
            </div>
        </div>
        <!-- Popup SMS -->
        {include file='../../main/header.tpl'}
        {include file='../../main/left_side.tpl'}
        <!-- Popup SMS -->
        <!--Popup offer page-->
        <div class="offer-popup-all" style="display: none;">
            <div class="offer-popup-grey-bg"></div>
            <div id="popup-form" class="popup-form" style="display: none;">
                <a class="img-close-bl staff-poput-close" href="#">&nbsp;</a>

                <form class="reg-form" method="" action="">
                    <div class="title-form">
                        <span class="title-t">New customer</span>

                        <p>Hello. Welcome to PowerPlay system. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                            eiusmod.
                        </p>
                    </div>
                    <div class="lang-block">
                        <span class="lang-title">Language:</span><a href="#" class="lang-brit">&nbsp;</a><a href="#"
                                                                                                            class="lang-ger">
                            &nbsp;</a>
                    </div>
                    <div class="block-name">
                        <div class="block-title-name">NAME</div>
                        <div class="firstname rel-block">
                            <span class="name-input">Firstname:</span>
                            <input type="text" class="input-firstname input-main">

                            <div class="valid-field"></div>
                            <div class="valid-error-field"><span>This field is required</span></div>
                        </div>
                        <div class="lastname rel-block">
                            <span class="name-input">Lastname:</span>
                            <input type="text" class="input-lastname input-main">

                            <div class="valid-field"></div>
                            <div class="valid-error-field"><span>This field is required</span></div>
                        </div>
                    </div>
                    <div class="block-company">
                        <div class="company rel-block">
                            <span class="name-input">Company:</span>
                            <input type="text" class="input-company input-main">

                            <div class="valid-field"></div>
                            <div class="valid-error-field"><span>This field is required</span></div>
                        </div>
                    </div>
                    <div class="block-address">
                        <div class="block-title-name">ADDRESS</div>
                        <div class="street rel-block">
                            <span class="name-input">Street and House Number:</span>
                            <input type="text" class="input-street input-main">

                            <div class="valid-field"></div>
                            <div class="valid-error-field"><span>This field is required</span></div>
                        </div>
                        <div class="postalcode-city ">
                            <div class="postalcode rel-block">
                                <span class="name-input">Postal Code:</span>
                                <input type="text" class="input-postalcod input-main">

                                <div class="valid-field"></div>
                                <div class="valid-error-field"><span>This field is required</span></div>
                            </div>
                            <div class="city rel-block">
                                <span class="name-input">City:</span>
                                <input type="text" class="input-city input-main">
                            </div>
                        </div>
                        <div class="region rel-block">
                            <span class="name-input">Region:</span>
                            <input type="text" class="input-region input-main">
                        </div>
                        <div class="country rel-block">
                            <span class="name-input">Country:</span>
                            <input type="text" class="input-country input-main">
                        </div>
                    </div>
                    <div class="block-phone rel-block">
                        <div class="phone">
                            <span class="name-input">Cell phone number:</span>
                            <input type="text" class="input-phone input-main">
                        </div>
                    </div>
                    <span class="format-phone rel-block">in format +00 (000) 633-513-65</span>

                    <div class="check rel-block">
                        <label>
                            <input type="checkbox" class="checkbox"><span class="label-text">By signing up, I agree to</span>
                            <a href="#" class="chek-link">general terms of living internet.</a>
                        </label>

                        <div class="valid-error-field"><span>This field is required</span></div>
                        <div class="btn-form">
                            <button class="btn-continue"><span>Continue</span></button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="select-customer-popup-block" style="display: none;">
                <div class="staff-poput-close"></div>
                <span class="customer-title">Select Staff</span>

                <div class="fields-group">
                    <span class="input-customer"><input type="text" placeholder="Start type name user or id"></span>
                    <select name="sel-categ">
                        <option value="">All customer</option>
                    </select>
                </div>


                <div class="scroll scroll_negative-viewport">
                    <div class="wrapper baron wrp-scroll">
                        <div class="scroller scroll-b">
                            <form action="" name="" class="">
                                <div class="customer-popup-form">

                                </div>
                            </form>
                            <div class="scroller__bar-wrapper scroll-bsn">
                                <div class="scroller__bar"></div>
                            </div>
                        </div>
                    </div>
                </div>


                <span class="count-customer">Customer: 2</span>

                <div class="buttons-group">
                    <button class="accept-customer-btn"><span>Accept customer</span></button>
                    <a href="#" class="new-customer-btn"> <span>New customer</span></a>
                </div>
            </div>
            <div style="display: none;" class="staff-popup-block">
                <div class="staff-poput-close"></div>
                <div class="scroll scroll_negative-viewport">
                    <div class="wrapper baron wrp-scroll">
                        <div class="scroller scroll-b">
                            <div class="staff-popup">
                                <form action="" name="" onclick="return false">
                                    <div class="staff-popup-top">
                                        <span class="staff-title">Select Staff</span>
                                        <select name="sel-categ">
                                            <option value="">Web development</option>
                                            <option value="">Full time</option>
                                            <option value="">AJAX</option>
                                            <option value="">MySQL</option>
                                        </select>

                                        <div class="buttons-group">
                                            <button class="add-abilities"><span>ADD abilities</span></button>
                                            <button class="apply"><span>Apply</span></button>
                                        </div>
                                    </div>
                                </form>
                                <div class="staff-popup-middle">
                                    <form action="" name="">
                                        <div class="select-staff-form">
                                            <label for="staff-tr-1">
                                                <span class="bg-inp-r"></span>
                                                <input type="radio" id="staff-tr-1" name="#" checked>
                                                <span class="speciality-span">PHP Programmer</span>
                                                <span class="level-span">Middle</span>
                                                <span class="skillset-span">
                                                    <a href="#">#PHP</a>,
                                                    <a href="#">#Basic Javascript</a>,
                                                    <a href="#">#MySQL</a>,
                                                    <a href="#">#Basic HTML</a>,
                                                    <a href="#">#Basic CSS</a>,
                                                    <a href="#">#CodeIgniter</a>,
                                                    <a href="#">#Net Beans</a>,
                                                    <a href="#">#PHPMyAdmin</a>.
                                                </span>
                                                <span class="price-m-span">1200.11 Euro</span>
                                            </label>
                                            <label for="staff-tr-2">
                                                <span class="bg-inp-r"></span>
                                                <input type="radio" id="staff-tr-2" name="#">
                                                <span class="speciality-span">PHP Programmer</span>
                                                <span class="level-span">Middle</span>
                                                <span class="skillset-span">
                                                    <a href="#">#PHP</a>,
                                                    <a href="#">#Basic Javascript</a>,
                                                    <a href="#">#MySQL</a>,
                                                    <a href="#">#Basic HTML</a>,
                                                    <a href="#">#Basic CSS</a>,
                                                    <a href="#">#CodeIgniter</a>,
                                                    <a href="#">#Net Beans</a>,
                                                    <a href="#">#PHPMyAdmin</a>.
                                                </span>
                                                <span class="price-m-span">1200.11 Euro</span>
                                            </label>
                                        </div>
                                    </form>
                                </div>
                                <div class="staff-popup-bottom">
                                    <span class="specialist-count">Specialist count: 234</span>
                                    <button class="accept-button"><span>Accept</span></button>
                                </div>
                                <div class="scroller__bar-wrapper scroll-bsn">
                                    <div class="scroller__bar"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- HEADER -->
        <!-- HEADER END -->
        <!-- LEFTBAR -->

        <!-- LEFTBAR END -->
        <!-- CONTENT -->
        <div id="container">
            <div class="content">
                <div class="content-body">
                    <div class="content-top-block">
                        <div class="offer-step1">
                            <div class="offer-step1_wrapper">
                                <div class="step1-block step1-left-block">
                                    <div class="title-step1">
                                        <span>{$user->getUserFirstName()} {$user->getUserLastName()} -&nbsp;</span>
                                        <span>{$user->getStreet()} - <br>{$user->getPostalCode(true)} {$user->getCity(true)->name }</span>
                                    </div>
                                    <div class="step1-info-user">
                                        {*<span><span>Living Internet GmbH</span>*}
                                        {*<span>  Björn Wesarg</span>*}
                                        {*<span>  Rahmannstrasse 11</span>*}
                                        {*<span>  65760 Eschborn</span>*}
                                        {*</span>*}
                                        {*<a href="#">Change</a>*}
                                        {*<a href="#">These customers</a>*}
                                    </div>
                                    <div class="select-custome-block">
                                        <span>Step 1: Select a <span>custome</span>.</span>
                                        <button class="select-customer-block"><span>Select customer</span></button>
                                    </div>
                                </div>
                                <div class="step1-block step1-center-block">
                                    <div class="title">How to reach us:</div>
                                    <ul>
                                        <li>E-mail:</li>
                                        <li>Phone:</li>
                                        <li>Fax:</li>
                                        <li>Mobile:</li>
                                        <li>Tax NR:</li>
                                        <li>UStID:</li>
                                    </ul>
                                    <ul>
                                        <li>office@livinginternet.de</li>
                                        <li>+49 69-34-87-660-0</li>
                                        <li>+49 69-34-87-660-99</li>
                                        <li>+49 69-34-87-660-99</li>
                                        <li>04523819390</li>
                                        <li>DE267854834</li>
                                    </ul>
                                </div>
                                <div class="step1-block step1-right-block">
                                    <form action="" method="" name="">
                                        <div class="date-input">
                                            <div class="date-input-icon"></div>
                                            <input type="text">
                                        </div>
                                        <div class="duration-block">
                                            <span>Duration: </span>

                                            <div class="duration-count">
                                                <input type="text" property="0">

                                                <div class="duration-up-down">
                                                    <div class="duration-up"></div>
                                                    <div class="duration-down"></div>
                                                </div>
                                            </div>
                                            <span>Number <br> from 0 to 30</span>

                                            <p class="text-after-duration">This offer is valid till 10.12.2015 (include)</p>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="offer-step1-comment-block">
                            <textarea name="" id="" placeholder="Comment"></textarea>
                        </div>
                        <div class="offer-step2">
                            <div class="offer-step2-wrapper">
                                <span>Offer 10001</span>

                                <div class="select-invoice-line-block">
                                    <span>Step 2: Add an <span>invoice line</span>.</span>

                                    <div class="offer-new-fields">
                                        <span class="title">Staff</span>
                                        <table class="offer-staff-table">
                                            <tr>
                                                <th class="offer-th priority-th"><span>Speciality</span></th>
                                                <th class="offer-th unit-price-th"><span>Unit price (incl. VAT)</span></th>
                                                <th class="offer-th month-th"><span>Month</span></th>
                                                <th class="offer-th tot-price-th"><span>Total price (incl. VAT)</span></th>
                                            </tr>
                                            <tr>
                                                <td class="spec-td"><span>PHP Programmer (Middle)</span></td>
                                                <td><span>€</span>0.00</td>
                                                <td><span class="multiply">x</span><span class="month-td">24 month</span><span class="little-arr"></span><form method="#" action="#">
                                                        <input type="text" placeholder="24" class="inp-tp" name="page" >
                                                    </form><span class="arrow"></span><span class="equal">=</span></td>
                                                <td><span>€</span>0.00</td>
                                            </tr>
                                            <tr>
                                                <td class="spec-td"><span>JAVA Programmer (Senior)</span></td>
                                                <td><span>€</span>0.00</td>
                                                <td><span class="multiply">x</span><span class="month-td">24 month</span><span class="little-arr"></span><form method="#" action="#">
                                                        <input type="text" placeholder="24" class="inp-tp" name="page" >
                                                    </form><span class="arrow"></span><span class="equal">=</span></td>
                                                <td><span>€</span>0.00</td>
                                            </tr>
                                            <tr>
                                                <td class="spec-td"><span>PHP Programmer (Middle)</span></td>
                                                <td><span>€</span>0.00</td>
                                                <td><span class="multiply">x</span><span class="month-td">24 month</span><span class="little-arr"></span><form method="#" action="#">
                                                        <input type="text" placeholder="24" class="inp-tp" name="page" >
                                                    </form><span class="arrow"></span><span class="equal">=</span></td>
                                                <td><span>€</span>0.00</td>
                                            </tr>
                                        </table>
                                        <div class="offer-staff-res-all">
                                            <div class="offer-staff-res-list">
                                                <ul>
                                                    <li>Vat:</li>
                                                    <li>Sum total:</li>
                                                </ul>
                                                <ul>
                                                    <li><span>€</span>0.00</li>
                                                    <li><span>€</span>0.00</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="invoice-line-btn"><span>Insert</span></button>
                                    <div class="step2-insert-menu">
                                        <a href="#" class="staff-btn">
                                            <span>Staff</span>
                                        </a>

                                        <div class="close-img">
                                            <div class="close-icon"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="offer-step2-preres-block">
                                <div class="preres-block-bg ">
                                    <div class="preres-block-top">
                                        <ul>
                                            <li>New amount</li>
                                            <li>Vat 19%</li>
                                        </ul>
                                        <ul>
                                            <li><span>€</span>0.00</li>
                                            <li><span>€</span>0.00</li>
                                        </ul>
                                    </div>
                                    <div class="preres-block-bottom">
                                        <ul>
                                            <li>Offer amount:</li>
                                            <li>Purchase price</li>
                                            <li>Margin</li>
                                        </ul>
                                        <ul>
                                            <li><span>€</span>0.00</li>
                                            <li><span>€</span>0.00</li>
                                            <li><span>€</span>0.00</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="offer-step2-res-block">
                                <div class="offer-step2-res">
                                    <div class="step2-lists">
                                        <ul>
                                            <li>Invoice amount:</li>
                                            <li>Pitchase price</li>
                                            <li>Margin</li>
                                        </ul>
                                        <ul>
                                            <li><span>€</span> 0.00</li>
                                            <li><span>€</span> 0.00</li>
                                            <li><span>€</span> 0.00</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="offer-step2-comment-block">
                                <textarea name="" placeholder="Comment"></textarea>
                            </div>
                        </div>
                        <div class="offer-step3">
                            <div class="offer-step3_wrapper">
                                <span class="title-step1">Step 3: Save your<span> document</span>. </span><a href="#"> Print
                                    preview</a>

                                <div class="offer-form-all-block">
                                    <span>Send via: </span>

                                    <form class="offer-form" name="test" method="post" action="#">
                                        <div class="radioboxes">
                                            <input type="radio" id="radio-1" name="#" checked>
                                            <label for="radio-1">Mail service</label>
                                            <input type="radio" id="radio-2" name="#">
                                            <label for="radio-2">Personal mail</label>
                                        </div>
                                    </form>
                                </div>
                                <div class="buttons-group">
                                    <div class="button-left-block">
                                        <a href="#"><span>Complete and sent to customer</span></a>
                                        <span>The document is savedand<br> assigned a number.</span>
                                    </div>
                                    <div class="button-right-block">
                                        <a href="#"><span>Erasure</span></a>
                                        <span>The draft will be<br>deleted.</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- CONTENT END -->
        <!-- RIGHTNBAR -->
        {include file='../../main/right_side.tpl'}
        <!-- RIGHTBAR END -->
        <!-- FOOTER -->
        <footer>
            <div class="ft-wrapper">
                <div class="ft-links">
                    <ul>
                        <li><a href="#">Imprint</a></li>
                        <li><a href="#">Conditions</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Terms and Conditions</a></li>
                        <li><a href="#">Confidentiality Agreement</a></li>
                    </ul>
                </div>
                <div class="ft-contacts">
                    <div class="ft-phone">
                        <span class="ft-m">telephone:</span><span class="ft-phone-number">+1-541-754-3010</span>
                    </div>
                    <div class="ft-social">
                        <span class="ft-m">We are in social:</span>
                        <a href="#" class="ft-link-fb">&nbsp;</a>
                        <a href="#" class="ft-link-gl">&nbsp;</a>
                    </div>
                </div>
            </div>
        </footer>

    </body>

</html>
