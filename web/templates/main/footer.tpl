<footer>
    <div class="ft-wrapper">
        <div class="ft-links">
            <ul>
                <li><a href="#">Imprint</a></li>
                <li><a href="#">Conditions</a></li>
                <li><a href="#">Privacy Policy</a></li>
                <li><a href="#">Terms and Conditions</a></li>
                <li><a href="#">Confidentiality Agreement</a></li>
            </ul>
        </div>
        <div class="ft-contacts">
            <div class="ft-phone">
                <span class="ft-m">telephone:</span><span class="ft-phone-number">+1-541-754-3010</span>
            </div>
            <div class="ft-social">
                <span class="ft-m">We are in social:</span>
                <a href="#" class="ft-link-fb">&nbsp;</a>
                <a href="#" class="ft-link-gl">&nbsp;</a>
            </div>
        </div>
    </div>
</footer>