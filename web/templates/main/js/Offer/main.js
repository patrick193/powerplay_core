/* COUNTER FOR OFFER PAGES*/
function counterForOfferPage() {

    var counter = 0;
    $('.duration-up').on('click', function () {
        if (counter >= 0 && counter < 30) {
            counter++;
            $('.duration-count input').prop('value', counter);
        } else {
            $('.duration-count input').prop('value', 30);
        }
    });

    $('.duration-down').on('click', function () {
        if (counter > 0 && counter <= 30) {
            counter--;
            $('.duration-count input').prop('value', counter);
        } else {
            $('.duration-count input').prop('value', '0');
        }
    });

    $('.duration-count input').on('focusout keyup', (function () {
        if ($('.duration-count input').val() > 30) {
            $('.duration-count input').prop('value', 30);
        } else if ($('.duration-count input').val() < 0) {
            $('.duration-count input').prop('value', 0);
        }
        else if ($('.duration-count input').val().toString()) {
            $('.duration-count input').prop('value', 0);
        }

    }));
}


$(document).ready(function () {

    $('select').selecter();

    window.onload = function () {
        window.scroll = baron({
            root: '.wrapper',
            scroller: '.scroller',
            bar: '.scroller__bar',
            barOnCls: 'baron'
        });

    };




    // add only scroll





    $('input').iCheck({
        checkboxClass: 'checkbox-plg',
        radioClass: 'iradio-plg',
        increaseArea: '20%'
    });



    counterForOfferPage();



    $('.little-arr').on('click', function () {
        if ($(this).next().find('.inp-tp').is(':hidden')) {
            $(this).next().find('.inp-tp').show(200);
        } else {
            $(this).next().find('.inp-tp').hide(200);
        }

    });







    $('.accept-button, .accept-customer-btn').on('click', function () {
        $('.offer-popup-all').fadeOut(300);
        $('.step2-insert-menu').hide();

        $('.step1-info-user').show();

    });

    $('.accept-button').on('click', function () {
        $('.offer-new-fields').show();
        $('.offer-step2-preres-block').show();
        $('.select-invoice-line-block > span').hide();
    });



    $('.select-staff-form a').on('click', function () {
        return false;
    });



    $('.img-close-bl').on('click', function () {
        $('.offer-popup-all').fadeOut(300);
        $('#popup-form').fadeOut(300);
        $('#popup-form').animate({
            top: '-789px'
        });
        return false;
    });



//    $('.select-customer-block').on('click', function () {
//        $('.staff-popup-block').hide();
//        $('.popup-form').hide();
//        $('#popup-form').animate({
//            top: '-789px'
//        });
//        $('.step2-insert-menu').hide();
//        $('.offer-popup-all,.select-customer-popup-block').fadeIn(300);
//        if ($('.customer-popup-form ').height() <= 180) {
//            $('.select-customer-popup-block .scroll-bsn').css("display", "none")
//        } else {
//            $('.select-customer-popup-block .scroll-bsn').css("display", "block")
//        }
//    });




    /*TODO:
     * INSERT BUTTON
     * */

    /*Click on INSERT BUTTON*/
    $('.invoice-line-btn').on('click', function () {
        $('.step2-insert-menu').fadeIn(300);
    });

    /*Click on INSERT BUTTON and close button*/
    $('.step2-insert-menu .close-img').on('click', function () {
        $('.step2-insert-menu').fadeOut();
    });







    $('.new-customer-btn').on('click', function () {
        $('.select-customer-popup-block,.staff-popup-block').hide();
        $('#popup-form').animate({
            top: '82px'
        }).show();
    });


    /*Click grey popup block*/
    $('.offer-popup-grey-bg').on('click', function () {
        $('.step2-insert-menu').hide();
        $('.offer-popup-all').fadeOut(300);
    });

    /*TODO:
     * STAFF POPUP
     * */
    if ($('.staff-popup-block .staff-popup ').height() <= 475) {
        $('.staff-popup-block .scroll-bsn').css("display", "none")
    } else {
        $('.staff-popup-block .scroll-bsn').css("display", "block")
    }

    $('.staff-popup-top .add-abilities').on('click', function () {
        $('.staff-popup-top .selecter.closed').first().clone().insertAfter($(".staff-title")).selecter();

        if ($('.staff-popup-block .staff-popup ').height() <= 475) {
            $('.staff-popup-block .scroll-bsn').css("display", "none")
        } else {
            $('.staff-popup-block .scroll-bsn').css("display", "block")
        }

    });

    $('.staff-btn').on('click', function () {
        $('.select-customer-popup-block,.popup-form').hide();
        $('#popup-form').animate({
            top: '-789px'
        });
        $('.step2-insert-menu').hide();
        $('.offer-popup-all,.staff-popup-block').fadeIn(300);

        return false;
    });

    /*Active for staff popup table (for radio buttons)*/
    $('.select-staff-form label').removeClass('active-label').first().addClass('active-label');

    $('.select-staff-form label').on('click', function () {
        $(this).siblings().removeClass('active-label');
        $(this).addClass('active-label');
    });

    /*Close staff popup when we click on close button*/
    $('.staff-poput-close').on('click', function () {
        $('.offer-popup-all').fadeOut(300);

        $('.step2-insert-menu').hide();
    });





});
