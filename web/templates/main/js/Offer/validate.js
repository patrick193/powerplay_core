$(document).ready(function () {

    $('input').iCheck({
        checkboxClass: 'checkbox-plg',
        radioClass: 'iradio-plg',
        increaseArea: '20%'
    });


    $('.checkbox-plg').removeClass('checked');
    $(".input-phone").mask("+99 (999) 999-999-99");

    $('.input-firstname').focusout(function () {
        var valid_field = $(this).val(),
            valid_field_length = valid_field.length;

        if ((valid_field === '') || (valid_field_length < 2)) {
            $(this).closest('.firstname').find('.valid-field').removeClass('valid-ok').addClass('valid-error');
            $(this).closest('.firstname').find('.valid-error-field').fadeIn(300);

        } else {
            $(this).closest('.firstname').find('.valid-field').removeClass('valid-error').addClass('valid-ok');
            $(this).closest('.firstname').find('.valid-error-field').hide();
        }

    });


    $('.input-lastname').focusout(function () {
        var valid_field = $(this).val(),
            valid_field_length = valid_field.length;

        if ((valid_field === '') || (valid_field_length < 2)) {
            $(this).closest('.lastname').find('.valid-field').removeClass('valid-ok').addClass('valid-error');
            $(this).closest('.lastname').find('.valid-error-field').fadeIn(300);
        } else {
            $(this).closest('.lastname').find('.valid-field').removeClass('valid-error').addClass('valid-ok');
            $(this).closest('.lastname').find('.valid-error-field').hide();
        }
    });


    $('.input-company').focusout(function () {
        var valid_field = $(this).val();
        var valid_field_length = valid_field.length;

        if ((valid_field === '') || (valid_field_length < 2)) {
            $(this).closest('.company').find('.valid-field').removeClass('valid-ok').addClass('valid-error');
            $(this).closest('.company').find('.valid-error-field').fadeIn(300);
        } else {
            $(this).closest('.company').find('.valid-field').removeClass('valid-error').addClass('valid-ok');
            $(this).closest('.company').find('.valid-error-field').hide();
        }
    });

    $('.input-street').focusout(function () {
        var valid_field = $(this).val(),
            valid_field_length = valid_field.length;

        if ((valid_field === '') || (valid_field_length < 4)) {
            $(this).closest('.street').find('.valid-field').removeClass('valid-ok').addClass('valid-error');
            $(this).closest('.street').find('.valid-error-field').fadeIn(300);
        } else {
            $(this).closest('.street').find('.valid-field').removeClass('valid-error').addClass('valid-ok');
            $(this).closest('.street').find('.valid-error-field').hide();
        }
    });


    $('.input-postalcod').focusout(function () {

        var valid_field = $(this).val(),
            regex_field = /[^[0-9]/,
            regex_valid_field = regex_field.test(valid_field),
            valid_field_length = valid_field.length;

        if ((valid_field === '') || (valid_field_length < 3) || (regex_valid_field === true)) {
            $(this).closest('.postalcode ').find('.valid-field').removeClass('valid-ok').addClass('valid-error');
            $(this).closest('.postalcode').find('.valid-error-field').fadeIn(300);
        } else {
            $(this).closest('.postalcode ').find('.valid-field').removeClass('valid-error').addClass('valid-ok');
            $(this).closest('.postalcode').find('.valid-error-field').hide();
        }
    });


    $(".input-phone").mask("+99 (999) 999-999-99");


    $(".reg-form .btn-continue").on('click', function (e) {

        var valid_firstname_field = $('.input-firstname').val(),
            valid_firstname_field_length = valid_firstname_field.length,

            valid_lastname_field = $('.input-lastname').val(),
            valid_lastname_field_length = valid_lastname_field.length,

            valid_company_field = $('.input-company').val(),
            valid_company_field_length = valid_company_field.length,

            valid_street_field = $('.input-street').val(),
            valid_street_field_length = valid_street_field.length,

            valid_postalcode_field = $('.input-postalcod').val(),
            regex_postalcode_field = /[^[0-9]/,
            regex_valid_postalcode_field = regex_postalcode_field.test(valid_postalcode_field),
            valid_postalcode_field_length = valid_postalcode_field.length;


        var reg_if = (valid_firstname_field === '') || (valid_firstname_field_length < 2)
            || (valid_lastname_field === '') || (valid_lastname_field_length < 2)
            || (valid_company_field === '') || (valid_company_field_length < 2)
            || (valid_street_field === '') || (valid_street_field_length < 4)
            || (valid_postalcode_field === '') || (valid_postalcode_field_length < 3) || (regex_valid_postalcode_field === true);


        if ((valid_firstname_field === '') || (valid_firstname_field_length < 2)) {
            $('.input-firstname').closest('.firstname').find('.valid-field').removeClass('valid-ok').addClass('valid-error');
            $('.input-firstname').closest('.firstname').find('.valid-error-field').fadeIn(300);
        } else {
            $('.input-firstname').closest('.firstname').find('.valid-field').removeClass('valid-error').addClass('valid-ok');
            $('.input-firstname').closest('.firstname').find('.valid-error-field').hide();
        }

        if ((valid_lastname_field === '') || (valid_lastname_field_length < 2)) {
            $('.input-lastname').closest('.lastname').find('.valid-field').removeClass('valid-ok').addClass('valid-error');
            $('.input-lastname').closest('.lastname').find('.valid-error-field').fadeIn(300);
        } else {
            $('.input-lastname').closest('.lastname').find('.valid-field').removeClass('valid-error').addClass('valid-ok');
            $('.input-lastname').closest('.lastname').find('.valid-error-field').hide();
        }

        if ((valid_company_field === '') || (valid_company_field_length < 2)) {
            $('.input-company').closest('.company').find('.valid-field').removeClass('valid-ok').addClass('valid-error');
            $('.input-company').closest('.company').find('.valid-error-field').fadeIn(300);
        } else {
            $('.input-company').closest('.company').find('.valid-field').removeClass('valid-error').addClass('valid-ok');
            $('.input-company').closest('.company').find('.valid-error-field').hide();
        }

        if ((valid_street_field === '') || (valid_street_field_length < 4)) {
            $('.input-street').closest('.street').find('.valid-field').removeClass('valid-ok').addClass('valid-error');
            $('.input-street').closest('.street').find('.valid-error-field').fadeIn(300);
        } else {
            $('.input-street').closest('.street').find('.valid-field').removeClass('valid-error').addClass('valid-ok');
            $('.input-street').closest('.street').find('.valid-error-field').hide();
        }

        if ((valid_postalcode_field === '') || (valid_postalcode_field_length < 3) || (regex_valid_postalcode_field === true)) {
            $('.input-postalcod').closest('.postalcode ').find('.valid-field').removeClass('valid-ok').addClass('valid-error');
            $('.input-postalcod').closest('.postalcode').find('.valid-error-field').fadeIn(300);
        } else {
            $('.input-postalcod').closest('.postalcode ').find('.valid-field').removeClass('valid-error').addClass('valid-ok');
            $('.input-postalcod').closest('.postalcode').find('.valid-error-field').hide();
        }

        if(!$('.checkbox-plg').hasClass('checked')){
            $('.checkbox-plg').closest('.check').find('.valid-error-field').fadeIn(300);
        }else{
            $('.checkbox-plg').closest('.check').find('.valid-error-field').hide();
        }




        if(reg_if || !$('.checkbox-plg').hasClass('checked')) {
            return true;
        } else {
            $('.offer-popup-all').fadeOut();
        }
    });


});

