  $(document).ready(function() {
      $("div.scroller").scrollTop($('div.body-s:last').offset().top);

      $('#sms-popup-container').hide();
      $('#btn-answer').click(function() {
          $('#sms-popup-container').show(250);

      });
      $('div.sms-close-img').click(function() {
          $('div#sms-popup-container').hide(250);
      })

      $(function() {
          $('.scroller').scroll(function() {
              if ($(this).scrollTop() != 0) {
                  $('.bg-opac-m').show(300);
              } else {
                  $('.bg-opac-m').hide(300);
              }
          });
      });
      $(".btn-answer").hover(function() {
          $(".shadow-btn").show(10);
      });
      $(".btn-answer").mouseleave(function() {
          $(".shadow-btn").hide(10);
      });
      if ($('.messages-body').height() < 420) {
          $('.bg-opac-m').css("display", "none")
          $('.scroll-smsb').css("display", "none")
      }
  });
