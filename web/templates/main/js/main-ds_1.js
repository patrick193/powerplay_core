(function ($) {
    $.fn.hasScrollBar = function () {
        return this.get(0).scrollHeight > this.height();
    }
})(jQuery);

var hasScroll = $('html').hasScrollBar();


var leftSideBar = {
    slideToggle: function () {
        $('#main-menu li').hover(function () {
            $(this).children('ul').stop(true).slideDown(300);
        }, function () {
            $(this).children('ul').stop(true).slideUp(1);
        });

        $('.menu-drop li').hover(function () {
            $(this).children('ul').stop(true).slideDown(500);
        }, function () {
            $(this).children('ul').stop(true).slideUp(500);
        });
    },


    changeBackgroundAnimation: function () {
        /*FUNCTION FOR LEFTBAR BLACK BACKGROUND*/
        $('#main-menu li.menu-list').hover(function () {
            $(this).find('.text-menu').stop().animate({
                opacity: "1"
            }, 0);
            $('#menu-bg').stop().animate({
                opacity: "1"
            }, 10);
            $(this).siblings().find('.text-menu').stop().animate({
                opacity: "0"
            }, 0);
        }, function () {
            $('#main-menu li.menu-list').find('.text-menu').stop().animate({
                opacity: "1"
            }, 0);
            $('#menu-bg').stop().animate({
                opacity: "0"
            }, 10);
        });
    }
};

var rightSideBar = {
    preProfile: function () {

        $(".pre-profile").click(function () {

            if (hasScroll == true) {
                $('footer').css({
                    position: 'relative',
                    left: 0,
                    top: '30px',
                    transform: 'translateX(0%)',
                    webkitTransform: 'translateX(0%)'
                });
            } else if (hasScroll == false) {
                $('footer').css({
                    position: 'fixed',
                    left: '50%',
                    bottom: 0,
                    top: ' ',
                    transform: 'translateX(-50%)',
                    webkitTransform: 'translateX(-50%)'
                });
            } else if ((hasScroll == true) && ($(".rightbar-panel").css('display') === 'block')) {
                $('footer').css({
                    position: 'fixed',
                    left: '50%',
                    top: ' ',
                    bottom: 0,
                    transform: 'translateX(-50%)',
                    webkitTransform: 'translateX(-50%)'
                });
            }

            if ($(window).width() <= 1660) {

                if ($(".rightbar-panel").hasClass('hide-class')) {
                    $(".rightbar-panel").removeClass('hide-class').addClass('active-class').show(0).stop().animate({
                        right: "0"
                    }, 400);

                } else {
                    $(".rightbar-panel").removeClass('active-class').addClass('hide-class').stop().animate({
                        right: "-330px"
                    }, 400).hide(0);
                }
            }
        });
    },

    usersTree: function () {
        $(".us2").click(function () {
            $(".customer-block-show").hide(350);
            if ($(this).children(".customer-block-show").is(':hidden')) {
                $(this).children(".customer-block-show").show(350);
            }
        });
        $(".us1").click(function () {
            if ($(".sales-block-show").is(':hidden')) {
                $(this).parent().children(".sales-block-show").show(350);
            } else {
                $(".sales-block-show").hide(350);
            }
        });
    }
};

var headerScripts = {
    bannerOpenClose: function () {
        /*BANNERS open/ close */
        $('#modal-sms, #sms-popup').hide();
        $('body').on("click", "#show-sms, #modal-sms", function (ob) {
            ob.stopPropagation();
            $('#modal-sms,  #sms-popup').fadeIn(300).addClass('activePopup');
        });
        $('html').click(function (ob) {
            ob.stopPropagation();
            $('#modal-sms, #sms-popup').fadeOut(300).removeClass('activePopup');
        });
        $('a.close').click(function (ob) {
            ob.stopPropagation();
            $('#modal-sms, #sms-popup').fadeOut(300).removeClass('activePopup');
        });
    }
};

var allScripts = {
    scrollTopAfterDownload: function () {

        /*TODO:
         * scroll top for all browsers when page will be download
         */

        window.scrollTo(0, 0);
        jQuery("body").animate({
            scrollTop: 0
        }, "1");
    },

    effectsHeader_LeftSidebar: function () {
        $('.leftbar, .header-l').on({
            mouseenter: function () {
                if ($(window).width() <= 1660) {
                    $(".header-l").stop().animate({
                        width: "330px"
                    });
                    $("#leftbar").stop().animate({
                        width: "330px"
                    });
                    $(".text-menu").css({
                        left: "70px"
                    });
                    $(".ft-logo-ds").css("display", "block");
                    $(".ft-logo-lp").css("display", "none");
                    $(".copywrite").stop().animate({
                        left: "137px"
                    });
                    $(".title-text-l").css("display", "inline-block");
                    $(".logo-big").css("display", "inline-block");
                    $(".logo-mini").css("display", "none");
                }
            },
            mouseleave: function () {
                if ($(window).width() <= 1660) {
                    $(".header-l").stop().animate({
                        width: "70px"
                    });
                    $(".leftbar").stop().animate({
                        width: "70px"
                    });
                    $(".text-menu").stop().animate({
                        left: "-150px"
                    });
                    $(".title-text-l").css("display", "none");
                    $(".logo-big").css("display", "none");
                    $(".logo-mini").css("display", "block");
                    $(".ft-logo-ds").css("display", "none");
                    $(".ft-logo-lp").css("display", "block");
                    $(".copywrite").stop().animate({
                        left: "11px"
                    });
                }
            }

        });
    }

};

$(function () {

    $(".tabs").lightTabs();

    allScripts.scrollTopAfterDownload();
    allScripts.effectsHeader_LeftSidebar();

    rightSideBar.preProfile();
    rightSideBar.usersTree();

    leftSideBar.changeBackgroundAnimation();
    leftSideBar.slideToggle();

    headerScripts.bannerOpenClose();
    var leftbar_height = $('.leftbar').height();

    if (leftbar_height > $('#container').height()) {
        $('#container').height(leftbar_height - 185);
    }
});


;


(function () {

    /*TODO:
     * RESIZE FOR RIGHTBAR SCROLL
     */

    /* Функция для изменения правого сайдбара для больших и средних экранов*/
    var changeParamForRightSideBar = function (e) { /* resize and scroll logic */

        if (hasScroll == true) {
            $('footer').css({
                position: 'relative',
                left: 0,
                top: '30px',
                transform: 'translateX(0%)',
                webkitTransform: 'translateX(0%)'
            });
        } else if (hasScroll == false) {
            $('footer').css({
                position: 'fixed',
                left: '50%',
                bottom: 0,
                top: ' ',
                transform: 'translateX(-50%)',
                webkitTransform: 'translateX(-50%)'
            });
        } else if ((hasScroll == true) && ($(".rightbar-panel").css('display') === 'block')) {
            $('footer').css({
                position: 'fixed',
                left: '50%',
                top: ' ',
                bottom: 0,
                transform: 'translateX(-50%)',
                webkitTransform: 'translateX(-50%)'
            });
        }

        var getBottomPosition = $(window).scrollTop() + $(window).height();
        /* Маленький экран*/
        if ($(window).height() < 905) {

            if (getBottomPosition > 957) {

                if ($(".content").height() < $(".rightbar-panel").height()) {
                    $(".content").height($(".rightbar-panel").height());
                }

                $('.rightbar-panel').css({
                    position: 'fixed',
                    minHeight: '870px',
                    bottom: '0'
                });

            } else {
                if ($(".content").height() < $(".rightbar-panel").height()) {
                    $(".content").height($(".rightbar-panel").height());
                }
                $('.rightbar-panel').css({
                    position: 'relative'
                })
            }

        } else if ($(window).height() >= 905) {
            /*Большой экран*/
            $(".content").css('height', 'auto');

            $('.rightbar-panel').css({
                position: 'fixed',
                minHeight: '900px',
                bottom: ''
            });

            //var leftbar_height = $('.rightbar-panel').height();


        }
    };
    $(window).scroll(changeParamForRightSideBar).resize(changeParamForRightSideBar);


    /*TODO:
     * RESIZE ALL BLOCKS
     * */
    $(window).resize(function () {
        if ($(window).width() < 1660) {
            $(".rightbar-panel ").removeClass('active-class').addClass('hide-class').stop().animate({
                right: "-330px"
            }, 400).hide(0);
            $(".header-l").stop().animate({
                width: "70px"
            });
            $(".leftbar").stop().animate({
                width: "70px"
            });
            $(".text-menu").stop().animate({
                left: "-150px"
            });
            $(".title-text-l").css("display", "none");
            $(".logo-big").css("display", "none");
            $(".logo-mini").css("display", "block");
            $(".ft-logo-ds").css("display", "none");
            $(".ft-logo-lp").css("display", "block");
            $(".copywrite").stop().animate({
                left: "11px"
            });
        } else {

            $(".rightbar-panel").removeClass('active-class').addClass('hide-class').show(0).stop().animate({
                right: "0"
            }, 400);
            $(".header-l").stop().animate({
                width: "330px"
            });
            $("#leftbar").stop().animate({
                width: "330px"
            });
            $(".text-menu").css({
                left: "70px"
            });
            $(".ft-logo-ds").css("display", "block");
            $(".ft-logo-lp").css("display", "none");
            $(".copywrite").stop().animate({
                left: "137px"
            });
            $(".title-text-l").css("display", "inline-block");
            $(".logo-big").css("display", "inline-block");
            $(".logo-mini").css("display", "none");
        }
    });


    /*TODO:
     * PLUGIN FOR TABS
     * */

    jQuery.fn.lightTabs = function (options) {

        var createTabs = function () {
            tabs = this;
            i = 0;

            showPage = function (i) {
                $(tabs).children("div").children("div").hide();
                $(tabs).children("div").children("div").eq(i).show();
                $(tabs).children(".tabs-right").children("li").removeClass("active");
                $(tabs).children(".tabs-right").children("li").eq(i).addClass("active");
            }

            showPage(1);

            $(tabs).children(".tabs-right").children("li").each(function (index, element) {
                $(element).attr("data-page", i);
                i++;
            });

            $(tabs).children(".tabs-right").children("li").click(function () {
                showPage(parseInt($(this).attr("data-page")));
            });
        };
        return this.each(createTabs);
    };
    window.onload = function () {
        window.scroll = baron({
            root: '.wrapper',
            scroller: '.scroller',
            bar: '.scroller__bar',
            barOnCls: 'baron'
        })

    };
    if ($('.sms-body p').height() < 110) {
        $('.scroll-bsn').css("display", "none");
    } else {
        $('.scroll-bsn').css("display", "block");
    }

}());
