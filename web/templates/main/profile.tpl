<div class="popup-form" id="popup-form">
    <a href="#" class="img-close-bl">&nbsp;</a>
    <span class="title-popup-form" data-role-name="">Add </span>
    <form  method="post" class="reg-form-b">
        <input name="role_id" type="hidden" class="role_id" value="">
        <div class="block-name">
            <div class="block-title-name">NAME</div>
            <div class="firstname">
                <span class="name-input">Firstname:</span>
                <input type="text" class="input-firstname input-main" name="name">
            </div>
            <div class="lastname">
                <span class="name-input">Lastname:</span>
                <input type="text" class="input-lastname input-main" name="lastname">
            </div>
        </div>
        <div class="block-company">
            <div class="company">
                <span class="name-input">Email:</span>
                <input type="text" class="input-company input-main" name="email">
            </div>
        </div>
        <div class="block-company">
            <div class="company">
                <span class="name-input">Company:</span>
                <input type="text" class="input-company input-main" name="company_name">
            </div>
        </div>
        <div class="block-address">
            <div class="block-title-name">ADDRESS</div>
            <div class="street">
                <span class="name-input">Street and House Number:</span>
                <input type="text" class="input-street input-main" name="street">
            </div>
            <div class="postalcode-city">
                <div class="postalcode">
                    <span class="name-input">Postal Code:</span>
                    <input type="text" class="input-postalcod input-main" name="postal_code">
                </div>
                <div class="city">
                    <span class="name-input">City:</span>
                    <input type="text" class="input-city input-main" name="city">
                </div>
            </div>
            <div class="region">
                <span class="name-input">Region:</span>
                <input type="text" class="input-region input-main" name="region">
            </div>
            <div class="country">
                <span class="name-input">Country:</span>
                <input type="text" class="input-country input-main" name="country">
            </div>
        </div>
        <div class="block-phone">
            <div class="phone">
                <span class="name-input">Cell phone number:</span>
                <input type="text" class="input-phone input-main" name="cell_phone">
            </div>
        </div>
        <span class="format-phone">in format +00 (000) 633-513-65</span>
        <div class="check">
            <div class="btn-form">
                <a class="btn-continue" data-edit=''><span>ADD</span></a>
            </div>
        </div>
    </form>
</div>