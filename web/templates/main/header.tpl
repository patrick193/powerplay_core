{assign var="user" value=$smarty.session.register.user_auth|base64_decode|unserialize}
<header>
    <div class="header-l">
        <div class="logo-img">
            <a href="/"><img src="../../../web/images/logo.png" class="logo-big" alt="logo">
                <img src="../../../web/images/logo-mini.png" class="logo-mini" alt="">
            </a>
        </div>
        <div class="title-text-l"><span>CUSTOMER</span></div>
    </div>
    <div class="header-c">
        <div class="header-text">
            <span class="title-text">{$module.name}</span>
            <p>
                {foreach from=$module.breadscrumb key=key item=value}
                    {if $value == $module.breadscrumb|@end }
                        <span class="finish-text">{$key}</span>
                    {else}
                        <a href="{$value}">{$key}</a><span class="arrow-step">&nbsp;</span>
                    {/if}
                {/foreach}
            </p>
        </div>
        <div class="messages">
            <div class="notification">
                <a href="#" id="show-sms"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAPCAMAAADXs89aAAAAjVBMVEX///9Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd55O6nhAAAALnRSTlMAAQIGCwwODxUWFyAhIiQvMDNGW1xgYXKRmJmaqaq4ubzLzs/a29zd5Ofr8fP5hiMMmAAAAKJJREFUGNNt0EcOgzAQBdChOPSaACY00zEG7n+84CBkhPgLL55G1syH7TGwbZl1S8Y5YliGS2TMop0tjxaKUKWgnsUZ7I5op2qks+Fg0OvWPNRsax1OBrUcXa7uWKogGNA0BwDBPCG4MKqG7xKGSzpUSLBeNwYk65qA0Yi/nZ689pnPe39epHcO9ml+3TunPueYYel6pYRZzDtJ752k/6qe8gOEXhjFxkAh5wAAAABJRU5ErkJggg=="><span class="quantity">2</span>
                    <div class="mess-text">
                        <span>messages</span></div>
                </a>
                <div id="modal-sms">
                    <div class="container-modal-sms">
                        <div class="bg-modal-sms">&nbsp;</div>
                        <div class="top-sms-block">
                            <div class="logo-sms-img">
                                &nbsp;
                            </div>
                            <div class="sms-info-block">
                                <span class="from-block">from VIRTURO</span>
                                <span class="time-sms-block">07.08.2015 at 17:45</span>
                            </div>
                        </div>
                        <div class="line-sms">&nbsp;</div>
                        <div class="sms-body">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam quis nostrud exercitation ullamco. laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
                            </p>
                        </div>
                        <div class="sms-nav">
                            <div class="nav-prev-sms-block">
                                <a href="#" class="prev-sms">earlier</a>
                            </div>
                            <div class="close-sms-block">
                                <a href="#close" title="Закрыть" class="close">close</a>
                            </div>
                            <div class="nav-next-sms-block">
                                <a href="#" class="next-sms">next</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-r">
        <div class="pre-profile">
            <div class="img-user"><img src="../../../web/images/users/{$user->getPhoto()}" alt=""></div>
            <div class="info-user">
                <p><strong class="name-user">{$user->getUserFirstName()} {$user->getUserLastName()}</strong> <span class="id-user">({$user->getUserId()})</span></p>
                <span class="user-type">{$user->getCompanyName()}, {$user->getRole()->getRoleName()}</span>
            </div>
        </div>
        <div class="line-prepr">&nbsp;</div>
        <div class="logout"><a href="/">&nbsp;</a></div>
    </div>
</header>