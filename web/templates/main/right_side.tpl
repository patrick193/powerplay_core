{assign var="user" value=$smarty.session.register.user_auth|base64_decode|unserialize}
<div class="rightbar">
    <div class="rightbar-panel show-rp">
        <div class="tabs">
            <ul class="tabs-right">
                <li>USERS</li>
                <li>PROFILE</li>
            </ul>
            <div class="tabs-content">
                <div class="tab-users">
                    <div class="views-users-block">
                        <div class="businessman-block">
                            <div class="yourself-block">
                                <div class="us1 us3">
                                    <div class="img-user-block">
                                        <img src="/web/templates/UsManag_2/BisList/img/users/yourself-photo.png" alt="you">
                                    </div>
                                    <div class="info-user-block">
                                        <a href="#" class="name-user-link">{$user->getUserFirstName()} {$user->getUserLastName()}</a>
                                        <span class="info-user">{$user->getRole()->getRoleName()}</span>
                                    </div>
                                </div>
                                <div class="sales-block-show">
                                    {assign var='code' value=$user->getRole()->getRoleCode()}
                                    {if $code == '2B'} {assign var='who' value='2S'} {else} {assign var='who' value='2C'}{/if}
                                    {if $users != 0}
                                        {foreach from = $users item = 'sales'}
                                            {if $sales->getRole()->getRoleCode() == $who}
                                                <div class="sales-block us2">
                                                    <div class="line-sl">&nbsp;</div>
                                                    <div class="img-user-block">
                                                        <img src="/web/templates/UsManag_2/BisList/img/users/user-s1.png" alt="">
                                                    </div>
                                                    <div class="info-user-block">
                                                        <a href="#" class="name-user-link">{$sales->getUserFirstName()} {$sales->getUserLastName()}</a><span class="id-user-block">({$sales->getUserId()})</span>
                                                        <span class="info-user">{$sales->getCompanyName()}, {$sales->getRole()->getRoleName()}</span>
                                                    </div>
                                                    {assign var='children' value = $sales->getChildren()}
                                                    {if !$children|is_array}
                                                        <div class="customer-block-show">
                                                            This sales does not have customers yet
                                                        </div>
                                                    {else}
                                                        <div class="customer-block-show">
                                                            {foreach from = $children item='sCustomer'}
                                                                <div class="customer-block">
                                                                    <div class="img-user-block">
                                                                        <img src="/web/templates/UsManag_2/BisList/img/users/customer-photo.png" alt="">
                                                                    </div>
                                                                    <div class="info-user-block">
                                                                        <a href="#" class="name-user-link">{$sCustomer->getUserFirstName()} {$sCustomer->getUserLastName()}</a><span class="id-user-block">({$sCustomer->getUserId()})</span>
                                                                        <span class="info-user">{$sCustomer->getCompanyName()}, {$sCustomer->getRole()->getRoleName()}</span>
                                                                    </div>
                                                                </div>
                                                            {/foreach}
                                                        </div>
                                                    {/if}
                                                </div>
                                            {/if}
                                        {/foreach}
                                    {else}
                                        <div class="sales-block us2">
                                            <div class="line-sl">&nbsp;</div>
                                            <div class="img-user-block">
                                                <img src="/web/templates/UsManag_2/BisList/img/users/user-s1.png" alt="">
                                            </div>
                                            <div class="info-user-block">
                                                <a href="#" class="name-user-link">You do not have a sales yet</a>
                                            </div>
                                        </div>
                                    {/if}
                                </div>
                                <div class="line-bs-block">&nbsp;</div>
                            </div>
                            {if $users != 0}
                                {foreach from =$users item ='businessman'}
                                    {if {$businessman->getRole()->getRoleCode()} == '2B'}
                                        <div class="businessman-user">
                                            <div class="line-bs">&nbsp;</div>
                                            <div class="us1 us3">
                                                <div class="img-user-block">
                                                    <img src="/web/templates/UsManag_2/BisList/img/users/user-b1.png" alt="">
                                                </div>
                                                <div class="info-user-block">
                                                    <a href="#" class="name-user-link">{$businessman->getUserFirstName()} {$businessman->getUserLastName()}</a><span class="id-user-block">({$businessman->getUserId()})</span>
                                                    <span class="info-user">{$businessman->getCompanyName()}, {$businessman->getRole()->getRoleName()}</span>
                                                </div>
                                                <div class="sales-block-show">
                                                    {assign var='businessmanChildren' value=$businessman->getChildren()}
                                                    {if $businessmanChildren|is_array}

                                                        {foreach from = $businessmanChildren item = 'sales'}
                                                            {if $sales->getRole()->getRoleCode() == '2S'}
                                                                <div class="sales-block us2">
                                                                    <div class="line-sl">&nbsp;</div>
                                                                    <div class="img-user-block">
                                                                        <img src="/web/templates/UsManag_2/BisList/img/users/user-s1.png" alt="">
                                                                    </div>
                                                                    <div class="info-user-block">
                                                                        <a href="#" class="name-user-link">{$sales->getUserFirstName()} {$sales->getUserLastName()}</a><span class="id-user-block">({$sales->getUserId()})</span>
                                                                        <span class="info-user">{$sales->getCompanyName()}, {$sales->getRole()->getRoleName()}</span>
                                                                    </div>
                                                                    {assign var='children' value = $sales->getChildren()}
                                                                    {if !$children|is_array}
                                                                        <div class="customer-block-show">
                                                                            This sales does not have customers yet
                                                                        </div>
                                                                    {else}
                                                                        <div class="customer-block-show">
                                                                            {foreach from = $children item='sCustomer'}
                                                                                <div class="customer-block">
                                                                                    <div class="img-user-block">
                                                                                        <img src="img/users/customer-photo.png" alt="">
                                                                                    </div>
                                                                                    <div class="info-user-block">
                                                                                        <a href="#" class="name-user-link">{$sCustomer->getUserFirstName()} {$sCustomer->getUserLastName()}</a><span class="id-user-block">({$sCustomer->getUserId()})</span>
                                                                                        <span class="info-user">{$sCustomer->getCompanyName()}, {$sCustomer->getRole()->getRoleName()}</span>
                                                                                    </div>
                                                                                </div>
                                                                            {/foreach}
                                                                        </div>
                                                                    {/if}
                                                                </div>
                                                            {/if}
                                                        {/foreach}
                                                    {/if}
                                                </div>
                                            </div>
                                        </div>
                                    {/if}
                                {/foreach}
                            {else}
                                <div class="businessman-user">
                                    <div class="line-bs">&nbsp;</div>
                                    <div class="us1 us3">
                                        <div class="img-user-block">
                                            <img src="/web/templates/UsManag_2/BisList/img/users/user-b1.png" alt="">
                                        </div>
                                        <div class="info-user-block">
                                            <a href="#" class="name-user-link">You do not have any users in your list yet</a>
                                        </div>
                                    </div></div>
                                {/if}
                        </div>
                    </div>
                </div>
                <div class="tab-profile">
                    <div class="user-photo-block">
                        <div class="refresh-user-photo">
                            <form action="#" method="">
                                <div class="block-file">
                                    <input type="file" id="btn_file">
                                </div>
                            </form>
                        </div>
                        <div class="user-photo">
                            <img src="../../../web/images/users/{$user->getPhoto()}" alt="">
                        </div>
                    </div>
                    <div class="profile-info-block">
                        <div class="name-user-block">
                            <div class="user-name" id="user-name">
                                <span>{$user->getUserFirstName()} {$user->getUserLastName()}</span>
                            </div>
                            <div class="id-user" id="id-user">
                                <span>({$user->getUserId()})</span>
                            </div>
                            <div class="type-user" id="type-user">
                                <span>{$user->getCompanyName()}, {$user->getRole()->getRoleName()}</span>
                            </div>
                        </div>
                        <div class="mail-user-block">
                            <div class="mail-img">&nbsp;</div>
                            <div class="profile-line-bt">&nbsp;</div>
                            <div class="email-title">
                                <a href="mailto:{$user->getUserEmail()}">{$user->getUserEmail()}</a>
                            </div>
                        </div>
                        <div class="address-user-block">
                            <div class="address-img">&nbsp;</div>
                            <div class="profile-line-bt">&nbsp;</div>
                            <div class="address-user">
                                <div class="city-user">
                                    <span>{$user->getCountry()->name}</span>
                                </div>
                                <div class="street-user">
                                    <span>{$user->getCity(true)->name} 10117, {$user->getStreet()}</span>
                                </div>
                            </div>
                        </div>
                        <div class="info-user-block">
                            <div class="info-img">&nbsp;</div>
                            <div class="profile-line-bt">&nbsp;</div>
                            <div class="code-user">
                                <span>636656538863</span>
                            </div>
                        </div>
                        <div class="btn-profile">
                            <a href="/"><span>Change profile</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>