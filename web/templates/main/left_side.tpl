<div class="leftbar" id="leftbar">
    <div class="leftside">
        <div id="menu-bg">&nbsp;</div>
        <div class="main-menu" id="main-menu">
            <ul>
                <li class="usmanag-m menu-list">
                    <a href="/usermanagement">
                        <span class="icon-manu icon-mm">&nbsp;</span>
                        <span class="text-menu">User Management</span>
                    </a>
                </li>
                <li class="support-m menu-list">
                    <span class="icon-manu icon-mm">&nbsp;</span>
                    <span class="text-menu">Support</span>
                    <ul class="menu-drop">
                        <li class="menu-drop-list">
                            <a href="/support/faq/show">FAQ</a>
                        </li>
                        <li class="menu-drop-list"><a href="/support/ticket/all">Tickets</a>
                            <ul class="menu-dropdown">
                                <li><a href="/support/ticket/all">My Tickets</a></li>
                                <li><a href="/support/ticket/categories/">Create Ticket</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="orders-m menu-list">
                    <span class="icon-manu icon-mm">&nbsp;</span>
                    <span class="text-menu">Orders</span>
                    <ul class="menu-drop">
                        <li class="menu-drop-list">
                            <a href="#">New Offer</a>
                        </li>
                        <li class="menu-drop-list">
                            <a href="#">My Orders</a>
                        </li>
                        <li class="menu-drop-list">
                            <a href="#">My Invoice</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="copywrite">
            <div class="ft-logo-ds">
                <span class="copywr">2015  ©</span>
                <div class="ft-logo">
                    <a href="#"><img src="../../../../web/images/logo-LI.png" class="logo-ft" alt="logo">
                        <img src="../../../../web/images/logo-li-mini.png" class="logo-ft-mini" alt=""></a>
                </div>
            </div>
            <div class="ft-logo-lp">
                <div class="ft-logo-lp">
                    <img src="../../../../web/images/logo-li-mini.png" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
