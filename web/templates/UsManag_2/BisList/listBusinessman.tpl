<div class="blist">
    <div class="top-block-b">
        <div class="block-user">
            <div class="us-avat-bl">
                <img src="/web/templates/UsManag_2/BisList/img/ava-bl.png" alt="">
            </div>
            <div class="us-info-bl">
                <span class="top-sp-us"><strong>{$user->getUserFirstName()} {$user->getUserLastName()}</strong> ({$user->getUserId()})</span>
                <span class="bt-ps-us">{$user->getCompanyName()}, {$user->getRole()->getRoleName()}</span>
            </div>
        </div>
        <div class="line-left">&nbsp;</div>
        <div class="line-mid">&nbsp;</div>
        <div class="line-right">&nbsp;</div>
    </div>
    <div class='main-block-user'>
        <div class="block-businessmans">
            <div class="top-block-us">
                <span class="title-sp-us">BUSINESSMANs</span>
                <a href="#" class="show-add" data-role="4" data-role-name="Businessman">&nbsp;</a>
            </div>
            <div class="blocklist-b">

                {if $users != 0}
                    {foreach from = $users item='businessman'}
                        {if $businessman->getRole()->getRoleCode() == '2B'}
                            <div class="b-block">
                                <a href="#" class="show-info-b" id="show-info-b">
                                </a>
                                <div class="mod-window-sms" id="mod-window-sms">
                                    <div class="bl-left-i">&nbsp;</div>
                                    <div class="block-inf-popup">
                                        <span>Sorry. This information is confidential and available only Juna Dapas</span>
                                        <span class="sp-scnd">You can view information about your salesmans and customers.</span>
                                        <a href="#" class="btn-ok-close">
                                            <span>OK</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="b-avat-bl">
                                    <img src="/web/templates/UsManag_2/BisList/img/b1.png" alt="">
                                </div>
                                <div class="b-info-bl">
                                    <span class="top-sp-b"><strong>{$businessman->getUserFirstName()} {$businessman->getUserLastName()}</strong> ({$businessman->getUserId()})</span>
                                    <span class="bt-ps-b">{$businessman->getCompanyName()}</span>
                                </div>
                                <div class="block-btns-us">
                                    <div class="line-li-us-l">&nbsp;</div>
                                    <div class="line-li-us-r">&nbsp;</div>
                                    <a href="#" class="btn-change">Change</a>
                                    <a href="/usermanagement/block/{$businessman->getUserId()}" class="btn-block">Block user</a>
                                    <a href="/usermanagement/block/{$businessman->getUserId()}" class="btn-delete">Delete</a>
                                </div>
                            </div>
                        {/if}
                    {/foreach}
                {else}
                    <div class="b-block">
                        <a href="#" class="show-info-b" id="show-info-b">
                        </a>
                        <div class="b-avat-bl">
                            <img src="/web/templates/UsManag_2/BisList/img/b1.png" alt="">
                        </div>
                        <div class="b-info-bl">
                            <span class="top-sp-b"><strong>You do not have businessmens yet</strong> </span>
                        </div>
                        <div class="block-btns-us">
                            <div class="line-li-us-l">&nbsp;</div>
                            <div class="line-li-us-r">&nbsp;</div>
                        </div>
                    </div>
                {/if}
            </div>
        </div>

        <div class="block-sales">
            <div class="top-block-us">
                <span class="title-sp-us">SALEs</span>
                <a href="#" class="show-add" data-role="5" data-role-name="Sale">&nbsp;</a>
            </div>
            <div class="blocklist-b">
                {if $users != 0 }
                    {foreach from = $users key='i' item='sales'}
                        {if $sales->getRole()->getRoleCode() == '2S'}

                            <div class="b-block">
                                <a href="/usermanagement/auth/{$user->getUserId()}/{$sales->getUserId()}" class="show-info-b"></a>
                                <div class="b-avat-bl">
                                    <img src="/web/templates/UsManag_2/BisList/img/s5.png" alt="">
                                </div>
                                <div class="b-info-bl">
                                    <span class="top-sp-b"><strong>{$sales->getUserFirstName()} {$sales->getUserLastName()}</strong> ({$sales->getUserId()})</span>
                                    <span class="bt-ps-b">{$sales->getCompanyName()}</span>
                                </div>
                                <div class="block-btns-us">
                                    <div class="line-li-us-l">&nbsp;</div>
                                    <div class="line-li-us-r">&nbsp;</div>
                                    <a href="#" class="btn-change" data-change='2S' data-rolename='Sales'>Change</a>
                                    <a href="/usermanagement/block/{$sales->getUserId()}" class="btn-block">{if $sales->getIsLocked() == 0}Block user{else} Unblock user {/if}</a>
                                    <a href="/usermanagement/delete/" class="btn-delete" data-id='{$sales->getUserId()}'>Delete</a>
                                </div>
                            </div>
                        {/if}
                    {/foreach} 
                {else}
                    <div class="b-block">
                        <div class="b-avat-bl">
                            <img src="/web/templates/UsManag_2/BisList/img/s5.png" alt="">
                        </div>
                        <div class="b-info-bl">
                            <span class="top-sp-b"><strong>You do not have a sales yet</strong> </span>
                        </div>
                        <div class="block-btns-us">
                            <div class="line-li-us-l">&nbsp;</div>
                            <div class="line-li-us-r">&nbsp;</div>
                        </div>
                    </div>
                {/if}
            </div>
        </div>
        <div class="block-customers">
            <div class="top-block-us">
                <span class="title-sp-us">CUSTOMERs</span>
                <a href="#" class="show-add" data-role="6" data-role-name="Customer">&nbsp;</a>
            </div>
            <div class="blocklist-c">
                <form action="#" method="#">
                    <input type="text" name="search-c" class="inp-cust" placeholder="Start type name or id">
                </form>
                <div class="body-blockl-c">
                    {if $users != 0}
                        {foreach from = $users item='customer'}
                            {if $customer->getRole()->getRoleCode() == '2C'}
                                <div class="block-c">
                                    <div class="c-avat-bl">
                                        <img src="/web/templates/UsManag_2/BisList/img/users/customer-photo.png" alt="">
                                    </div>
                                    <div class="c-info-bl">
                                        <span class="top-sp-c"><a href="/usermanagement/auth/{$user->getUserId()}/{$customer->getUserId()}">{$customer->getUserFirstName()} {$customer->getUserLastName()}</a> ({$customer->getUserId()})</span>
                                        <span class="bt-ps-c">{$customer->getCompanyName()}</span>
                                    </div>
                                </div>
                            {/if}
                        {/foreach}
                    {else}
                        <div class="block-c">
                            <div class="c-avat-bl">
                                <img src="/web/templates/UsManag_2/BisList/img/users/customer-photo.png" alt="">
                            </div>
                            <div class="c-info-bl">
                                <span class="top-sp-c"><a href="#">You do not have any customer yet</a></span>
                            </div>
                        </div>
                    {/if}
                </div>
            </div>
        </div>
    </div>
</div>
{literal}
    <script type="text/javascript">
        $(".btn-block").click(function (e) {
            e.preventDefault();
            getAjax(this);
        });

        $(".btn-delete").click(function (e) {
            e.preventDefault();
            postAjax(this);
        });

        function getAjax(selector) {
            var href = $(selector).attr('href');
            $.get(href, function (response) {
                if (response == '') {
                    alert('You can not block/delete a businessman');
                    return false;
                } else {
                    alert('Done');
                    if ($(selector).text() == 'Block user') {
                        $(selector).parents('.b-block').toggleClass('deleted');
                        $('.deleted').insertAfter($(selector).parents('.b-block').last());
                        $(selector).text('Unblock User');

                    } else {
                        $(selector).text('Block user');
                    }
                }
            });
        }

        function postAjax(selector) {
            var href = $(selector).attr('href');
            var id = $(selector).data('id');
            $.post(href, {user_id: id});
        }

        $('.btn-change').click(function () {
            var roleCode = $(this).data('change');
            var roleName = $(this).data('rolename');
            var formText = "Edit ";

            $(".role_id").val(roleCode);
            $(".title-popup-form").text(formText + roleName)
            $('#popup-form, #bg-layer-2').show();
            $('#bg-layer-2').animate({
                opacity: 0.8
            });
            
            $('.input-firstname').val()

        });
    </script>
{/literal}