<!DOCTYPE html>
<html lang="en">
    {assign var="user" value=$smarty.session.register.user_auth|base64_decode|unserialize}
    <head>
        <meta charset="UTF-8">
        <title>PowerPlay</title>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/web/templates/UsManag_2/BisList/css/style.css">
        <link rel="stylesheet" href="/web/templates/main/css/main-style.css">
        <script src="/web/templates/main/js/jquery-1.11.3.min.js"></script>
    </head>
    <body>
        <!-- Popup SMS -->
        <div id="bg-layer">&nbsp;</div>
        <div id="bg-layer-2">&nbsp;</div>
        <div class="popup-form" id="popup-form">
            <a href="#" class="img-close-bl">&nbsp;</a>
            <span class="title-popup-form">Add Businessman</span>
            <form action="" method="" class="reg-form-b">
                <div class="block-name">
                    <div class="block-title-name">NAME</div>
                    <div class="firstname">
                        <span class="name-input">Firstname:</span>
                        <input type="text" class="input-firstname input-main">
                    </div>
                    <div class="lastname">
                        <span class="name-input">Lastname:</span>
                        <input type="text" class="input-lastname input-main">
                    </div>
                </div>
                <div class="block-company">
                    <div class="company">
                        <span class="name-input">Company:</span>
                        <input type="text" class="input-company input-main">
                    </div>
                </div>
                <div class="block-address">
                    <div class="block-title-name">ADDRESS</div>
                    <div class="street">
                        <span class="name-input">Street and House Number:</span>
                        <input type="text" class="input-street input-main">
                    </div>
                    <div class="postalcode-city">
                        <div class="postalcode">
                            <span class="name-input">Postal Code:</span>
                            <input type="text" class="input-postalcod input-main">
                        </div>
                        <div class="city">
                            <span class="name-input">City:</span>
                            <input type="text" class="input-city input-main">
                        </div>
                    </div>
                    <div class="region">
                        <span class="name-input">Region:</span>
                        <input type="text" class="input-region input-main">
                    </div>
                    <div class="country">
                        <span class="name-input">Country:</span>
                        <input type="text" class="input-country input-main">
                    </div>
                </div>
                <div class="block-phone">
                    <div class="phone">
                        <span class="name-input">Cell phone number:</span>
                        <input type="text" class="input-phone input-main">
                    </div>
                </div>
                <span class="format-phone">in format +00 (000) 633-513-65</span>
                <div class="check">
                    <div class="btn-form">
                        <button class="btn-continue"><span>ADD</span></button>
                    </div>
                </div>
            </form>
        </div>
        <div id="sms-popup">
            <div class="top-sms-popup">
                <div class="logo-sms-img-popup">
                    &nbsp;
                </div>
                <div class="sms-info-block-popup">
                    <span class="from-block-popup">from VIRTURO</span>
                    <span class="time-sms-block-popup">07.08.2015 at 17:45</span>
                </div>
            </div>
            <div class="sms-popup-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna</p>
            </div>
        </div>
        <!-- Popup SMS -->
        {include file='../../main/header.tpl'}
        {include file='../../main/left_side.tpl'}
        <!-- CONTENT -->
        <div id="container">
            <div class="content">
                <div class="content-body">
                    {if $user->getRole()->getRoleCode() == '2B'}
                        {include file='./listBusinessman.tpl'}
                    {elseif $user->getRole()->getRoleCode() == '2S'}
                        {include file='./listSales.tpl'}
                    {else}
                        {assign var='backUp' value = $smarty.session.register.user_back_up|base64_decode|unserialize}
                        {if !$backUp|is_bool}
                            {assign var='backUp2' value = $smarty.session.register.user_back_up_2|base64_decode|unserialize}
                            {if !$backUp2|is_null and !$backUp2|is_bool}
                                <a href="#">{$user->getUserFirstName()}</a> (you are here)| <a href="/usermanagement/back/">{$backUp->getUserFirstName()}</a> | <a href="/usermanagement/goup/">{$backUp2->getUserFirstName()}</a>
                            {else}
                                <a href="#">{$user->getUserFirstName()}</a> (you are here)| <a href="/usermanagement/back/">{$backUp->getUserFirstName()}</a> 

                            {/if}
                        {/if}
                        <br>
                        You are a customer and does not have this possibility
                    {/if}
                </div>
            </div>
        </div>
        <!-- CONTENT END -->
        {include file='../../main/right_side.tpl'}
        {include file='../../main/footer.tpl'}
        <script src="/web/templates/main/js/jquery.fs.selecter.min.js"></script>
        <script src="/web/templates/main/js/main-ds.js"></script>
        <script src="/web/templates/Support/FAQ/js/main.js"></script>
        <script>
            $('#modal-sms, #bg-layer, #sms-popup, #mod-window-sms').hide();

            $('.show-info-b').click(function () {
                $(this).parent().children('.mod-window-sms').fadeIn(150);
                $(this).parent('.b-block').css("z-index", "99999");
                $('#bg-layer').fadeIn();
            });
        </script>
        <script>
            $('#popup-form, #bg-layer-2').hide();
            $('.show-add').click(function () {
                $('#bg-layer-2').fadeIn(300);
                $('#popup-form').fadeIn(50);
                $('#popup-form').animate({
                    top: '82px'
                }, 300);
            });

            $('#bg-layer-2').click(function () {
                $('#popup-form, #bg-layer-2').fadeOut(400);
                $('#popup-form').animate({
                    top: '-789px'
                });
            });
        </script>
        <script>
            $('.btn-ok-close').click(function () {
                $('#popup-form, #bg-layer-2').fadeOut(400);
                $('.b-block').css("z-index", "10");
            })
        </script>
        <script>
            $('a.img-close-bl').click(function () {
                $('div#popup-form, #bg-layer-2').fadeOut(400);
                $('#popup-form').animate({
                    top: '-789px'
                }, 450);
            })
        </script>
        <script>
            $('.b-block').mouseover(function () {
                $(this).css("margin-top", "20px");
                // $(this).filter(":last").css("margin-bottom", "38px");
                $(this).css("background-color", "#4b545d");
                $(this).css("cursor", "pointer");
                $(this).css("padding-top", "15px");
                $(this).css("padding-bottom", "38px");
                $(this).find('.block-btns-us').stop().fadeIn(100);

                $(this).find('.show-info-b').css("display", "block");
                $(this).find('.show-info-s').css("display", "block");
            });
            $('.b-block').mouseleave(function () {
                $(this).css("background-color", "#fff");
                $(this).css("padding-top", "0");
                $(this).css("padding-bottom", "0");
                $(this).find('.block-btns-us').stop().fadeOut(100);

                $(this).find('.show-info-b').css("display", "none");
                $(this).find('.show-info-s').css("display", "none");
            });
        </script>
        <script>
            $('.show-info-b').click(function () {
                $(this).parent('.b-block').css("margin-top", "20px");
                $(this).parent('.b-block').css("background-color", "#4b545d");
                $(this).parent('.b-block').css("cursor", "pointer");
                $(this).parent('.b-block').css("padding-top", "15px");
                $(this).parent('.b-block').css("padding-bottom", "38px");
                $(this).parent().find('.top-sp-b strong').css("color", "#fff");
                $(this).parent().find('.top-sp-b').css("color", "#cdd6de");
                $(this).parent().find('.bt-ps-b').css("color", "#cdd6de");
                $(this).parent().find('.block-btns-us').css("display", "block");
                $(this).css("opacity", "1");
                $(this).css("background-color", "#fff");
                $(this).css("background-image", "url('../../images/User-Management/Businessman-List/arr-hb.png')");
            });


            var test = $('.block-businessmans').height();

            if (test < 677) {
                $('.block-businessmans').find('.blocklist-b').css({
                    overflowY: 'auto'
                })
            } else {
                $('.block-businessmans').find('.blocklist-b').css({
                    overflow: 'hidden'
                })
            }
        </script>
        <script>
            $('.b-block').mouseleave(function () {
                if ($(this).find('.mod-window-sms').css('display') === 'block') {
                    $(this).css("margin-top", "20px");
                    $(this).css("background-color", "#4b545d");
                    $(this).css("cursor", "pointer");
                    $(this).css("padding-top", "15px");
                    $(this).css("padding-bottom", "38px");
                    $(this).find('.top-sp-b strong').css("color", "#fff");
                    $(this).find('.top-sp-b').css("color", "#cdd6de");
                    $(this).find('.bt-ps-b').css("color", "#cdd6de");
                    $(this).find('.block-btns-us').stop().fadeIn(100);
                    $(this).find('.show-info-b').css("display", "block");
                }
            });
        </script>
        <script>
            $('.btn-ok-close').click(function () {
                $('#bg-layer, #mod-window-sms').fadeOut(150);
                $(this).closest('.b-block').css("background-color", "#fff");
                $(this).closest('.b-block').css("padding-top", "0");
                $(this).closest('.b-block').css("padding-bottom", "0");
                $(this).closest('.b-block').find('.top-sp-b strong').css("color", "#4b545d");
                $(this).closest('.b-block').find('.top-sp-b').css("color", "#949a9f");
                $(this).closest('.b-block').find('.bt-ps-b').css("color", "#949a9f");
                $(this).closest('.b-block').find('.block-btns-us').css("display", "none");
                $(this).closest('.b-block').find('.show-info-b').css("display", "none");
                $(this).closest('.b-block').find('.show-info-b').css("background-color", "#98c11f");
                $(this).closest('.b-block').find('.show-info-b').css("background-image", "url('../../images/User-Management/Businessman-List/arr-b.png')");
            })
        </script>
        <script>
            $('#bg-layer').click(function () {
                $('#bg-layer, #mod-window-sms').fadeOut(150);
                $('.b-block').css("z-index", "10");
                $('.b-block').css("background-color", "#fff");
                $('.b-block').css("padding-top", "0");
                $('.b-block').css("padding-bottom", "0");
                $('.b-block').find('.top-sp-b strong').css("color", "#4b545d");
                $('.b-block').find('.top-sp-b').css("color", "#949a9f");
                $('.b-block').find('.bt-ps-b').css("color", "#949a9f");
                $('.b-block').find('.block-btns-us').css("display", "none");
                $('.b-block').find('.show-info-b').css("display", "none");
                $('.b-block').find('.show-info-b').css("background-color", "#98c11f");
                $('.b-block').find('.show-info-b').css("background-image", "url('../../images/User-Management/Businessman-List/arr-b.png')");
            });
        </script>

    </body>

</html>
