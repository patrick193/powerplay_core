<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>Power Play</title>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/web/templates/login/step/css/style.css">
    </head>

    <body>
        <header>
            <div class="header-left-block">
                <div class="logo-img">
                    <a href="/login"><img src="/web/templates/login/step/img/logo.png" alt="logo"></a>
                </div>
                <div class="title-text"><span>BUSINESSMAN</span></div>
            </div>
            <div class="header-top">
                <div class="header-text">
                    <span class="title-text">Registretion Step 2</span>
                    <p><a href="/login">home</a><span class="arrow-step">&nbsp;</span><a href="/login/page/registration">registration</a><span class="arrow-step">&nbsp;</span><span class="finish-text">step two</span></p>
                </div>
            </div>
        </header>
        <div class="content">
            <form action="/login/step" method="post" class="reg-form">
                <div class="title-form">
                    <span class="title-t">Registration</span>
                    <p>Hello. Welcome to PowerPlay system. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>
                </div>
                <div class="lang-block">
                    <span class="lang-title">Language:</span><a href="#" class="lang-brit">&nbsp;</a><a href="#" class="lang-ger">&nbsp;</a>
                </div>
                <div class="block-name">
                    <div class="block-title-name">NAME</div>
                    <div class="firstname">
                        <span class="name-input">Firstname:</span>
                    <input type="text" class="input-firstname input-main" name="fname" >
                    </div>
                    <div class="lastname">
                        <span class="name-input">Lastname:</span>
                    <input type="text" class="input-lastname input-main" name="lname" >
                    </div>
                </div>
                <div class="block-company">
                    <div class="company">
                        <span class="name-input">Company:</span>
                    <input type="text" class="input-company input-main" name="company">
                    </div>
                </div>
                <div class="block-address">
                    <div class="block-title-name">ADDRESS</div>
                    <div class="street">
                        <span class="name-input">Street and House Number:</span>
                    <input type="text" class="input-street input-main" name="street">
                    </div>
                <div class="postalcode-country">
                        <div class="postalcode">
                            <span class="name-input">Postal Code:</span>
                        <input type="text" class="input-postalcod input-main" name="postal">
                        </div>
                        <div class="country">
                            <span class="name-input">Country:</span>
                            <select type="text" class="input-country input-main" name="country">
                            </select>
                        </div>
                    </div>
                    <div class="region">
                        <span class="name-input">Region:</span>
                        <select type="text" class="input-region input-main" name="region">
                        </select>
                    </div>
                    <div class="city">
                        <span class="name-input">City:</span>
                        <select type="text" class="input-city input-main" name="city">
                        </select>
                    </div>
                </div>
                <div class="block-phone">
                    <div class="phone">
                        <span class="name-input">Cell phone number:</span>
                    <input type="text" class="input-phone input-main" name="cell_phone">
                    </div>
                </div>
                <span class="format-phone">in format +00 (000) 633-513-65</span>
                <div class="check">
                    <label>
                        <input type="checkbox" class="checkbox"><span class="label-text">By signing up, I agree to</span>
                        <a href="#" class="chek-link">general terms of living internet.</a>
                    </label>
                    <div class="btn-form">
                        <button class="btn-continue"><span>Continue</span></button>
                    </div>
                </div>
            </form>
        </div>
        <footer>
            <div class="ft-wrapper">
                <div class="copywrite">
                    <span class="copywr">2015  ©</span>
                    <div class="ft-logo">
                        <a href="#"><img src="img/logo-LI.png" alt="logo"></a>
                    </div>
                </div>
                <!-- <div class="ft-links">
                    <ul>
                        <li><a href="#">Imprint</a></li>
                        <li><a href="#">Conditions</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Terms and Conditions</a></li>
                        <li><a href="#">Confidentiality Agreement</a></li>
                    </ul>
                </div> -->
                <div class="ft-contacts">
                    <div class="ft-phone">
                        <span>telephone:</span><span class="ft-phone-number">+1-541-754-3010</span>
                    </div>
                    <div class="ft-social">
                        <span>We are in social:</span>
                        <a href="#" class="ft-link-fb">&nbsp;</a>
                        <a href="#" class="ft-link-gl">&nbsp;</a>
                    </div>
                </div>
            </div>
        </footer>
        <script src="/web/templates/login/step/js/jquery-2.1.4.js"></script>
        <link rel="stylesheet" href="/web/templates/login/step/css/select2.css">
        <script src="/web/templates/login/step/js/select2.js"></script>
        <script>

            function formatList(data) {
                if (data.loading) {
                    return data.text;
                }

                return data.name;
            }

            function formatListSelection(data) {
                id = null;
                id = data.id;
                return data.name;
            }

            $(document).ready(function () {
                id = null;
                countrySelect2();
                regionSelect2();
                citySelect2();
            });

            function countrySelect2()
            {
                $(".input-country").select2({
                    ajax: {
                        url: "/geo/country/",
                        dataType: 'json',
                        data: function (params) {
                            return {
                                key: params.term,
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: data.items,
                            };
                        },
                        cache: true
                    },
                    escapeMarkup: function (markup) {
                        return markup;
                    },
                    minimumInputLength: 2,
                    templateResult: formatList,
                    templateSelection: formatListSelection
                });
            }
            
            function regionSelect2()
            {
                $(".input-region").select2({
                    ajax: {
                        url: "/geo/region/",
                        dataType: 'json',
                        data: function (params) {
                            return {
                                key: params.term,
                                country_id: id
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: data.items,
                            };
                        },
                        cache: true
                    },
                    escapeMarkup: function (markup) {
                        return markup;
                    },
                    minimumInputLength: 2,
                    templateResult: formatList,
                    templateSelection: formatListSelection
                });
            }
            
            function citySelect2()
            {
                $(".input-city").select2({
                    ajax: {
                        url: "/geo/city/",
                        dataType: 'json',
                        data: function (params) {
                            return {
                                key: params.term,
                                region_id: id
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: data.items,
                            };
                        },
                        cache: true
                    },
                    escapeMarkup: function (markup) {
                        return markup;
                    },
                    minimumInputLength: 2,
                    templateResult: formatList,
                    templateSelection: formatListSelection
                });
            }
        </script>
    </body>

</html>
