<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Power Play</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/web/templates/login/registration/css/style.css">
</head>

<body>
    <div id="container">
        <div class="wrapper">
            <div class="content">
                <div class="logo-block">
                    <div class="logo-img">
                        <a href="/login"><img src="/web/templates/login/registration/img/logo.png" alt=""></a>
                    </div>
                    <div class="title-text">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                    </div>
                    <div class="lang-block">
                        <span class="lang-title">Language:</span><a href="#" class="lang-brit">&nbsp;</a><a href="#" class="lang-ger">&nbsp;</a>
                    </div>
                    <div class="btn-started">
                        <a href="/login"><span>BACK TO enter</span></a>
                    </div>
                    <footer>
                        <div class="ft-logo">
                            <a href="#">
                                <img src="/web/templates/login/registration/img/logo-LI.png" alt="">
                            </a>
                        </div>
                        <div class="ft-info">
                            <span class="ft-phone">+1-541-754-3010</span>
                            <span class="ft-cp">2015 © copywrite</span>
                        </div>
                    </footer>
                </div>
                <div class="authorization-block">
                    <div class="info-login-reg">
                        <div class="block-reg">
                            <div class="reg-logo-img">
                                <img src="/web/templates/login/registration/img/form-logo.png" alt="">
                            </div>
                            <span class="title-text">Register</span>
                            <a href="/login/facebook" class="login-with-mail">Sign up with Facebook or Google</a>
                            <div class="login-with-social">
                                <div class="or-bg">
                                    <span>or</span>
                                </div>
                                <div class="autoriz-social">
                                    <span>Sign up with social media</span>
                                    <form action="/login/registration/" method="post" enctype="multipart/form-data" class="form-block">
                                        <div class="input-form">
                                            <input type="text" class="input-mail" placeholder="enter your mail or id number" name="email">
                                            <div class="line-g">&nbsp;</div>
                                            <div class="block-pass">
                                                <input type="password" class="input-pass" placeholder="enter your password" name="pwd">
                                            </div>
                                        </div>
                                        <div class="check">
                                            <label>
                                                <input type="checkbox" name="agree" class="checkbox"><span class="label-text">By signing up, I agree to</span>
                                                <a href="#" class="chek-link">general terms of living internet.</a>
                                            </label>
                                        </div>
                                        <div class="btn-form">
                                            <button class="btn-continue"><span>Continue</span></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="line-bt">&nbsp;</div>
                            <div class="block-reg-bottom">
                                <p><span>Already <strong>powerplay</strong> an member? </span><a href="/login">Log in</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
