<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Power Play</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/web/templates/login/Reg-with-social/css/style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
    <div id="container">
        <div class="wrapper">
            <div class="content">
                <div class="logo-block">
                    <div class="logo-img">
                        <a href="/authorization"><img src="/web/templates/login/Reg-with-social/img/logo.png" alt=""></a>
                    </div>
                    <div class="title-text">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                    </div>
                    <div class="lang-block">
                        <span class="lang-title">Language:</span><a href="#" class="lang-brit">&nbsp;</a><a href="#" class="lang-ger">&nbsp;</a>
                    </div>
                    <div class="btn-started">
                        <a href="/authorization"><span>BACK TO enter</span></a>
                    </div>
                    <footer>
                        <div class="ft-logo">
                            <a href="#">
                                <img src="/web/templates/login/Reg-with-social/img/logo-LI.png" alt="">
                            </a>
                        </div>
                        <div class="ft-info">
                            <span class="ft-phone">+1-541-754-3010</span>
                            <span class="ft-cp">2015 © copywrite</span>
                        </div>
                    </footer>
                </div>
                <div class="authorization-block">
                    <div class="info-login-reg">
                        <div class="block-reg">
                            <div class="reg-logo-img">
                                <img src="/web/templates/login/Reg-with-social/img/form-logo.png" alt="">
                            </div>
                            <span class="title-text">Register</span>
                            <div class="login-with-social">
                                <div class="or-bg">
                                    &nbsp;
                                </div>
                                <div class="autoriz-social">
                                    <span>Sign up with social media</span>
                                    <a href="/authorization/facebookreg" class="login-fb"><span>Sign up with Facebook</span></a>
                                    <a href="/authorization/googlereg" class="login-gl"><span>Sign up with Google+</span></a>
                                    <span class="text-b">or</span>
                                    <a href="/authorization/step1" class="login-with-mail">Sign up with E-mail</a>
                                </div>
                            </div>
                            <div class="check">
                                <label>
                                    <input type="checkbox" class="checkbox"><span class="label-text">By signing up, I agree to</span>
                                    <a href="#" class="chek-link">general terms of living internet.</a>
                                </label>
                            </div>
                            <div class="line-bt">&nbsp;</div>
                            <div class="block-reg-bottom">
                                <p><span>Already <strong>powerplay</strong> an member? </span><a href="/authorization">Log in</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
