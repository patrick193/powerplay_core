<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Power Play</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/web/templates/login/step3/css/style.css">
</head>

<body>
    <header>
        <div class="header-left-block">
            <div class="logo-img">
                <a href="/athorization"><img src="/web/templates/login/step3/img/logo.png" alt="logo"></a>
            </div>
            <div class="title-text"><span>BUSINESSMAN</span></div>
        </div>
        <div class="header-top">
            <div class="header-text">
                <span class="title-text">Registretion Step 3</span>
                <p><a href="/authorization">home</a><span class="arrow-step">&nbsp;</span><a href="/authorization/step1">registration</a><span class="arrow-step">&nbsp;</span><a href="/authorization/step2">step two</a><span class="arrow-step">&nbsp;</span><span class="finish-text">step three</span></p>
            </div>
        </div>
    </header>
    <div class="content">
        <form action="/authorization/end/" method="" class="verification-form">
            <div class="title-form">
                <span>Verification</span>
            </div>
            <div class="text-info-form">
                <p class="text-info-block">We sent you a verification code to your
                    <br>phone +00 (000) 633-513-65. Code come within minutes.</p>
            </div>
            <input type="text" class="enter-code" placeholder="Enter code here" name="conf_sms_code">
            <div class="change-pr">
                <a href="/Dashboard-start">Change phone</a>
            </div>
            <div class="btn-form">
                <button class="get-str"><span>GET STARTED</span></button>
            </div>
        </form>
    </div>
    <footer>
        <div class="ft-wrapper">
            <div class="copywrite">
                <span class="copywr">2015  ©</span>
                <div class="ft-logo">
                    <a href="#"><img src="/web/templates/login/step3/img/logo-LI.png" alt="logo"></a>
                </div>
            </div>
            <!-- <div class="ft-links">
                <ul>
                    <li><a href="#">Imprint</a></li>
                    <li><a href="#">Conditions</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Terms and Conditions</a></li>
                    <li><a href="#">Confidentiality Agreement</a></li>
                </ul>
            </div> -->
            <div class="ft-contacts">
                <div class="ft-phone">
                    <span>telephone:</span><span class="ft-phone-number">+1-541-754-3010</span>
                </div>
                <div class="ft-social">
                    <span>We are in social:</span>
                    <a href="#" class="ft-link-fb">&nbsp;</a>
                    <a href="#" class="ft-link-gl">&nbsp;</a>
                </div>
            </div>
        </div>
    </footer>
</body>

</html>
