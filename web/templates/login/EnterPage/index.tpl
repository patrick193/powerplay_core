<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Power Play</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/web/templates/login/EnterPage/css/style.css">
</head>

<body>
    <div id="container">
        <div class="wrapper">
            <div class="content">
                <div class="logo-block">
                    <div class="logo-img">
                        <a href="/autorization"><img src="/web/templates/login/EnterPage/img/logo.png" alt=""></a>
                    </div>
                    <div class="title-text">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                    </div>
                    <div class="btn-started">
                        <a href="/login/page/registration"><span>Get Started</span></a>
                    </div>
                    <footer>
                        <div class="ft-logo">
                            <a href="#">
                                <img src="/web/templates/login/EnterPage/img/logo-LI.png" alt="">
                            </a>
                        </div>
                        <div class="ft-info">
                            <span class="ft-phone">+1-541-754-3010</span>
                            <span class="ft-cp">2015 © copywrite</span>
                        </div>
                    </footer>
                </div>
                <div class="authorization-block">
                    <div class="info-login-reg">
                        <div class="block-login">
                            <div class="form-logo-img">
                                <img src="/web/templates/login/EnterPage/img/form-logo.png" alt="">
                            </div>
                            <form action="/login/auth/" method="post" class="form-block">
                                <span class="title-text">Login</span>
                                <div class="input-form">
                                    <input type="text" class="input-mail" placeholder="your mail or id number" name="login">
                                    <div class="line-g">&nbsp;</div>
                                    <div class="block-pass">
                                        <input type="password" class="input-pass" placeholder="password" name="pwd">
                                        <div class="line-v">&nbsp;</div>
                                        <div class="fogot">
                                            <a href="/login/page/forgot/">Fogot?</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="check">
                                    <label>
                                        <input type="checkbox" class="checkbox"><span class="label-text">Remember me</span>
                                    </label>
                                </div>
                                <div class="btn-form">
                                    <button class="btn-continue"><span>Continue</span></button>
                                </div>
                            </form>
                            <div class="login-with-social">
                                <div class="or-bg">
                                    <span>or</span>
                                </div>
                                <div class="autoriz-social">
                                    <span>Login with social media</span>
                                    <a href="/login/facebookauth/" class="login-fb"><span>Sign with in Facebook</span></a>
                                    <a href="/login/googleauth/" class="login-gl"><span>Sign with in Google+</span></a>
                                </div>
                            </div>
                            <div class="link-to-registration-sm">
                                <a href="../Reg-with-social/index.html">Have not account yet?</a>
                            </div>
                        </div>
                        <div class="link-to-registration">
                            <a href="../Reg-with-social/index.html">Have not account yet?</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
