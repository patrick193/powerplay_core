<!DOCTYPE html>
<html lang="en">
    {assign var="user" value=$smarty.session.register.user_auth|base64_decode|unserialize}
    <head>
        <meta charset="UTF-8">
        <title>PowerPlay</title>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
        {if $user->getRole()->getRoleCode() == '2B'}
            <link rel="stylesheet" href="/web/templates/UserManagement/css/business.css">
        {elseif $user->getRole()->getRoleCode() == '2S'}
            <link rel="stylesheet" href="/web/templates/UserManagement/css/sales.css">
        {/if}
        <link rel="stylesheet" href="/web/templates/main/css/main-style.css">
        <script src="/web/templates/main/js/jquery-1.11.3.min.js"></script>
    </head>
    <body>
        <!-- Popup SMS -->
        <div id="bg-layer">&nbsp;</div>
        <div id="bg-layer-2">&nbsp;</div>
        {include file='../main/profile.tpl'}
        <div id="sms-popup">
            <div class="top-sms-popup">
                <div class="logo-sms-img-popup">
                    &nbsp;
                </div>
                <div class="sms-info-block-popup">
                    <span class="from-block-popup">from VIRTURO</span>
                    <span class="time-sms-block-popup">07.08.2015 at 17:45</span>
                </div>
            </div>
            <div class="sms-popup-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna</p>
            </div>
        </div>
        <!-- Popup SMS -->
        {include file='../main/header.tpl'}
        {include file='../main/left_side.tpl'}
        <!-- CONTENT -->
        <div id="container">
            <div class="content">
                <div class="content-body">
                    <div class="blist">
                        {if $user->getRole()->getRoleCode() == '2B'}
                            <div class="top-block-b">
                                <div class="block-user">
                                    <div class="us-avat-bl">
                                        <img src="../../images/User-Management/Businessman-List/ava-bl.png" alt="">
                                    </div>
                                    <div class="us-info-bl">
                                        <span class="top-sp-us"><strong>That’s you</strong> ({$user->getUserId()})</span>
                                        <span class="bt-ps-us">{$user->getCompanyName()}, Businessman</span>
                                    </div>
                                </div>
                                <div class="line-left">&nbsp;</div>
                                <div class="line-mid">&nbsp;</div>
                                <div class="line-right">&nbsp;</div>
                            </div>
                        {elseif $user->getRole()->getRoleCode() == '2S'}
                            <div class="top-block-s">
                                <div class="block-user">
                                    <div class="us-avat-bl">
                                        <img src="../../images/User-Management/Sales-list/s5.png" alt="">
                                    </div>
                                    <div class="us-info-bl">
                                        <span class="top-sp-us"><strong>{$user->getUserFirstName()} {$user->getUserLastName()}</strong> ({$user->getUserId()})</span>
                                        <span class="bt-ps-us">{$user->getCompanyName()}, salesman</span>
                                    </div>
                                </div>
                                <div class="line-left-s">&nbsp;</div>
                                <div class="line-mid-s">&nbsp;</div>
                                <div class="line-right-s">&nbsp;</div>
                                <div class="line-to-u">&nbsp;</div>
                                <a href="#" class="block-yours">
                                    <span>That’s you</span>
                                </a>
                            </div>
                        {/if}
                        <div class="main-block-user">
                            <div class="block-businessmans">
                                <div class="top-block-us">
                                    <span class="title-sp-us">BUSINESSMANs</span>
                                    {if $user->getRole()->getRoleCode() == '2B'}
                                        <a href="#" class="show-add">&nbsp;</a>
                                    {/if}
                                </div>
                                <div class="scroll scroll_negative-viewport">
                                    <div class="mod-window-sms" id="mod-window-sms">
                                        <div class="bl-left-i">&nbsp;</div>
                                        <div class="block-inf-popup">
                                            <span>Sorry. This information is confidential and available only Juna Dapas</span>
                                            <span class="sp-scnd">You can view information about your salesmans and customers.</span>
                                            <a href="#" class="btn-ok-close">
                                                <span>OK</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="wrapper baron wrp-scroll">
                                        <div class="scroller scroll-b">
                                            <div class="blocklist-b">
                                                {if $user->getRole()->getRoleCode() == '2B'}
                                                    {*                                                    TODO FREACH *}
                                                    <div class="b-block">
                                                        <a href="#" class="show-info-b">
                                                        </a>
                                                        <div class="b-avat-bl">
                                                            <img src="../../images/User-Management/Businessman-List/b1.png" alt="">
                                                        </div>
                                                        <div class="b-info-bl">
                                                            <span class="top-sp-b"><strong class="str-name-us">Juan Dapas</strong> (10102334)</span>
                                                            <span class="bt-ps-b">LivingInternet</span>
                                                        </div>
                                                        <div class="block-btns-us">
                                                            <div class="line-li-us-l">&nbsp;</div>
                                                            <div class="line-li-us-r">&nbsp;</div>
                                                            <a href="#" class="btn-change">Change</a>
                                                            <a href="#" class="btn-block">Block user</a>
                                                            <a href="#" class="btn-delete">Delete</a>
                                                        </div>
                                                    </div>
                                                {else}
                                                    <div class="b-block-sl">
                                                        <span>Salesman can only have<br>customers as subusers.</span>
                                                    </div>
                                                {/if}
                                                <div class="scroller__bar-wrapper scroll-bsn">
                                                    <div class="scroller__bar"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="block-sales">
                                <div class="top-block-us">
                                    <span class="title-sp-us">SALEs</span>
                                    {if $user->getRole()->getRoleCode() == '2B'}
                                        <a href="#" class="show-add">&nbsp;</a>
                                    {/if}
                                </div>
                                <div class="scroll1 scroll_negative-viewport">
                                    <div class="wrapper wrp-scroll">
                                        <div class="scroller scroll-s">
                                            <div class="blocklist-b">
                                                {if $user->getRole()->getRoleCode() == '2B'}
                                                    <div class="b-block">
                                                        <a href="#" class="show-info-s">
                                                        </a>
                                                        <div class="b-avat-bl">
                                                            <img src="../../images/User-Management/Businessman-List/s5.png" alt="">
                                                        </div>
                                                        <div class="b-info-bl">
                                                            <span class="top-sp-b"><strong>Juan Dapas</strong> (10102334)</span>
                                                            <span class="bt-ps-b">LivingInternet</span>
                                                        </div>
                                                        <div class="block-btns-us">
                                                            <div class="line-li-us-l">&nbsp;</div>
                                                            <div class="line-li-us-r">&nbsp;</div>
                                                            <a href="#" class="btn-change">Change</a>
                                                            <a href="#" class="btn-block">Block user</a>
                                                            <a href="#" class="btn-delete">Delete</a>
                                                        </div>
                                                    </div>
                                                {elseif $user->getRole()->getRoleCode() == '2S' }
                                                    <div class="block-s-user">
                                                        <div class="b-avat-bl">
                                                            <img src="../../images/User-Management/Sales-list/s5.png" alt="">
                                                        </div>
                                                        <div class="b-info-bl">
                                                            <span class="top-sp-b"><strong>{$user->getUserFirstName()} {$user->getUserLastName()}</strong> ({$user->getUserId()})</span>
                                                            <span class="bt-ps-b">{$user->getCompanyName()}</span>
                                                        </div>
                                                    </div>
                                                    <div class="b-block-sl">
                                                        <span>Salesman can only have<br>customers as subusers.</span>
                                                    </div>
                                                {/if}
                                                <div class="scroller__bar-wrapper scroll-sls">
                                                    <div class="scroller__bar"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="block-customers">
                                <div class="top-block-us">
                                    <span class="title-sp-us">CUSTOMERs</span>
                                    {if $user->getRole()->getRoleCode() == '2B'}
                                        <a href="#" class="show-add">&nbsp;</a>
                                    {/if}
                                </div>
                                <div class="blocklist-c">
                                    <form action="#" method="#">
                                        <input type="text" name="search-c" class="inp-cust" placeholder="Start type name or id">
                                    </form>
                                    <div class="scroll2 scroll_negative-viewport">
                                        <div class="wrapper wrp-scroll">
                                            <div class="scroller">
                                                <div class="body-blockl-c">
                                                    <div class="block-c">
                                                        <div class="c-avat-bl">
                                                            <img src="../../images/User-Management/Businessman-List/customer-photo.png" alt="">
                                                        </div>
                                                        <div class="c-info-bl">
                                                            <span class="top-sp-c"><a href="#">Peter Kirk</a> (10102334)</span>
                                                            <span class="bt-ps-c">LivingInternet</span>
                                                        </div>
                                                    </div>
                                                    <div class="scroller__bar-wrapper scroll-ctm">
                                                        <div class="scroller__bar"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- CONTENT END -->
        {include file='../main/right_side.tpl'}
        {include file='../main/footer.tpl'}
        <!-- FOOTER END -->
        <script src="/web/templates/main/js/baron.js"></script>
        <script src="/web/templates/main/js/main-ds.js"></script>
        <script src="/web/templates/UserManagement/js/business.js"></script>
    </body>

</html>
