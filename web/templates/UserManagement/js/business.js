;$(function () {


    $('#popup-form, #bg-layer-2').hide();
    $('#bg-layer-2').on('click', function() {
        $('#popup-form, #bg-layer-2').fadeOut(400);
        $('#popup-form').animate({
            top: '-789px'
        });
    });


    $('.show-add').on('click', function() {
        $('#bg-layer-2').fadeIn(300);
        $('#popup-form').fadeIn(50);
        $('#popup-form').animate({
            top: '82px'
        }, 300);
    });


    $('.btn-ok-close').on('click', function() {
        $('#popup-form, #bg-layer-2').fadeOut(400);
        $('.b-block').css("z-index", "10");
    });


    $('a.img-close-bl').on('click', function() {
        $('div#popup-form, #bg-layer-2').fadeOut(400);
        $('#popup-form').animate({
            top: '-789px'
        }, 450);
    });


    $('.b-block').mouseover(function() {
        $(this).addClass("b-stl-block");

        $(this).find('.block-btns-us').stop().fadeIn(100);

        $(this).find('.show-info-b').css("display", "block");
        $(this).find('.show-info-s').css("display", "block");
    });
    $('.b-block').mouseleave(function() {
        $(this).removeClass("b-stl-block");

        $(this).find('.block-btns-us').stop().fadeOut(100);

        $(this).find('.show-info-b').css("display", "none");
        $(this).find('.show-info-s').css("display", "none");
        if ($('.mod-window-sms').css('display') === 'block') {
            $(this).addClass("b-stl-block");
            $(this).find('.top-sp-b strong').css("color", "#fff");
            $(this).find('.top-sp-b').css("color", "#cdd6de");
            $(this).find('.bt-ps-b').css("color", "#cdd6de");
            $(this).find('.block-btns-us').stop().fadeIn(100);
            $(this).find('.show-info-b').css("display", "block");
        }
    });


    $('.show-info-b').on('click', function() {
        $(this).parent('.b-block').addClass("b-stl-block");
        $(this).parent().find('.top-sp-b strong').css("color", "#fff");
        $(this).parent().find('.top-sp-b').css("color", "#cdd6de");
        $(this).parent().find('.bt-ps-b').css("color", "#cdd6de");
        $(this).parent().find('.block-btns-us').css("display", "block");
        $(this).css("opacity", "1");
        $(this).css("background-color", "#fff");
        $(this).css("background-image", "url('../../images/User-Management/Businessman-List/arr-hb.png')");
    });



    function somefunction() {
        $('#bg-layer, #mod-window-sms').fadeOut(150);
        $('.b-block').removeClass("b-stl-block");
        $('.b-block').find('.top-sp-b strong').css("color", "#4b545d");
        $('.b-block').find('.top-sp-b').css("color", "#949a9f");
        $('.b-block').find('.bt-ps-b').css("color", "#949a9f");
        $('.b-block').find('.block-btns-us').css("display", "none");
        $('.b-block').find('.show-info-b').css({
            display: "none",
            backgroundColor: "#98c11f",
            backgroundImage: "url('../../images/User-Management/Businessman-List/arr-b.png')"
        });
        $(this).addClass("scroll-b");
    }

    $('#bg-layer, .btn-ok-close').on('click', function() {
        somefunction();
    });


    $('#modal-sms, #bg-layer, #sms-popup, #mod-window-sms').hide();



    if ($('.scroll').height() < 574) {
        $('.scroll-bsn').css("display", "none")
    }

    if ($('.scroll1').height() < 574) {
        $('.scroll-sls').css("display", "none")
    }

    if ($('.scroll2').height() < 519) {
        $('.scroll-ctm').css("display", "none")
    }



    $(".show-info-b").on('click', function(event) {
        $('.mod-window-sms').fadeIn(150);
        $('#bg-layer').fadeIn();

        var offset = $(this).offset();
        event.stopPropagation();

        var pos_top_message_error = offset.top - 211;
        $('.mod-window-sms').css('top', pos_top_message_error);

    });



    window.onload = function() {
        window.scroll = baron({
            root: '.wrapper',
            scroller: '.scroller',
            bar: '.scroller__bar',
            barOnCls: 'baron'
        })

    };



});