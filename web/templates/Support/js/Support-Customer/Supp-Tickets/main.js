  $(document).ready(function() {
      var data_customers = [{
          value: 75,
          color: "rgb(157, 185, 32)",
          highlight: "rgba(255,255,255,0)",
          label: "Satisfied"
      }, {
          value: 25,
          color: "rgb(223, 232, 241)",
          highlight: "rgba(255,255,255,0)",
          label: ""
      }];
      // Satisfied Customers chart creates here 
      var Customers = new Chart(document.getElementById("customers_chart").getContext("2d")).Doughnut(data_customers);
      // Data Traffic chart data
      var data_traffic = [{
          value: 6038,
          color: "rgb(157, 185, 32)",
          highlight: "rgba(157, 185, 32, 1)",
          label: "Visitors"
      }, {
          value: 2974,
          color: "rgb(86, 188, 221)",
          highlight: "rgba(86, 188, 221, 1)",
          label: "Ratio Orders"
      }];
      var data_customers = [{
          value: 75,
          color: "rgb(157, 185, 32)",
          highlight: "rgba(255,255,255,0)",
          label: "Satisfied"
      }, {
          value: 25,
          color: "rgb(223, 232, 241)",
          highlight: "rgba(255,255,255,0)",
          label: ""
      }];
      // Satisfied Customers chart creates here 
      var Customers = new Chart(document.getElementById("customers_chart_1").getContext("2d")).Doughnut(data_customers);
      // Data Traffic chart data
      var data_traffic = [{
          value: 6038,
          color: "rgb(157, 185, 32)",
          highlight: "rgba(157, 185, 32, 1)",
          label: "Visitors"
      }, {
          value: 2974,
          color: "rgb(86, 188, 221)",
          highlight: "rgba(86, 188, 221, 1)",
          label: "Ratio Orders"
      }];
      $(".all-p-b").click(function() {
          if ($(window).width() <= 1920) {
              if ($(".inp-tp").is(':hidden')) {
                  $(".inp-tp").show(200);
              } else {
                  $(".inp-tp").hide(200);
              }
          }
      });
  });
