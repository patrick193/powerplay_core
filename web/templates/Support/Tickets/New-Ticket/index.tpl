<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>PowerPlay</title>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/web/templates/Support/Tickets/New-Ticket/css/style.css">
        <link rel="stylesheet" href="/web/templates/main/css/main-style.css">
        <script src="/web/templates/main/js/jquery-1.11.3.min.js"></script>
    </head>

    <body>
        <!-- Popup SMS -->
        <div id="sms-popup">
            <div class="top-sms-popup">
                <div class="logo-sms-img-popup">
                    &nbsp;
                </div>
                <div class="sms-info-block-popup">
                    <span class="from-block-popup">from VIRTURO</span>
                    <span class="time-sms-block-popup">07.08.2015 at 17:45</span>
                </div>
            </div>
            <div class="sms-popup-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna</p>
            </div>
        </div>
        <!-- Popup SMS -->
        <!-- HEADER -->
        {include file='../../../main/header.tpl'}
        <!-- LEFTBAR -->
        {include file='../../../main/left_side.tpl'}
        <!-- CONTENT -->
        <div id="container">
            <div class="content">
                <div class="content-body">
                    <div class="filters-block">
                        <span class="title-flt">Fast search category:</span>
                        <form action="#" method="" class="form-flt">
                            <select class="inp-ftr" type="text" name="search" placeholder="start type category name ...">
                            </select>
                            {*                            <input class="inp-ftr" type="text" name="search" placeholder="start type category name ...">*}
                        </form>
                    </div>
                    {foreach $categories.query as $category }
                        <div class="questions-block">
                            <div class="quest-blck">
                                <div class="quest-body">
                                    <div class="qeust-top-block">
                                        <span class="title-qest">{$category->category_name}</span>
                                        <a href="/support/ticket/create/{$category->ticket_category_id}" class="btn-cr-ticket">
                                            <span>Creat ticket</span>
                                        </a>
                                        <div class="arr-blc">&nbsp;</div>
                                    </div>
                                    <div class="quest-info-bl">
                                        <p>This feature allows you to report a system failure. Please describe your problem as well as possible. With an emergency message a number of escalation processes is initiated. The average response time is within 24 hours on weekdays. If you think a disturbance at night or on weekends is present, please contact us by phone in addition. Attention: An emergency message is chargeable when this function is used for problems that you have caused yourself or being abused. We provide in this case our costs.</p>
                                        <a href="/support/ticket/create/{$category->ticket_category_id}" class="btn-cr-ticket-out">
                                            <span>Creat ticket</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="bord-l">&nbsp;</div>
                            </div>
                        </div>
                    {/foreach}
                    <div class="pagination-block">
                        <div class="nav-l-r">
                            <div class="nav-l">
                                {if $current_page > 1}
                                    <a href="/support/ticket/categories/page={$current_page-1}">&nbsp;</a>
                                {else}
                                    <a href="/support/ticket/categories/page=1">&nbsp;</a>
                                {/if}
                            </div>
                            <div class="nav-r">
                                {if $current_page == $categories.page_num}
                                    <a href="/support/ticket/categories/page={$current_page}">&nbsp;</a>
                                {else}
                                    <a href="/support/ticket/categories/page={$current_page+1}">&nbsp;</a>
                                {/if}
                            </div>
                        </div>
                        <a href="/support/ticket/categories/page=1">
                            <div class="page-n-first">
                                <span>1</span>
                            </div>
                        </a>
                        <div class="block-all-p">
                            <div class="all-p-b">
                                <span class="p-act">{$current_page}</span>
                                <span>of</span>
                                <span>{$categories.page_num}</span>
                            </div>
                            <form action="" method="post" enctype="multipart/form-data">
                                <input type="text" name="page" class="inp-tp" placeholder="type page">
                            </form>
                        </div>
                        <a href="/support/ticket/categories/page={$categories.page_num}">
                            <div class="page-n-last">
                                <span>{$categories.page_num}</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- CONTENT END -->
        <!-- RIGHTNBAR -->
        {include file='../../../main/right_side.tpl'}
        <!-- FOOTER -->
        {include file='../../../main/footer.tpl'}
        <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" />
        <script src="/web/templates/login/step/js/select2.js"></script>
        <script src="/web/templates/Support/Tickets/New-Ticket/js/main.js"></script>
        <script src="/web/templates/main/js/jquery.fs.selecter.min.js"></script>
        <script src="/web/templates/main/js/main-ds.js"></script>
        <script>

            function formatList(data) {
                if (data.loading) {
                    return data.text;
                }

                return data.name;
            }

            function formatListSelection(data) {
                window.location = "/support/ticket/categories/category=" + data.id;
                return data.name;
            }

            $(document).ready(function () {
                $(".inp-ftr").select2({
                    ajax: {
                        url: "/support/ticket/category/search/",
                        dataType: 'json',
                        data: function (params) {
                            return {
                                key: params.term,
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: data.items,
                            };
                        },
                        cache: true
                    },
                    escapeMarkup: function (markup) {
                        return markup;
                    },
                    minimumInputLength: 2,
                    templateResult: formatList,
                    templateSelection: formatListSelection
                });
            });

        </script>
    </body>

</html>
