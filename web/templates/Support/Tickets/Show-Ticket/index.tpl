<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>PowerPlay</title>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/web/templates/Support/Tickets/Show-Ticket/css/style.css">
        <link rel="stylesheet" href="/web/templates/main/css/main-style.css">
        <script src="/web/templates/main/js/jquery-1.11.3.min.js"></script>
    </head>

    <body>
        <!-- Popup SMS -->
        <div id="sms-popup">
            <div class="top-sms-popup">
                <div class="logo-sms-img-popup">
                    &nbsp;
                </div>
                <div class="sms-info-block-popup">
                    <span class="from-block-popup">from VIRTURO</span>
                    <span class="time-sms-block-popup">07.08.2015 at 17:45</span>
                </div>
            </div>
            <div class="sms-popup-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna</p>
            </div>
        </div>
        <div class="sms-popup-container" id="sms-popup-container">
            <div href="#" class="sms-close-img">&nbsp;</div>
            <div class="popup-body">
                <form action="#" method="">
                    <textarea class="enter-sms" name="enter-sms" id="" cols="30" rows="6" placeholder="Enter your message here"></textarea>
                    <button class="btn-send">
                        <span>send</span>
                    </button>
                </form>
            </div>
        </div>
        <!-- Popup SMS -->
        <!-- HEADER -->
        {include file='../../../main/header.tpl'}
        <!-- LEFTBAR -->
        {include file='../../../main/left_side.tpl'}
        <!-- CONTENT -->
        <div id="container">
            <div class="content">
                <div class="content-body">
                    <div class="nav-ticket">
                        <div class="back-btn">
                            {if isset($smarty.session.register.ticket_page) }
                                <a href="/support/ticket/all/page={$smarty.session.register.ticket_page|base64_decode}">&nbsp;</a>
                            {else}
                                <a href="/support/ticket/all">&nbsp;</a>
                            {/if}
                        </div>
                        <div class="nav-l">
                            <a href="#">&nbsp;</a>
                        </div>
                        <div class="nav-r">
                            <a href="#">&nbsp;</a>
                        </div>
                        <a href="#" class="btn-close-t">
                            <span>Close ticket</span>
                        </a>
                        <a href="#" class="btn-inform">
                            <span>inform manager</span>
                        </a>
                    </div>
                    <div class="title-tick">
                        <span>Ticket #812852</span>
                    </div>
                    <div class="ticket-table">
                        <table>
                            <tbody>
                                <tr class="tr-b tr-f">
                                    <td class="td-f">
                                        <span>Status</span>
                                    </td>
                                    <td class="td-s td-s-f">
                                        <span><strong>{ticket->getStatus }</strong></span>
                                    </td>
                                </tr>
                                <tr class="tr-b">
                                    <td class="td-f">
                                        <span>Category</span>
                                    </td>
                                    <td class="td-s">
                                        <span>{ticket->getCategory}</span>
                                    </td>
                                </tr>
                                <tr class="tr-b">
                                    <td class="td-f">
                                        <span>Date</span>
                                    </td>
                                    <td class="td-s">
                                        <span>{ticket->getDate}</span>
                                    </td>
                                </tr>
                                <tr class="tr-b">
                                    <td class="td-f">
                                        <span>Subject</span>
                                    </td>
                                    <td class="td-s">
                                        <span>{ticket->getSubject}</span>
                                    </td>
                                </tr>
                                <tr class="tr-b">
                                    <td class="td-f">
                                        <span>Country</span>
                                    </td>
                                    <td class="td-s">
                                        <span>{ticket->getCountry}</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="content-line-m"></div>
                    <div class="title-mess">
                        <span>Messages</span>
                    </div>
                    <div class="messages-block">
                        <div class="bg-opac-m">&nbsp;</div>
                        <div class="messages-body">
                            {foreach from=$answers item='answer'}
                                {if {$answer->ticket_question} != ''}
                                    <div class="mess-user body-s">
                                        <div class="title-user-mess">
                                            <span><strong>{ticket->getDate}</strong> from <strong>{ticket->getUserId}</strong> (Adeska internet)</span>
                                        </div>
                                        <div class="body-mess-user">
                                            <p>
                                                {$answer->ticket_question}
                                            </p>
                                        </div>
                                    </div>
                                {/if}
                                {if {$answer->ticket_answer} != ''}
                                    <div class="mess-system body-s">
                                        <div class="title-mess-s">
                                            <span><strong>{$answer->answer_date}</strong> from <strong>{$answer->answer_user_id}</strong> (LivingInternet - support)</span>
                                        </div>
                                        <div class="body-mess-s">
                                            <div class="i-img">&nbsp;</div>
                                            <p>
                                                {$answer->ticket_answer}
                                            </p>
                                        </div>
                                    </div>
                                {/if}
                            {/foreach}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- CONTENT END -->
        <!-- RIGHTNBAR -->
        {include file='../../../main/right_side.tpl'}
        <!-- FOOTER -->
        {include file='../../../main/footer.tpl'}
        <script src="/web/templates/main/js/main-ds.js"></script>
        <script src="/web/templates/Support/Tickets/Show-Ticket/js/main.js"></script>
    </body>

</html>