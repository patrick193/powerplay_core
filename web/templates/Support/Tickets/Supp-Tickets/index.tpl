<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>PowerPlay</title>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/web/templates/Support/Tickets/Supp-Tickets/css/style.css">
        <link rel="stylesheet" href="/web/templates/Support/Tickets/Supp-Tickets/css/jquery.fs.selecter.css">
        <link rel="stylesheet" href="/web/templates/main/css/main-style.css">
        <script src="/web/templates/main/js/jquery-1.11.3.min.js"></script>
    </head>

    <body>
        <!-- Popup SMS -->
        <div id="sms-popup">
            <div class="top-sms-popup">
                <div class="logo-sms-img-popup">
                    &nbsp;
                </div>
                <div class="sms-info-block-popup">
                    <span class="from-block-popup">from VIRTURO</span>
                    <span class="time-sms-block-popup">07.08.2015 at 17:45</span>
                </div>
            </div>
            <div class="sms-popup-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna</p>
            </div>
        </div>
        <!-- Popup SMS -->
        <!-- HEADER -->
        {include file='../../../main/header.tpl'}
        <!-- LEFTBAR -->
        {include file='../../../main/left_side.tpl'}
        <!-- CONTENT -->
        <div id="container">
            <div class="content">
                <div class="content-body">
                    <div class="content-top-block">
                        <div class="date-block">
                            <form action="/support/ticket/all/" method="post" enctype="multipart/form-data">
                                <div class="date-top">
                                    <div class="tl-date">
                                        <span>Date</span>
                                    </div>
                                    <div class="btn-cld">
                                        <span>
                                            CHOOSE DATE
                                        </span>
                                    </div>
                                </div>
                                <select name="category_id" id="" class="sel-frm">
                                    <option value="0">ALL CATEGORY</option>
                                    {foreach $categories as $category}
                                        {if $smarty.session.register.ticket_category|base64_decode == $category->ticket_category_id}
                                            <option selected="selected" value="{$category->ticket_category_id}">{$category->category_name}</option>
                                        {else}
                                            <option value="{$category->ticket_category_id}">{$category->category_name}</option>
                                        {/if}
                                    {/foreach}
                                </select>
                                <button class="btn-form">
                                    <span>SEARCH</span>
                                </button>
                            </form>
                        </div>
                        <div class="customer-satisfaction">
                            <span class="interest-c-s">75%</span>
                            <canvas id="customers_chart" height="118" width="118"></canvas>
                            <span class="title-c-s">Customer<br>Satisfaction</span>
                        </div>
                        <div class="customer-satisfaction">
                            <span class="interest-c-s">75%</span>
                            <canvas id="customers_chart_1" height="118" width="118"></canvas>
                            <span class="title-c-s">Response<br>time</span>
                        </div>
                    </div>
                    <div class="content-tbl">
                        <table class="table-cnt">
                            <tr class="frts-tr">
                                <td class="priority-td td-title">
                                    <a href="#">
                                        <span>Priority</span>
                                        <div class="tr-img-frst">&nbsp;</div>
                                    </a>
                                </td>
                                <td class="date-td td-title">
                                    <a href="#">
                                        <span>Date</span>
                                        <div class="tr-img-frst">&nbsp;</div>
                                    </a>
                                </td>
                                <td class="ticket-td td-title">
                                    <a href="#">
                                        <span>Ticket ID</span>
                                        <div class="tr-img-frst">&nbsp;</div>
                                    </a>
                                </td>
                                <td class="subject-td td-title">
                                    <a href="#">
                                        <span>Subject</span>
                                        <div class="tr-img-frst">&nbsp;</div>
                                    </a>
                                </td>
                                <td class="status-td td-title">
                                    <a href="#">
                                        <span>Status</span>
                                        <div class="tr-img-frst">&nbsp;</div>
                                    </a>
                                </td>
                                <td class="category-td td-title">
                                    <a href="#">
                                        <span>Category</span>
                                        <div class="tr-img-frst">&nbsp;</div>
                                    </a>
                                </td>
                            </tr>
                            {foreach $result.query as $ticket}
                                <tr onclick="showTicket({$ticket->ticket_id})" class="tr-g tr-b">
                                    <td class="priority-td">
                                        <div class="img-str">
                                            <img src="../../images/Support-Customer/Supp-Tickets/star-tbl-g.png" alt="">
                                        </div>
                                    </td>
                                    <td class="date-td">
                                        <span>{$ticket->date|date_format: '%d.%m.%Y'}</span>
                                    </td>
                                    <td class="ticket-td">
                                        <span>#{$ticket->ticket_id}</span>
                                    </td>
                                    <td class="subject-td">
                                        <span>{$ticket->subject}</span>
                                    </td>
                                    <td class="status-td">
                                        <img src="../../images/Support-Customer/Supp-Tickets/progress-img.png" alt="">
                                        <span>{$ticket->status_name}</span>
                                    </td>
                                    <td class="category-td">
                                        <span>{$ticket->category_name}</span>
                                    </td>
                                </tr>
                            {/foreach}
                        </table>
                        <div class="pagination-block">
                            <div class="nav-l-r">
                                <div class="nav-l">
                                    {if $current_page > 1}
                                        <a href="/support/ticket/all/page={$current_page-1}">&nbsp;</a>
                                    {else}
                                        <a href="/support/ticket/all/page=1">&nbsp;</a>
                                    {/if}
                                </div>
                                <div class="nav-r">
                                    {if $current_page == $result.page_num}
                                        <a href="/support/ticket/all/page={$current_page}">&nbsp;</a>
                                    {else}
                                        <a href="/support/ticket/all/page={$current_page+1}">&nbsp;</a>
                                    {/if}
                                </div>
                            </div>
                            <a href="/support/ticket/all/page=1">
                                <div class="page-n-first">
                                    <span>1</span>
                                </div>
                            </a>
                            <div class="block-all-p">
                                <div class="all-p-b">
                                    <span class="p-act">{$current_page}</span>
                                    <span>of</span>
                                    <span>{$result.page_num}</span>
                                </div>
                                <form action="" method="post" enctype="multipart/form-data">
                                    <input type="text" name="page" class="inp-tp" placeholder="type page">
                                </form>
                            </div>
                            <a href="/support/ticket/all/page={$result.page_num}">
                                <div class="page-n-last">
                                    <span>{$result.page_num}</span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- CONTENT END -->
        <!-- RIGHTNBAR -->
        {include file='../../../main/right_side.tpl'}
        <!-- FOOTER -->
        {include file='../../../main/footer.tpl'}
        <script src="/web/templates/main/js/jquery.fs.selecter.min.js"></script>
        <script src="/web/templates/main/js/main-ds.js"></script>
        <script src="/web/templates/main/js/Chart.min.js"></script>
        <script src="/web/templates/Support/Tickets/Supp-Tickets/js/main.js"></script>
        <script>
                                    function showTicket(id)
                                    {
                                        window.location = "/support/ticket/show/" + id;
                                    }
                                    $(document).ready(function () {
                                        $('select').selecter();
                                    });
        </script>
    </body>

</html>
