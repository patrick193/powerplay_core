<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>PowerPlay</title>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/web/templates/Support/Tickets/Create-Ticket/css/style.css">
        <link rel="stylesheet" href="/web/templates/Support/Tickets/Create-Ticket/css/jquery.fs.selecter.css">
        <link rel="stylesheet" href="/web/templates/main/css/main-style.css">
        <script src="/web/templates/main/js/jquery-1.11.3.min.js"></script>
    </head>

    <body>
        <!-- Popup SMS -->
        <div id="sms-popup">
            <div class="top-sms-popup">
                <div class="logo-sms-img-popup">
                    &nbsp;
                </div>
                <div class="sms-info-block-popup">
                    <span class="from-block-popup">from VIRTURO</span>
                    <span class="time-sms-block-popup">07.08.2015 at 17:45</span>
                </div>
            </div>
            <div class="sms-popup-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna</p>
            </div>
        </div>
        <!-- Popup SMS -->
        <!-- HEADER -->
        {include file='../../../main/header.tpl'}
        <!-- LEFTBAR -->
        {include file='../../../main/left_side.tpl'}
        <!-- CONTENT -->
        <div id="container">
            <div class="content">
                <div class="content-body">
                    <form action="/support/ticket/add/" method="post" class="form-cr-ticket">
                        <div class="form-body">
                            <span class="title-form-cr">Create your message here</span>
                            <div class="mess-info">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            </div>
                            <select name="category_id" id="">
                                <option value="">Choose category</option>
                                {foreach from=$categories item='category'}
                                    {if $categoryId == $category->ticket_category_id}
                                        <option selected="selected" value="{$category->ticket_category_id}">{$category->category_name}</option>
                                    {else}
                                        <option value="{$category->ticket_category_id}">{$category->category_name}</option>
                                    {/if}
                                {/foreach}
                            </select>
                            <input type="text" name="subject" class="int-subject" placeholder="Subject">
                            <textarea name="message" id="" cols="30" rows="10" class="mess-b" placeholder="Messages"></textarea>
                            <div class="block-priority">
                                <span class="title-pr">Set priority</span>
                                <div class="bl-pr">
                                    <input type="hidden" class="hidden-priority" name="priority_id" value="">
                                    {foreach from=$priority item='pr'}
                                        <a href="#" data-id='{$pr->ticket_priority_id}' class="bl-{$pr->priority_name} block-prt">
                                            <div class="bl-img-{$pr->priority_name[0]}">&nbsp;</div>
                                            <div class="bl-tx-{$pr->priority_name[0]}"><span>{$pr->priority_name}</span></div>
                                        </a>
                                    {/foreach}

                                </div>
                            </div>
                            <button class="btn-cr">
                                <span>Create tickets</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- CONTENT END -->
        <!-- RIGHTNBAR -->
        {include file='../../../main/right_side.tpl'}
        <!-- FOOTER -->
        {include file='../../../main/footer.tpl'}
        <script src="/web/templates/main/js/jquery.fs.selecter.min.js"></script>
        <script src="/web/templates/main/js/main-ds.js"></script>
        <script src="/web/templates/Support/Tickets/Create-Ticket/js/main.js"></script>
        <script>
            $(document).ready(function () {
                $('select').selecter();
            });
            $(".block-prt").click(function () {
                $(this).prev().prev("input").val($(this).data('id'));
            });
        </script>
    </body>

</html>