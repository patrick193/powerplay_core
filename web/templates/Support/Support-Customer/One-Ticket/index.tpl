<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>PowerPlay</title>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/web/templates/Support/css/Support-Customer/One-Ticket/style.css">
        <link rel="stylesheet" href="/web/templates/Support//css/main-style.css">
        <script src="/web/templates/Support/js/jquery-1.11.3.min.js"></script>
    </head>

    <body>
        <!-- Popup SMS -->
        <div id="sms-popup">
            <div class="top-sms-popup">
                <div class="logo-sms-img-popup">
                    &nbsp;
                </div>
                <div class="sms-info-block-popup">
                    <span class="from-block-popup">from VIRTURO</span>
                    <span class="time-sms-block-popup">07.08.2015 at 17:45</span>
                </div>
            </div>
            <div class="sms-popup-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna</p>
            </div>
        </div>
        <div class="sms-popup-container" id="sms-popup-container">
            <div href="#" class="sms-close-img">&nbsp;</div>
            <div class="popup-body">
                <form action="#" method="">
                    <textarea class="enter-sms" name="enter-sms" id="" cols="30" rows="6" placeholder="Enter your message here"></textarea>
                    <button class="btn-send">
                        <span>send</span>
                    </button>
                </form>
            </div>
        </div>
        <!-- Popup SMS -->
        <!-- HEADER -->
        <header>
            <div class="header-l">
                <div class="logo-img">
                    <a href="/"><img src="../../images/logo.png" class="logo-big" alt="logo">
                        <img src="../../images/logo-mini.png" class="logo-mini" alt="">
                    </a>
                </div>
                <div class="title-text-l"><span>CUSTOMER</span></div>
            </div>
            <div class="header-c">
                <div class="header-text">
                    <span class="title-text">ONE TICKET</span>
                    <p><a href="/">home</a><span class="arrow-step">&nbsp;</span><span class="finish-text">start</span></p>
                </div>
                <div class="messages">
                    <div class="notification">
                        <a href="#" id="show-sms"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAPCAMAAADXs89aAAAAjVBMVEX///9Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd5Xvd55O6nhAAAALnRSTlMAAQIGCwwODxUWFyAhIiQvMDNGW1xgYXKRmJmaqaq4ubzLzs/a29zd5Ofr8fP5hiMMmAAAAKJJREFUGNNt0EcOgzAQBdChOPSaACY00zEG7n+84CBkhPgLL55G1syH7TGwbZl1S8Y5YliGS2TMop0tjxaKUKWgnsUZ7I5op2qks+Fg0OvWPNRsax1OBrUcXa7uWKogGNA0BwDBPCG4MKqG7xKGSzpUSLBeNwYk65qA0Yi/nZ689pnPe39epHcO9ml+3TunPueYYel6pYRZzDtJ752k/6qe8gOEXhjFxkAh5wAAAABJRU5ErkJggg=="><span class="quantity">2</span>
                            <div class="mess-text">
                                <span>messages</span></div>
                        </a>
                        <div id="modal-sms">
                            <div class="container-modal-sms">
                                <div class="bg-modal-sms">&nbsp;</div>
                                <div class="top-sms-block">
                                    <div class="logo-sms-img">
                                        &nbsp;
                                    </div>
                                    <div class="sms-info-block">
                                        <span class="from-block">from VIRTURO</span>
                                        <span class="time-sms-block">07.08.2015 at 17:45</span>
                                    </div>
                                </div>
                                <div class="line-sms">&nbsp;</div>
                                <div class="sms-body">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam quis nostrud exercitation ullamco. laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
                                    </p>
                                </div>
                                <div class="sms-nav">
                                    <div class="nav-prev-sms-block">
                                        <a href="#" class="prev-sms">earlier</a>
                                    </div>
                                    <div class="close-sms-block">
                                        <a href="#close" title="Закрыть" class="close">close</a>
                                    </div>
                                    <div class="nav-next-sms-block">
                                        <a href="#" class="next-sms">next</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-r">
                <div class="pre-profile">
                    <div class="img-user"><img src="../../images/pre-photo.png" alt=""></div>
                    <div class="info-user">
                        <p><strong class="name-user">Lilu Dalas</strong> <span class="id-user">(10102334)</span></p>
                        <span class="user-type">LivingInternet, businessman</span>
                    </div>
                </div>
                <div class="line-prepr">&nbsp;</div>
                <div class="logout"><a href="/">&nbsp;</a></div>
            </div>
        </header>
        <!-- HEADER END -->
        <!-- LEFTBAR -->
        <div class="leftbar" id="leftbar">
            <div class="leftside">
                <div id="menu-bg">&nbsp;</div>
                <div class="main-menu" id="main-menu">
                    <ul>
                        <li class="usmanag-m menu-list">
                            <a href="#">
                                <span class="icon-manu icon-mm">&nbsp;</span>
                                <span class="text-menu">User Management</span>
                            </a>
                        </li>
                        <li class="support-m menu-list">
                            <span class="icon-manu icon-mm">&nbsp;</span>
                            <span class="text-menu">Support</span>
                            <ul class="menu-drop">
                                <li class="menu-drop-list"><a href="#">Tickets</a>
                                    <ul class="menu-dropdown">
                                        <li><a href="#">My Tickets</a></li>
                                        <li><a href="#">Create Ticket</a></li>
                                    </ul>
                                </li>
                                <li class="menu-drop-list">
                                    <a href="#">FAQ</a>
                                </li>
                            </ul>
                        </li>
                        <li class="orders-m menu-list">
                            <span class="icon-manu icon-mm">&nbsp;</span>
                            <span class="text-menu">Orders</span>
                            <ul class="menu-drop">
                                <li class="menu-drop-list">
                                    <a href="#">New Offer</a>
                                </li>
                                <li class="menu-drop-list">
                                    <a href="#">My Orders</a>
                                </li>
                                <li class="menu-drop-list">
                                    <a href="#">My Invoice</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="copywrite">
                    <div class="ft-logo-ds">
                        <span class="copywr">2015  ©</span>
                        <div class="ft-logo">
                            <a href="#"><img src="../../images/logo-LI.png" class="logo-ft" alt="logo">
                                <img src="../../images/logo-li-mini.png" class="logo-ft-mini" alt=""></a>
                        </div>
                    </div>
                    <div class="ft-logo-lp">
                        <div class="ft-logo-lp">
                            <img src="../../images/logo-li-mini.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- LEFTBAR END -->
        <!-- CONTENT -->
        <div id="container">
            <div class="content">
                <div class="content-body">
                    <div class="nav-ticket">
                        <div class="back-btn">
                            <a href="#">&nbsp;</a>
                        </div>
                        <div class="nav-l">
                            <a href="#">&nbsp;</a>
                        </div>
                        <div class="nav-r">
                            <a href="#">&nbsp;</a>
                        </div>
                        <a href="#" class="btn-close-t">
                            <span>Close ticket</span>
                        </a>
                        <a href="#" class="btn-inform">
                            <span>inform manager</span>
                        </a>
                    </div>
                    <div class="title-tick">
                        <span>Ticket #812852</span>
                    </div>
                    <div class="ticket-table">
                        <table>
                            <tbody>
                                <tr class="tr-b tr-f">
                                    <td class="td-f">
                                        <span>Status</span>
                                    </td>
                                    <td class="td-s td-s-f">
                                        <span><strong>{ticket->getStatus }</strong></span>
                                    </td>
                                </tr>
                                <tr class="tr-b">
                                    <td class="td-f">
                                        <span>Category</span>
                                    </td>
                                    <td class="td-s">
                                        <span>{ticket->getCategory}</span>
                                    </td>
                                </tr>
                                <tr class="tr-b">
                                    <td class="td-f">
                                        <span>Date</span>
                                    </td>
                                    <td class="td-s">
                                        <span>{ticket->getDate}</span>
                                    </td>
                                </tr>
                                <tr class="tr-b">
                                    <td class="td-f">
                                        <span>Subject</span>
                                    </td>
                                    <td class="td-s">
                                        <span>{ticket->getSubject}</span>
                                    </td>
                                </tr>
                                <tr class="tr-b">
                                    <td class="td-f">
                                        <span>Country</span>
                                    </td>
                                    <td class="td-s">
                                        <span>{ticket->getCountry}</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="content-line-m"></div>
                    <div class="title-mess">
                        <span>Messages</span>
                    </div>
                    <div class="messages-block">
                        <div class="bg-opac-m">&nbsp;</div>
                        <div class="messages-body">
                            {foreach from=$answers item='answer'}
                                {if {$answer->ticket_question} != ''}
                                    <div class="mess-user body-s">
                                        <div class="title-user-mess">
                                            <span><strong>{ticket->getDate}</strong> from <strong>{ticket->getUserId}</strong> (Adeska internet)</span>
                                        </div>
                                        <div class="body-mess-user">
                                            <p>
                                                {$answer->ticket_question}
                                            </p>
                                        </div>
                                    </div>
                                {/if}
                                {if {$answer->ticket_answer} != ''}
                                    <div class="mess-system body-s">
                                        <div class="title-mess-s">
                                            <span><strong>{$answer->answer_date}</strong> from <strong>{$answer->answer_user_id}</strong> (LivingInternet - support)</span>
                                        </div>
                                        <div class="body-mess-s last-mess-sms">
                                            <div class="i-img">&nbsp;</div>
                                            <p>
                                                {$answer->ticket_answer}
                                            </p>
                                        </div>
                                    </div>
                                {/if}
                            {/foreach}
                            {*<div class="mess-user body-s">
                            <div class="title-user-mess">
                            <span><strong>06.08.2015</strong> from <strong>Dirk Kuhne</strong> (Adeska internet)</span>
                            </div>
                            <div class="body-mess-user">
                            <p>
                            Hello,
                            <br> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            <br>
                            <br> Thank You.
                            <br> Grub Dirk Kuhne
                            </p>
                            </div>
                            </div>
                            <div class="mess-system body-s">
                            <div class="title-mess-s">
                            <span><strong>06.08.2015</strong> from <strong>Bjoern Wesarg</strong> (LivingInternet - support)</span>
                            </div>
                            <div class="body-mess-s">
                            <div class="i-img">&nbsp;</div>
                            <p>
                            Hello,
                            <br> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            <br>
                            <br> Thank You.
                            <br> Grub Bjoern Wesarg
                            </p>
                            </div>
                            </div>
                            <div class="mess-user body-s">
                            <div class="title-user-mess">
                            <span><strong>06.08.2015</strong> from <strong>Dirk Kuhne</strong> (Adeska internet)</span>
                            </div>
                            <div class="body-mess-user">
                            <p>
                            Hello,
                            <br> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            <br>
                            <br> Thank You.
                            <br> Grub Dirk Kuhne
                            </p>
                            </div>
                            </div>
                            <div class="mess-system body-s">
                            <div class="title-mess-s">
                            <span><strong>06.08.2015</strong> from <strong>Bjoern Wesarg</strong> (LivingInternet - support)</span>
                            </div>
                            <div class="body-mess-s last-mess-sms">
                            <div class="i-img">&nbsp;</div>
                            <div class="shadow-btn" id="shadow-btn">&nbsp;</div>
                            <div href="#" id="btn-answer" class="btn-answer">
                            <span>Answer</span>
                            </div>
                            <p>
                            Hello,
                            <br> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            <br>
                            <br> Thank You.
                            <br> Grub Bjoern Wesarg
                            </p>
                            </div>
                            </div>*}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- CONTENT END -->
        <!-- RIGHTNBAR -->
        <div class="rightbar">
            <div class="rightbar-panel show-rp hide-class">
                <div class="tabs">
                    <ul class="tabs-right">
                        <li>USERS</li>
                        <li>PROFILE</li>
                    </ul>
                    <div class="tabs-content">
                        <div class="tab-users">
                            <div class="views-users-block">
                                <div class="businessman-block">
                                    <div class="yourself-block">
                                        <div class="us1 us3">
                                            <div class="img-user-block">
                                                <img src="../../images/users/yourself-photo.png" alt="you">
                                            </div>
                                            <div class="info-user-block">
                                                <a href="#" class="name-user-link">Thats You</a>
                                                <span class="info-user">businessman</span>
                                            </div>
                                        </div>
                                        <div class="sales-block-show">
                                            <div class="sales-block us2">
                                                <div class="line-sl">&nbsp;</div>
                                                <div class="img-user-block">
                                                    <img src="../../images/users/user-s1.png" alt="">
                                                </div>
                                                <div class="info-user-block">
                                                    <a href="#" class="name-user-link">Jina Fillinsky</a><span class="id-user-block">(10107334)</span>
                                                    <span class="info-user">LivingInternet, sales</span>
                                                </div>
                                                <div class="customer-block-show">
                                                    <div class="customer-block cstm-bl">
                                                        <div class="line-cst-bg">&nbsp;</div>
                                                        <div class="img-user-block">
                                                            <img src="../../images/users/customer-photo.png" alt="">
                                                        </div>
                                                        <div class="info-user-block">
                                                            <a href="#" class="name-user-link">Peter Kirk</a><span class="id-user-block">(10107334)</span>
                                                            <span class="info-user">LivingInternet, customer</span>
                                                        </div>
                                                    </div>
                                                    <div class="customer-block cstm-bl">
                                                        <div class="line-cst-bg">&nbsp;</div>
                                                        <div class="img-user-block">
                                                            <img src="../../images/users/customer-photo.png" alt="">
                                                        </div>
                                                        <div class="info-user-block">
                                                            <a href="#" class="name-user-link">Peter Kirk</a><span class="id-user-block">(10107334)</span>
                                                            <span class="info-user">LivingInternet, customer</span>
                                                        </div>
                                                    </div>
                                                    <div class="customer-block cstm-bl">
                                                        <div class="line-cst-bg">&nbsp;</div>
                                                        <div class="img-user-block">
                                                            <img src="../../images/users/customer-photo.png" alt="">
                                                        </div>
                                                        <div class="info-user-block">
                                                            <a href="#" class="name-user-link">Peter Kirk</a><span class="id-user-block">(10107334)</span>
                                                            <span class="info-user">LivingInternet, customer</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="sales-block us2">
                                                <div class="line-sl">&nbsp;</div>
                                                <div class="img-user-block">
                                                    <img src="../../images/users/user-s2.png" alt="">
                                                </div>
                                                <div class="info-user-block">
                                                    <a href="#" class="name-user-link">Peter Kirk</a><span class="id-user-block">(10107334)</span>
                                                    <span class="info-user">LivingInternet, sales</span>
                                                </div>
                                                <div class="customer-block-show">
                                                    <div class="customer-block cstm-bl">
                                                        <div class="line-cst-bg">&nbsp;</div>
                                                        <div class="img-user-block">
                                                            <img src="../../images/users/customer-photo.png" alt="">
                                                        </div>
                                                        <div class="info-user-block">
                                                            <a href="#" class="name-user-link">Juliya Kirk</a><span class="id-user-block">(10107334)</span>
                                                            <span class="info-user">LivingInternet, customer</span>
                                                        </div>
                                                    </div>
                                                    <div class="customer-block cstm-bl">
                                                        <div class="line-cst-bg">&nbsp;</div>
                                                        <div class="img-user-block">
                                                            <img src="../../images/users/customer-photo.png" alt="">
                                                        </div>
                                                        <div class="info-user-block">
                                                            <a href="#" class="name-user-link">Alina Kirk</a><span class="id-user-block">(10107334)</span>
                                                            <span class="info-user">LivingInternet, customer</span>
                                                        </div>
                                                    </div>
                                                    <div class="customer-block cstm-bl">
                                                        <div class="line-cst-bg">&nbsp;</div>
                                                        <div class="img-user-block">
                                                            <img src="../../images/users/customer-photo.png" alt="">
                                                        </div>
                                                        <div class="info-user-block">
                                                            <a href="#" class="name-user-link">Juliya Kirk</a><span class="id-user-block">(10107334)</span>
                                                            <span class="info-user">LivingInternet, customer</span>
                                                        </div>
                                                    </div>
                                                    <div class="customer-block cstm-bl">
                                                        <div class="line-cst-bg">&nbsp;</div>
                                                        <div class="img-user-block">
                                                            <img src="../../images/users/customer-photo.png" alt="">
                                                        </div>
                                                        <div class="info-user-block">
                                                            <a href="#" class="name-user-link">Alina Kirk</a><span class="id-user-block">(10107334)</span>
                                                            <span class="info-user">LivingInternet, customer</span>
                                                        </div>
                                                    </div>
                                                    <div class="customer-block cstm-bl">
                                                        <div class="line-cst-bg">&nbsp;</div>
                                                        <div class="img-user-block">
                                                            <img src="../../images/users/customer-photo.png" alt="">
                                                        </div>
                                                        <div class="info-user-block">
                                                            <a href="#" class="name-user-link">Juliya Kirk</a><span class="id-user-block">(10107334)</span>
                                                            <span class="info-user">LivingInternet, customer</span>
                                                        </div>
                                                    </div>
                                                    <div class="customer-block cstm-bl">
                                                        <div class="line-cst-bg">&nbsp;</div>
                                                        <div class="img-user-block">
                                                            <img src="../../images/users/customer-photo.png" alt="">
                                                        </div>
                                                        <div class="info-user-block">
                                                            <a href="#" class="name-user-link">Alina Kirk</a><span class="id-user-block">(10107334)</span>
                                                            <span class="info-user">LivingInternet, customer</span>
                                                        </div>
                                                    </div>
                                                    <div class="customer-block cstm-bl">
                                                        <div class="line-cst-bg">&nbsp;</div>
                                                        <div class="img-user-block">
                                                            <img src="../../images/users/customer-photo.png" alt="">
                                                        </div>
                                                        <div class="info-user-block">
                                                            <a href="#" class="name-user-link">Juliya Kirk</a><span class="id-user-block">(10107334)</span>
                                                            <span class="info-user">LivingInternet, customer</span>
                                                        </div>
                                                    </div>
                                                    <div class="customer-block cstm-bl">
                                                        <div class="line-cst-bg">&nbsp;</div>
                                                        <div class="img-user-block">
                                                            <img src="../../images/users/customer-photo.png" alt="">
                                                        </div>
                                                        <div class="info-user-block">
                                                            <a href="#" class="name-user-link">Alina Kirk</a><span class="id-user-block">(10107334)</span>
                                                            <span class="info-user">LivingInternet, customer</span>
                                                        </div>
                                                    </div>
                                                    <div class="customer-block cstm-bl">
                                                        <div class="line-cst-bg">&nbsp;</div>
                                                        <div class="img-user-block">
                                                            <img src="../../images/users/customer-photo.png" alt="">
                                                        </div>
                                                        <div class="info-user-block">
                                                            <a href="#" class="name-user-link">Juliya Kirk</a><span class="id-user-block">(10107334)</span>
                                                            <span class="info-user">LivingInternet, customer</span>
                                                        </div>
                                                    </div>
                                                    <div class="customer-block cstm-bl">
                                                        <div class="line-cst-bg">&nbsp;</div>
                                                        <div class="img-user-block">
                                                            <img src="../../images/users/customer-photo.png" alt="">
                                                        </div>
                                                        <div class="info-user-block">
                                                            <a href="#" class="name-user-link">Alina Kirk</a><span class="id-user-block">(10107334)</span>
                                                            <span class="info-user">LivingInternet, customer</span>
                                                        </div>
                                                    </div>
                                                    <div class="customer-block cstm-bl">
                                                        <div class="line-cst-bg">&nbsp;</div>
                                                        <div class="img-user-block">
                                                            <img src="../../images/users/customer-photo.png" alt="">
                                                        </div>
                                                        <div class="info-user-block">
                                                            <a href="#" class="name-user-link">Juliya Kirk</a><span class="id-user-block">(10107334)</span>
                                                            <span class="info-user">LivingInternet, customer</span>
                                                        </div>
                                                    </div>
                                                    <div class="customer-block cstm-bl">
                                                        <div class="line-cst-bg">&nbsp;</div>
                                                        <div class="img-user-block">
                                                            <img src="../../images/users/customer-photo.png" alt="">
                                                        </div>
                                                        <div class="info-user-block">
                                                            <a href="#" class="name-user-link">Alina Kirk</a><span class="id-user-block">(10107334)</span>
                                                            <span class="info-user">LivingInternet, customer</span>
                                                        </div>
                                                    </div>
                                                    <div class="customer-block cstm-bl">
                                                        <div class="line-cst-bg">&nbsp;</div>
                                                        <div class="img-user-block">
                                                            <img src="../../images/users/customer-photo.png" alt="">
                                                        </div>
                                                        <div class="info-user-block">
                                                            <a href="#" class="name-user-link">Juliya Kirk</a><span class="id-user-block">(10107334)</span>
                                                            <span class="info-user">LivingInternet, customer</span>
                                                        </div>
                                                    </div>
                                                    <div class="customer-block cstm-bl">
                                                        <div class="line-cst-bg">&nbsp;</div>
                                                        <div class="img-user-block">
                                                            <img src="../../images/users/customer-photo.png" alt="">
                                                        </div>
                                                        <div class="info-user-block">
                                                            <a href="#" class="name-user-link">Alina Kirk</a><span class="id-user-block">(10107334)</span>
                                                            <span class="info-user">LivingInternet, customer</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="businessman-user">
                                        <div class="line-bs">&nbsp;</div>
                                        <div class="us1 us3">
                                            <div class="img-user-block">
                                                <img src="../../images/users/user-b1.png" alt="">
                                            </div>
                                            <div class="info-user-block">
                                                <a href="#" class="name-user-link">John Doe</a><span class="id-user-block">(10142334)</span>
                                                <span class="info-user">LivingInternet, businessman</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-profile">
                            <div class="user-photo-block">
                                <div class="refresh-user-photo">
                                    <form action="#" method="">
                                        <div class="block-file">
                                            <input type="file" id="btn_file">
                                        </div>
                                    </form>
                                </div>
                                <div class="user-photo">
                                    <img src="../../images/user-photo.png" alt="">
                                </div>
                            </div>
                            <div class="profile-info-block">
                                <div class="name-user-block">
                                    <div class="user-name" id="user-name">
                                        <span>Lilu Dalas</span>
                                    </div>
                                    <div class="id-user" id="id-user">
                                        <span>(10102334)</span>
                                    </div>
                                    <div class="type-user" id="type-user">
                                        <span>LivingInternet, customer</span>
                                    </div>
                                </div>
                                <div class="mail-user-block">
                                    <div class="mail-img">&nbsp;</div>
                                    <div class="profile-line-bt">&nbsp;</div>
                                    <div class="email-title">
                                        <a href="mailto:liludalas@5thelement.de">liludalas@5thelement.de</a>
                                    </div>
                                </div>
                                <div class="address-user-block">
                                    <div class="address-img">&nbsp;</div>
                                    <div class="profile-line-bt">&nbsp;</div>
                                    <div class="address-user">
                                        <div class="city-user">
                                            <span>Germany</span>
                                        </div>
                                        <div class="street-user">
                                            <span>Berlin 10117, Albrechtstrasse 26</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="info-user-block">
                                    <div class="info-img">&nbsp;</div>
                                    <div class="profile-line-bt">&nbsp;</div>
                                    <div class="code-user">
                                        <span>636656538863</span>
                                    </div>
                                </div>
                                <div class="btn-profile">
                                    <a href="/"><span>Change profile</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- RIGHTBAR END -->
        <!-- FOOTER -->
        <footer>
            <div class="ft-wrapper">
                <div class="ft-links">
                    <ul>
                        <li><a href="#">Imprint</a></li>
                        <li><a href="#">Conditions</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Terms and Conditions</a></li>
                        <li><a href="#">Confidentiality Agreement</a></li>
                    </ul>
                </div>
                <div class="ft-contacts">
                    <div class="ft-phone">
                        <span class="ft-m">telephone:</span><span class="ft-phone-number">+1-541-754-3010</span>
                    </div>
                    <div class="ft-social">
                        <span class="ft-m">We are in social:</span>
                        <a href="#" class="ft-link-fb">&nbsp;</a>
                        <a href="#" class="ft-link-gl">&nbsp;</a>
                    </div>
                </div>
            </div>
        </footer>
        <!-- FOOTER END -->
        <script src="/web/templates/Support/js/main-ds.js"></script>
        <script src="/web/templates/Support/js/Support-Customer/One-Ticket/main.js"></script>
    </body>

</html>
