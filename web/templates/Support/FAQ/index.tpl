<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>PowerPlay</title>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/web/templates/Support/FAQ/css/style.css">
        <link rel="stylesheet" href="/web/templates/Support/FAQ/css/jquery.fs.selecter.css">
        <link rel="stylesheet" href="/web/templates/main/css/main-style.css">
    </head>
    <body>
        <!-- Popup SMS -->
        <div id="sms-popup">
            <div class="top-sms-popup">
                <div class="logo-sms-img-popup">
                    &nbsp;
                </div>
                <div class="sms-info-block-popup">
                    <span class="from-block-popup">from VIRTURO</span>
                    <span class="time-sms-block-popup">07.08.2015 at 17:45</span>
                </div>
            </div>
            <div class="sms-popup-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna</p>
            </div>
        </div>
        <!-- Popup SMS -->
        {include file='../../main/header.tpl'}
        {include file='../../main/left_side.tpl'}
        <div id="container">
            <div class="content">
                <div class="content-body">
                    <div class="filters-block">
                        <span class="title-flt">Filters:</span>
                        <form action="/support/faq/show/" method="post" class="form-flt" enctype="multipart/form-data">
                            {if isset($smarty.session.register.faq_search) }
                                <input class="inp-ftr" type="text" name="search" value="{$smarty.session.register.faq_search|base64_decode}" placeholder="type to search ...">
                            {else}
                                <input class="inp-ftr" type="text" name="search" placeholder="type to search ...">
                            {/if}
                            <select name="category_id" id="" class="sel-ftl">
                                <option value="0">ALL CATEGORY</option>
                                {foreach $categories.query as $category}
                                    {if $smarty.session.register.faq_category|base64_decode == $category->faq_category_id}
                                        <option selected="selected" value="{$category->faq_category_id}">{$category->category_name}</option>
                                    {else}
                                        <option value="{$category->faq_category_id}">{$category->category_name}</option>
                                    {/if}
                                {/foreach}
                            </select>
                            <button class="btn-flt">
                                <span>SEARCH</span>
                            </button>
                        </form>
                    </div>
                    <div class="questions-block">
                        {if isset($faqs.query.0) }
                            {foreach $faqs.query as $faq}
                                <div class="quest-blck">
                                    <div class="quest-body">
                                        <div class="qeust-top-block">
                                            <span class="title-qest">{$faq->powerplay_faq_question}</span>
                                            <div class="arr-blc">&nbsp;</div>
                                        </div>
                                        <div class="quest-info-bl">
                                            <p>{$faq->powerplay_faq_answer}</p>
                                        </div>
                                    </div>
                                    <div class="bord-l">&nbsp;</div>
                                </div>
                            {/foreach}
                        {/if}
                    </div>
                    <div class="pagination-block">
                        <div class="nav-l-r">
                            <div class="nav-l">
                                {if $current_page > 1}
                                    <a href="/support/faq/show/page={$current_page-1}">&nbsp;</a>
                                {else}
                                    <a href="/support/faq/show/page=1">&nbsp;</a>
                                {/if}
                            </div>
                            <div class="nav-r">
                                {if $current_page == $faqs.page_num}
                                    <a href="/support/faq/show/page={$current_page}">&nbsp;</a>
                                {else}
                                    <a href="/support/faq/show/page={$current_page+1}">&nbsp;</a>
                                {/if}
                            </div>
                        </div>
                        <a href="/support/faq/show/page=1">
                            <div class="page-n-first">
                                <span>1</span>
                            </div>
                        </a>
                        <div class="block-all-p">
                            <div class="all-p-b">
                                <span class="p-act">{$current_page}</span>
                                <span>of</span>
                                <span>{$faqs.page_num}</span>
                            </div>
                            <form action="" method="post" enctype="multipart/form-data">
                                <input type="text" name="page" class="inp-tp" placeholder="type page">
                            </form>
                        </div>
                        <a href="/support/faq/show/page={$faqs.page_num}">
                            <div class="page-n-last">
                                <span>{$faqs.page_num}</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        {include file='../../main/right_side.tpl'}
        {include file='../../main/footer.tpl'}
        <script src="/web/templates/main/js/jquery-1.11.3.min.js"></script>
        <script src="/web/templates/main/js/jquery.fs.selecter.min.js"></script>
        <script src="/web/templates/main/js/main-ds.js"></script>
        <script src="/web/templates/Support/FAQ/js/main.js"></script>
        <script>
            $(document).ready(function () {
                $('select').selecter();
            });
        </script>
    </body>

</html>
