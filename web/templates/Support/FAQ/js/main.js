$(document).ready(function() {
    $(".qeust-top-block").click(function() {
        $('.title-qest').css("font-weight", "400");
        $(".quest-info-bl").hide(400);
        $(".bord-l").hide(400);
        $('.qeust-top-block').children('.arr-blc-max')
            .removeClass('arr-blc-max').addClass('arr-blc');
        if ($(this).next("div.quest-info-bl").is(':hidden')) {
            $(this).children('.arr-blc').addClass('arr-blc-max');
            $(this).children('.title-qest').css("font-weight", "300");

            $(this).next("div.quest-info-bl").slideToggle(400);
            $(this).parent().next(".bord-l").show();
        } else {
            $(this).children('.arr-blc-max').removeClass('arr-blc-max')
                .addClass('arr-blc');
            $(this).children('.title-qest').css("font-weight", "400");
        }


    });



    $(".all-p-b").click(function() {

        if ($(".inp-tp").is(':hidden')) {
            $(".inp-tp").show(200);
        } else {
            $(".inp-tp").hide(200);
        }

    });
});
