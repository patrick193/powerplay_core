<div class="blist">
    <div class="top-block-s">
        <div class="block-user">
            <div class="us-avat-bl">
                <img src="/web/templates/UsManag/BisList/img/av-s.png" alt="">
            </div>
            <div class="us-info-bl">
                <span class="top-sp-us"><strong>{$user->getUserFirstName()} {$user->getUserLastName()}</strong> ({$user->getUserId()})</span>
                <span class="bt-ps-us">{$user->getCompanyName()}, {$user->getRole()->getRoleName()}</span>
            </div>
        </div>
        <div class="line-left">&nbsp;</div>
        <div class="line-mid">&nbsp;</div>
        <div class="line-right">&nbsp;</div>
        {assign var='ubu' value = $smarty.session.register.user_back_up|base64_decode|unserialize}
        
        
        {if $ubu}
            <div class="line-to-u">&nbsp;</div>
            <a href="/usermanagement/back" class="block-yours">
                <span>That’s you</span>
            </a>
        {/if}
    </div>
    <div class="main-block-user">
        <div class="block-businessmans">
            <div class="top-block-us">
                <span class="title-sp-us">BUSINESSMANs</span>
            </div>
            <div class="blocklist-b">
                <div class="b-block">
                    <span>Salesman can only have<br>customers as subusers.</span>
                </div>
            </div>
        </div>
        <div class="block-sales">
            <div class="top-block-us">
                <span class="title-sp-us">SALEs</span>
            </div>
            <div class="blocklist-c">
                <div class="block-s-user">
                    <div class="b-avat-bl">
                        <img src="/web/templates/UsManag/BisList/img/s5.png" alt="{$user->getUserFirstName()}">
                    </div>
                    <div class="b-info-bl">
                        <span class="top-sp-b"><strong>{$user->getUserFirstName()} {$user->getUserLastName()}</strong> ({$user->getUserId()})</span>
                        <span class="bt-ps-b">{$user->getCompanyName()}</span>
                    </div>
                </div>
                <div class="b-block">
                    <span>Salesman can only have<br>customers as subusers.</span>
                </div>
            </div>
        </div>
        <div class="block-customers">
            <div class="top-block-us">
                <span class="title-sp-us">CUSTOMERs</span>
            </div>
            <div class="blocklist-c">
                <form action="#" method="#">
                    <input type="text" name="search-c" class="inp-cust" placeholder="Start type name or id">
                </form>
                <div class="body-blockl-c">
                    <div class="block-c">
                        <div class="body-blockl-c">
                            {if $users != 0}
                                {foreach from = $users item='customer'}
                                    {if $customer->getRole()->getRoleCode() == '2C'}
                                        <div class="block-c">
                                            <div class="c-avat-bl">
                                                <img src="/web/templates/UsManag/BisList/img/users/customer-photo.png" alt="">
                                            </div>
                                            <div class="c-info-bl">
                                                <span class="top-sp-c"><a href="/usermanagement/auth/{$user->getUserId()}/{$customer->getUserId()}">{$customer->getUserFirstName()} {$customer->getUserLastName()}</a> ({$customer->getUserId()})</span>
                                                <span class="bt-ps-c">{$customer->getCompanyName()}</span>
                                            </div>
                                        </div>
                                    {/if}
                                {/foreach}
                            {else}
                                <div class="block-c">
                                    <div class="c-avat-bl">
                                        <img src="/web/templates/UsManag/BisList/img/users/customer-photo.png" alt="">
                                    </div>
                                    <div class="c-info-bl">
                                        <span class="top-sp-c"><a href="#">You do not have a sales yet </a> </span>
                                    </div>
                                </div>
                            {/if}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>