<!DOCTYPE html>
<html lang="en">
    {assign var="user" value=$smarty.session.register.user_auth|base64_decode|unserialize}

    <head>
        <meta charset="UTF-8">
        <title>ACOUNT AND PRICE</title>
        <script src="/web/templates/UsManag/BisList/js/jquery-1.11.3.min.js"></script>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
        <script src="/web/templates/UsManag/BisList/js/jquery.nicescroll.min.js"></script>
        <link rel="stylesheet" href="/web/templates/UsManag/BisList/css/style{if $user->getRole()->getRoleCode() == '2B'}.css{else}_sales.css{/if}">
        <script src="/web/templates/UsManag/BisList/js/jquery.fs.selecter.min.js"></script>
        <script scr="/web/templates/UsManag/BisList/js/modernizr.js"></script>
        <script>
            $(document).ready(function () {
                $('select').selecter();
            });
        </script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(
                    function () {
                        $("html").niceScroll({
                            cursoropacitymin: '0.1',
                            zindex: '9999',
                            cursorborder: '0'
                        });
                        $(".sms-body").niceScroll({
                            cursorcolor: '#323940',
                            cursorwidth: '8px',
                            cursorborder: '0'
                        });
                        $(".body-blockl-c").niceScroll({
                            cursoropacitymin: '0.2',
                            cursorcolor: '#323940',
                            cursorwidth: '8px',
                            cursorborder: '0'
                        });

                    }
            );
        </script>
        <script type="text/javascript" charset="utf-8">
            $(function () {
                $('#main-menu li').hover(function () {
                    $(this).children('ul').stop(false, true).fadeIn(300);
                }, function () {
                    $(this).children('ul').stop(false, true).fadeOut(0);
                })
            })
        </script>
        <script type="text/javascript" charset="utf-8">
            $(function () {
                $('#menu-drop li').hover(function () {
                    $(this).children('ul').stop(false, true).fadeIn(300);
                }, function () {
                    $(this).children('ul').stop(false, true).fadeOut(0);
                })
            })
        </script>
        <script language="JavaScript">
            function showLinkImage(id) {
                document.getElementById(id).style.display = "block";
            }

            function hideLinkImage(id) {
                document.getElementById(id).style.display = "none";
            }
        </script>

    </head>

    <body>
        <div class="popup-form" id="popup-form">
            <a href="#" class="img-close-bl">&nbsp;</a>
            <span class="title-popup-form" data-role-name="">Add </span>
            <form  method="post" class="reg-form-b">
                <input name="role_id" type="hidden" class="role_id" value="">
                <div class="block-name">
                    <div class="block-title-name">NAME</div>
                    <div class="firstname">
                        <span class="name-input">Firstname:</span>
                        <input type="text" class="input-firstname input-main" name="name">
                    </div>
                    <div class="lastname">
                        <span class="name-input">Lastname:</span>
                        <input type="text" class="input-lastname input-main" name="lastname">
                    </div>
                </div>
                <div class="block-company">
                    <div class="company">
                        <span class="name-input">Email:</span>
                        <input type="text" class="input-company input-main" name="email">
                    </div>
                </div>
                <div class="block-company">
                    <div class="company">
                        <span class="name-input">Company:</span>
                        <input type="text" class="input-company input-main" name="company_name">
                    </div>
                </div>
                <div class="block-address">
                    <div class="block-title-name">ADDRESS</div>
                    <div class="street">
                        <span class="name-input">Street and House Number:</span>
                        <input type="text" class="input-street input-main" name="street">
                    </div>
                    <div class="postalcode-city">
                        <div class="postalcode">
                            <span class="name-input">Postal Code:</span>
                            <input type="text" class="input-postalcod input-main" name="postal_code">
                        </div>
                        <div class="city">
                            <span class="name-input">City:</span>
                            <input type="text" class="input-city input-main" name="city">
                        </div>
                    </div>
                    <div class="region">
                        <span class="name-input">Region:</span>
                        <input type="text" class="input-region input-main" name="region">
                    </div>
                    <div class="country">
                        <span class="name-input">Country:</span>
                        <input type="text" class="input-country input-main" name="country">
                    </div>
                </div>
                <div class="block-phone">
                    <div class="phone">
                        <span class="name-input">Cell phone number:</span>
                        <input type="text" class="input-phone input-main" name="cell_phone">
                    </div>
                </div>
                <span class="format-phone">in format +00 (000) 633-513-65</span>
                <div class="check">
                    <div class="btn-form">
                        <a class="btn-continue" data-edit=''><span>ADD</span></a>
                    </div>
                </div>
            </form>
        </div>
        <div id="sms-popup">
            <div class="top-sms-popup">
                <div class="logo-sms-img-popup">
                    &nbsp;
                </div>
                <div class="sms-info-block-popup">
                    <span class="from-block-popup">from VIRTURO</span>
                    <span class="time-sms-block-popup">07.08.2015 at 17:45</span>
                </div>
            </div>
            <div class="sms-popup-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna</p>
            </div>
        </div>
        <div id="bg-layer">&nbsp;</div>
        <div id="bg-layer-2">&nbsp;</div>
        <div class="leftbar-bg" id="leftbar-bg">&nbsp;</div>
        <div class="rightbar-bg">&nbsp;</div>
        <div id="container">
            <div class="header-block">&nbsp;</div>
            {include file='../../main/left_side.tpl'}
            <div class="content">
                <div class="header-c">
                    <div class="header-text">
                        <span class="title-text">ACOUNT AND PRICE</span>
                        <p><a href="/">home</a><span class="arrow-step">&nbsp;</span><a href="/">start</a><span class="arrow-step">&nbsp;</span><span class="finish-text">businessman</span></p>
                    </div>
                    <div class="messages">
                        <div class="notification">
                            <a href="#" id="show-sms"><img src="/web/templates/UsManag/BisList/img/sms-img.png"><span class="quantity">2</span>
                                <div class="mess-text">
                                    <span>messages</span></div>
                            </a>
                            <div id="modal-sms">
                                <div class="container-modal-sms">
                                    <div class="bg-modal-sms">&nbsp;</div>
                                    <div class="top-sms-block">
                                        <div class="logo-sms-img">
                                            &nbsp;
                                        </div>
                                        <div class="sms-info-block">
                                            <span class="from-block">from VIRTURO</span>
                                            <span class="time-sms-block">07.08.2015 at 17:45</span>
                                        </div>
                                    </div>
                                    <div class="line-sms">&nbsp;</div>
                                    <div class="sms-body">
                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam quis nostrud exercitation ullamco. laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
                                        </p>
                                    </div>
                                    <div class="sms-nav">
                                        <div class="nav-prev-sms-block">
                                            <a href="#" class="prev-sms">earlier</a>
                                        </div>
                                        <div class="close-sms-block">
                                            <a href="#close" title="Закрыть" class="close">close</a>
                                        </div>
                                        <div class="nav-next-sms-block">
                                            <a href="#" class="next-sms">next</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-body">
                    {if $user->getRole()->getRoleCode() == '2B'}
                        {include file='./listBusinessman.tpl'}
                    {elseif $user->getRole()->getRoleCode() == '2S'}
                        {include file='./listSales.tpl'}
                    {else}
                        {assign var='backUp' value = $smarty.session.register.user_back_up|base64_decode|unserialize}
                        {if !$backUp|is_bool}
                            {assign var='backUp2' value = $smarty.session.register.user_back_up_2|base64_decode|unserialize}
                            {if !$backUp2|is_null and !$backUp2|is_bool}
                                <a href="#">{$user->getUserFirstName()}</a> (you are here)| <a href="/usermanagement/back/">{$backUp->getUserFirstName()}</a> | <a href="/usermanagement/goup/">{$backUp2->getUserFirstName()}</a>
                            {else}
                                <a href="#">{$user->getUserFirstName()}</a> (you are here)| <a href="/usermanagement/back/">{$backUp->getUserFirstName()}</a> 

                            {/if}

                        {/if}
                        <br>
                        You are a customer and does not have this possibility
                    {/if}
                </div>
            </div>
            {include file='../../main/right_side.tpl'}
        </div>
        {include file='../../main/footer.tpl'}
        <script>
            $('#modal-sms, #bg-layer, #sms-popup, #mod-window-sms').hide();
            $('#show-sms').click(function () {
                $('#modal-sms, #bg-layer, #sms-popup').show();
                $('#bg-layer').animate({
                    opacity: 0.8
                });
            });
            $('.show-info-b').click(function () {
                $(this).parent().children('.mod-window-sms').show();
                $(this).parent('.b-block').css("z-index", "99999");
                $('#bg-layer').show();
                $('#bg-layer').animate({
                    opacity: 0.8
                });
            });
            $('#bg-layer').click(function () {
                $('#modal-sms, #bg-layer, #sms-popup, #mod-window-sms').fadeOut(500);
                $('.b-block').css("z-index", "10");
            });
            $('#popup-form, #bg-layer-2').hide();
            $('.show-add').click(function () {
                $(".btn-continue").click(function () {
                    url = "/usermanagement/add/";
                    $.post(url, $(".reg-form-b").serialize());
                    alert('added');
                });
                var role = $(this).data('role');
                var roleName = $(this).data('role-name');
                var formText = "Add ";

                $(".role_id").val(role);
                $(".title-popup-form").text(formText + roleName)
                $('#popup-form, #bg-layer-2').show();
                $('#bg-layer-2').animate({
                    opacity: 0.8
                });
            });


            $('#bg-layer-2').click(function () {
                $('#popup-form, #bg-layer-2').fadeOut(500);
            });
            $('a.btn-ok-close').click(function () {
                $('div#mod-window-sms').hide();
                $('div#bg-layer').hide();
                $('.b-block').css("z-index", "10");
            });
            $('a.img-close-bl').click(function () {
                $('#popup-form, #bg-layer-2').fadeOut(500);

            });
            $('a.close').click(function () {
                $('div#modal-sms, div#bg-layer, #sms-popup').hide();
            });
            (function ($) {
                jQuery.fn.lightTabs = function (options) {

                    var createTabs = function () {
                        tabs = this;
                        i = 0;

                        showPage = function (i) {
                            $(tabs).children("div").children("div").hide();
                            $(tabs).children("div").children("div").eq(i).show();
                            $(tabs).children(".tabs-right").children("li").removeClass("active");
                            $(tabs).children(".tabs-right").children("li").eq(i).addClass("active");
                        }

                        showPage(1);

                        $(tabs).children(".tabs-right").children("li").each(function (index, element) {
                            $(element).attr("data-page", i);
                            i++;
                        });

                        $(tabs).children(".tabs-right").children("li").click(function () {
                            showPage(parseInt($(this).attr("data-page")));
                        });
                    };
                    return this.each(createTabs);
                };
            })(jQuery);
            $(document).ready(function () {
                $(".tabs").lightTabs();
            });
            $(".us1").click(function () {
                if ($(".sales-block-show").is(':hidden')) {
                    $(this).parent().children(".sales-block-show").show(200);
                } else {
                    $(".sales-block-show").hide(200);
                }
            });
            $(".us2").click(function () {
                $(".customer-block-show").hide(200);
                if ($(this).children(".customer-block-show").is(':hidden')) {
                    $(this).children(".customer-block-show").show(200);
                }
            });
            $(".us3").click(function () {
                if ($(".line-bs-block").is(':hidden')) {
                    $(this).parent().children(".line-bs-block").show(200);
                } else {
                    $(".line-bs-block").hide(200);
                }
            });
            $(".pre-profile").click(function () {
                if ($(window).width() <= 1660) {
                    if ($(".rightbar-panel, .rightbar-bg ").is(':hidden')) {
                        $(".rightbar-panel, .rightbar-bg ").show(200);
                    } else {
                        $(".rightbar-panel, .rightbar-bg ").hide(200);
                    }
                }
            });

            $(".leftbar").hover(function () {
                if ($(window).width() < 1660) {
                    $(".logo-ft").css("display", "block");
                    $(".copywr").css("display", "inline-block");
                    $(".logo-ft-mini").css("display", "none");
                    $(".leftbar-bg").css("width", "330");
                }
            });
            $(".leftbar").mouseout(function () {
                if ($(window).width() < 1660) {
                    $(".logo-ft").css("display", "none");
                    $(".copywr").css("display", "none");
                    $(".logo-ft-mini").css("display", "block");
                    $(".leftbar-bg").css("width", "70");
                } else {

                    $(".leftbar-bg").css("width", "330");
                }
            });


            $(window).resize(function () {
                if ($(window).width() < 1660) {
                    $(".rightbar-panel, .rightbar-bg ").hide();
                    $(".logo-ft").css("display", "none");
                    $(".copywr").css("display", "none");
                    $(".logo-ft-mini").css("display", "block");
                    $(".leftbar-bg").css("width", "70");
                } else {
                    $(".rightbar-panel, .rightbar-bg ").show();
                    $(".logo-ft").css("display", "block");
                    $(".copywr").css("display", "inline-block");
                    $(".logo-ft-mini").css("display", "none");
                    $(".leftbar-bg").css("width", "330");
                }
            });
        </script>
    </body>

</html>
