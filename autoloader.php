<?php

class autoloader {

    private static $repository;

    public static function addAutoloader($repository) {
        self::$repository = $repository;
        spl_autoload_register(array(__CLASS__, 'autoload'));
    }

    static function autoload() {
        $autoloader = self::$repository;
        require_once $autoloader;
    }

}
